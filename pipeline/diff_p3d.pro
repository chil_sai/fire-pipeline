function diff_p3d,k3d,dim

s=size(k3d)
if(s[0] ne 3) then begin
    message,/inf,'Only 3-dimensional input is supported'
    return,!values.d_nan
endif

case dim of
    0 : begin
            dk3d=double(k3d[1:*,*,*])
            for j=0,s[1]-2 do dk3d[j,*,*]*=double(j+1)
        endcase
    1 : begin
            dk3d=double(k3d[*,1:*,*])
            for j=0,s[2]-2 do dk3d[*,j,*]*=double(j+1)
        endcase
    2 : begin
            dk3d=double(k3d[*,*,1:*])
            for j=0,s[3]-2 do dk3d[*,*,j]*=double(j+1)
        endcase
    else : begin
               message,/inf,'Dimension must be 0,1 or 2'
               dk3d=!values.d_nan
           endcase
endcase

return,dk3d
end
