function fire_get_readout_dirs,filelist,datapath=datapath

n_files=n_elements(filelist)
if(n_elements(datapath) ne 1) then datapath='/Volumes/ENFUEGO/FIRE/BAADE/H2RG-C001-ASIC-FIRE001/'

dirs=strarr(n_files)+datapath

for i=0,n_files-1 do begin
    h=headfits(filelist[i])
    dirs[i]+=(sxpar(h,'READMODE') eq 'SUTR')? '/UpTheRamp/' : '/FSRamp/'
    dirs[i]+=sxpar(h,'RAWPATH')+'/'
endfor


return,dirs
end
