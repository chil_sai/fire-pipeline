; Taken from the CfA MMIRS pipeline and renamed

function fire_correct_nonlinearity,raw_im,calib_data=c,nobias=nobias
    s_im=size(raw_im)
    n_cal=n_elements(c)
    if(s_im[1] ne 2048 or s_im[2] ne 2048 or (n_cal ne 4096 and n_cal ne 2048)) then return,raw_im*!values.f_nan
    im_corr=raw_im*!values.f_nan
    bias_coeff=(keyword_set(nobias))? 0.0 : 1.0
    for i=0,n_cal-1 do begin
        im_frag=poly(double(raw_im[c[i].xmin:c[i].xmax,c[i].ymin:c[i].ymax]-bias_coeff*c[i].bias),c[i].c_poly)
        satur_im=where((raw_im[c[i].xmin:c[i].xmax,c[i].ymin:c[i].ymax]-bias_coeff*c[i].bias) gt c[i].saturation,c_satur_im)
;        print,'i=',i,' c_satur_im=',c_satur_im
        if(c_satur_im gt 0) then im_frag[satur_im]=!values.f_nan
        im_corr[c[i].xmin:c[i].xmax,c[i].ymin:c[i].ymax]=im_frag
    endfor

    return,im_corr
end
