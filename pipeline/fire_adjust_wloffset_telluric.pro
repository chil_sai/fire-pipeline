function fire_adjust_wloffset_telluric,tel_res,thr_spix=thr_spix,thr_espix=thr_espix,deg1=deg1a,deg2=deg2a,badonly=badonly,robust=robust
    if(n_elements(deg1a) ne 1) then deg1a=4
    if(n_elements(deg2a) ne 1) then deg2a=4
    if(n_elements(thr_espix) ne 1) then thr_espix=0.2 ;;; errors of dwl_shift<0.2 pix
    if(n_elements(thr_spix) ne 1) then thr_spix=2.0 ;;; abs(dwl_shift)<2.0 pix

    n_ord=n_elements(tel_res)
    tel_res_fit=tel_res
    swlnp=n_elements(tel_res[0].wl_shift.wl_shift)

    deg1=(deg1a lt swlnp)? deg1a : swlnp-1
    deg2=(deg2a lt n_ord)? deg2a : n_ord-1
    xarr = (dindgen(swlnp)+0.5d) # (dblarr(n_ord)+1)
    oarr = (dblarr(swlnp)+1d) # dindgen(n_ord)
    varr = tel_res.wl_shift.wl_shift
    earr = tel_res.wl_shift.ewl_shift
    for i=0,n_ord-1 do begin
        varr[*,i]/=tel_res[i].wl_shift.wl_step
        k_err=(i eq n_ord-1)? median(tel_res.wl_shift.chi2dof) : tel_res[i].wl_shift.chi2dof
        earr[*,i]*=sqrt(k_err)/tel_res[i].wl_shift.wl_step
    endfor
    g_shift = where(abs(varr) lt thr_spix and earr gt 0 and earr le thr_espix and finite(varr+earr) eq 1, cg_shift, compl=b_shift, ncompl=cb_shift)
    if(cg_shift gt 1.5*(deg1+1)*(deg2+1)) then begin
        sdata=dblarr(3,cg_shift)
        sdata[0,*]=xarr[g_shift]
        sdata[1,*]=oarr[g_shift]
        sdata[2,*]=varr[g_shift]
        edata=earr[g_shift]
        if(deg1 gt 0) then begin
            f = sfit_2deg(sdata,deg1,deg2,err=edata,kx=kx,/irreg)
            g_fit = poly2d(dindgen(swlnp)+0.5d,dindgen(n_ord),kx,deg1=deg1,deg2=deg2)
        endif else begin
            f = (deg2 le 5 and keyword_set(robust))? robust_poly_fit(transpose(sdata[1,*]),transpose(sdata[2,*]),deg2) : poly_fit(transpose(sdata[1,*]),transpose(sdata[2,*]),deg2,measure_errors=edata)
            g_fit = transpose(poly(dindgen(n_ord),f))
        endelse
        if(keyword_set(badonly)) then g_fit[g_shift]=varr[g_shift]
        for i=0,n_ord-1 do g_fit[*,i]*=tel_res[i].wl_shift.wl_step
        tel_res_fit.wl_shift.wl_shift=g_fit
    endif

    return,tel_res_fit
end


