function fire_measure_offset_2d,image_in,kwl3d,trace_coeff,fwhm,$
    ord_ny=ord_ny,legendre=legendre,crd_map=crd_map,m0=m0,o_dim=o_dim,$
    flat=flat,nopixflat=nopixflat,pixel_fwhm=pixel_fwhm,sigma=sigma,smooth_sigma=smooth_sigma,$
    corners=corners,linetab=linetab,weight_tab=weight_tab,el_lab_tab=el_lab_tab,labellist=labellist,clean=clean,$
    xshift=xshift,yshift=yshift,magnification=magnification,int5p=int5p,fourier=fourier,$
    max_corr=max_corr,synframe=synframe,corr_mat=corr_mat,fit=fit,verbose=verbose

    image=image_in
    s_im=size(image)
    nx=s_im[1]
    ny=s_im[2]
    n_ord=n_elements(trace_coeff[0,*])

    if(n_params() eq 3) then fwhm=2.0d ;;; 0.45 arcsec slit
    if(n_elements(smooth_sigma) ne 1) then smooth_sigma=0d
    if(n_elements(sigma) ne 1) then sigma=1.0d ;;; FIRE PSF
    if(n_elements(linetab) lt 1) then linetab=getenv('FIRE_PIPELINE_PATH')+'calib_FIRE/linelists/linesOH_R2k_HITRAN_H2O_nm.tab'
    if(n_elements(magnification) ne 1) then magnification=10
    if(n_elements(xshift) ne 1) then xshift=10
    if(n_elements(yshift) ne 1) then yshift=10
    if(n_elements(corners) lt 1) then corners=[1230,1300,1400,1450] ;; x0,y0,x1,y1 [[1230,1300,1400,1450],[460,1330,630,1480]]
    pixflat=(keyword_set(nopixflat))? 1.0 : readfits(getenv('FIRE_PIPELINE_PATH')+'calib_FIRE/detector/H2RG/pixflat.fits',/silent)

    s_c=size(corners)
    n_reg=(s_c[0] eq 1)? 1 : s_c[2]
    xy_off=dblarr(2,n_reg) + !values.d_nan

    for r=0,n_reg-1 do begin
        if(keyword_set(verbose)) then print,'Processing region (X1,Y1,X2,Y2): '+string(corners[*,r],format='(4i5)')
        if(keyword_set(clean)) then begin
            if(r eq 0) then image_med=median(image,3)
            min_med=min(image_med[corners[0,r]:corners[2,r],corners[1,r]:corners[3,r]],max=max_med,/nan)
            badpix=where(finite(image) ne 1 or $
                         image lt min_med-(max_med-min_med)*0.5 or $
                         image gt max_med+(max_med-min_med)*1.0, cbadpix)
            if(cbadpix gt 0) then image[badpix]=image_med[badpix]
        endif


        p1=(image/pixflat)[corners[0,r]:corners[2,r],corners[1,r]:corners[3,r]]
        bp1=where(finite(p1) ne 1, cbp1)
        if(cbp1 gt 1) then p1[bp1]=0d
        if(smooth_sigma gt 0.1d) then begin
            psf=psf_gaussian(ndim=2,npix=((smooth_sigma*5) > 15),/double,/norm,fwhm=2.355d*smooth_sigma)
            p1=convol(p1,psf)
        endif

        if(r eq 0) then begin
            if(n_elements(flat) eq n_elements(image)) then begin
                flat_orders=dblarr(nx,n_ord)
                for o=0,n_ord-1 do begin
                    flat_ord_cur=fire_extract_order(flat,trace_coeff,o)
                    ord_ny=n_elements(flat_ord_cur[0,*])
                    flat_orders[*,o]=median(flat_ord_cur[*,5:ord_ny-6],dim=2)
                endfor
            endif else flat_orders=1d
            ;;;;flat_sm=(n_elements(flat) eq n_elements(image))? flat_norm/pixflat : 1d
            print,'Generating a synthetic frame'
            synframe=fire_arc_model_image(nx,ny,kwl3d,trace_coeff,fwhm,ord_ny=ord_ny,pixel_fwhm=pixel_fwhm,sigma=sigma,linetab=linetab,weight_tab=weight_tab,el_lab_tab=el_lab_tab,labellist=labellist,flat_orders=flat_orders,legendre=legendre,crd_map=crd_map,m0=m0,o_dim=o_dim)
            print,'Done'
        endif
        p2=synframe[corners[0,r]:corners[2,r],corners[1,r]:corners[3,r]]    
        bp2=where(finite(p2) ne 1, cbp2)
        if(cbp2 gt 1) then p2[bp2]=0d

        print,'Running image cross-correlation'
        mtx=correl_images_frac(p1,p2,xshift=xshift,yshift=yshift,mag=magnification,int5p=int5p,fourier=fourier)
        print,'Done'
        if(magnification gt 1) then begin
            krnl=psf_gaussian(ndim=2,npix=((magnification*2.5) > 11),fwhm=double(magnification),/norm,/double)
            mtx=convol(mtx,krnl,/edge_truncate)
        endif
        corrmat_analyze,mtx,x_best,y_best,max_corr,print=verbose
        if(r eq 0) then corr_mat=dblarr(n_elements(mtx[*,0]),n_elements(mtx[0,*]),n_reg)
        corr_mat[*,*,r]=mtx
        xy_off[*,r]=[x_best,y_best]/double(magnification)
        if(keyword_set(fit)) then begin
            xvec=(dindgen(2*xshift*magnification+1)-xshift*magnification)/double(magnification)
            yvec=(dindgen(2*yshift*magnification+1)-yshift*magnification)/double(magnification)
            yfit=mpfit2dpeak(mtx,c_yfit,xvec,yvec,/tilt,/moffat)
            xy_off[*,r]=[c_yfit[4:5]]
        endif
    endfor

    return, xy_off
end
