function fire_xnl2y,x,n,l,trace_coeff,x0=x0,y0=y0,truen=truen,nocorrect=nocorrect

s_x = size(x)
s_n = size(n)
s_l = size(l)

if(n_elements(x0) ne 1) then x0=1024
ny_img=2048l
if(n_elements(y0) ne 1) then y0=fix(ny_img*0.6)

if(array_equal(s_x[0:s_x[0]],s_n[0:s_x[0]]) ne 1 or $
   array_equal(s_x[0:s_x[0]],s_l[0:s_x[0]]) ne 1) then begin
    message,/inf,'X, N and L should have the same number of elements'
    return,-1
endif

n_ord=n_elements(trace_coeff[0,*,0])

n_inp=s_x[n_elements(s_x)-1]
x_vec=reform(x,n_inp)
n_vec=reform(n,n_inp)
if(keyword_set(truen)) then n_vec=32-1-n_vec
l_vec=reform(l,n_inp)

y_vec=dblarr(n_inp)+!values.d_nan
np=n_elements(trace_coeff[*,0,0])
for i=0,n_ord-1 do begin
    g_vec=where(n_vec eq i,cg_vec)
    if(cg_vec eq 0) then continue
    order_borders = dblarr(cg_vec,2)
    order_borders[*,0]=poly(x_vec[g_vec]-x0,trace_coeff[*,i,0])
    order_borders[*,1]=poly(x_vec[g_vec]-x0,trace_coeff[*,i,1])

    if(~keyword_set(nocorrect)) then begin
        order_mean=(order_borders[*,1]+order_borders[*,0])/2d
        dorder_mean=(poly(x_vec[g_vec],trace_coeff[1:*,i,0]*(1d +dindgen(np)))+poly(x_vec[g_vec],trace_coeff[1:*,i,1]*(1d +dindgen(np))))/2d
;;;;;        ord_ycorr=(order_mean-median(order_mean))/(abs(order_borders[0,1]-order_borders[0,0])*2.5)
        ord_ycorr=abs((order_mean-median(order_mean))/600d)*11d*(0.05d +abs(median(order_mean)-y0)/(ny_img))
;        ord_ycorr=abs(dorder_mean)*8d*(0.1d +abs(median(order_mean)-y0)/ny_img)
        order_borders[*,0]+=ord_ycorr
        order_borders[*,1]+=ord_ycorr
    endif

    y_vec[g_vec]=order_borders[*,0]+(order_borders[*,1]-order_borders[*,0])*l_vec[g_vec]
endfor

y=(s_x[0] le 1)? y_vec : reform(y_vec,s_x[1:s_x[0]])

return,y
end
