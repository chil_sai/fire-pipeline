function fire_order_detect_lines,order_img,fwhm,maxrms=maxrms,smy=smy,$
    curve_par=curve_par,inp_curve_par=inp_curve_par,$
    flux_all=good_flux_all,rms_arr=rms_arr,deg1=deg1,deg2=deg2,fast=fast,quadpoly=quadpoly

    if(n_elements(smy) ne 1) then smy=0
    if(n_elements(deg1) ne 1) then deg1=2
    if(n_elements(deg2) ne 1) then deg2=2
    a=size(order_img)
    Nx=a[1]
    Ny=a[2]

    if(smy gt 0) then begin
        for i=0,Nx-1 do order_img[i,*]=transpose(median(transpose(order_img[i,*]),smy))
    endif

    w=1
    tresh=3

    x=findgen(Nx)
    xflag=bytarr(Nx)
    binbgr=64
    nbinbgr=Nx/binbgr
    bin_ord_vec=dblarr(nbinbgr)

    ;background subtraction
    Ndeg=5
    bad_arc=where(finite(order_img) eq 0,cbad_arc) 
    if (cbad_arc gt 0) then order_img[bad_arc]=0
    for y=0,Ny-1 do begin
        order_vec=double(median(order_img[*,y],5))
        order_vec[0:3]=!values.d_nan
        order_vec[Nx-5:*]=!values.d_nan
        for xx=0,nbinbgr-1 do bin_ord_vec[xx]=min(order_vec[xx*binbgr:(xx+1)*binbgr-1],/nan)
;        f=robust_poly_fit(double(x),double(order_img[*,y]),Ndeg)
;        bgr=poly(double(x),f)
        bgr=interpol(bin_ord_vec,(dindgen(nbinbgr)+0.5)*binbgr,x,/spl)
        order_img[*,y]=order_img[*,y]-bgr
    endfor

    resistant_mean,order_img,3.0,img_level,img_rms
    neg_img=where(order_img lt -5*img_rms,cneg_img)
    if(cneg_img gt 0) then order_img[neg_img]=0.0

    ;; creating a reference spectrum
    arc_obs=median(order_img[*,Ny/2-2:Ny/2+2],dim=2)

    if(n_elements(inp_curve_par) eq 1) then begin
        deg1a=inp_curve_par.deg1
        deg2a=inp_curve_par.deg2
        kx=inp_curve_par.kx
        curve_par=inp_curve_par
    endif else begin
        ;; determining the curvature of arc lines as a function of wavelength by making
        ;; a piece-wise cross-correlation with the reference spectrum
    
        M = 100
        x_cross=findgen(2*M+1)-M
        mean_curve=fltarr(Ny)
        n_seg=4
        deg1a=deg1
        deg2a=deg2
        overlap=0.3
        skippix=0.0
        curve_arr=fltarr(n_seg,Ny)
        curve_img=fltarr(Nx,Ny)
    
        for L=0,Ny-1 do begin
                x_curve=fltarr(n_seg)
                for i=0,n_seg-1 do begin
                    nmin=(i eq 0)? skippix : (double(i)-overlap)*Nx/n_seg
                    nmax=(i eq n_seg-1)? Nx-skippix-1 : (double(i)+1.0+overlap)*Nx/n_seg
                    if(nmin lt 0) then nmin=0
                    if(nmax ge Nx) then nmax=Nx-1L
                    x_curve[i]=(nmax+nmin)/2.0
                    cross_cur=reverse(c_correlate($
                                   abs(order_img[nmin:nmax,L])*cosin_apod((nmax-nmin+1),10),$
                                   abs(arc_obs[nmin:nmax])*cosin_apod((nmax-nmin+1),10),x_cross))
                    if(max(cross_cur,/nan) ge 0.3) then begin
                        gau=mpfitpeak(x_cross,cross_cur,G,nterms=3,/gauss,/positive)
                        curve_arr[i,L]=(abs(G[1]) lt max(x_cross)*0.8)? G[1] : !values.f_nan
                    endif else curve_arr[i,L]=!values.f_nan
                endfor
        endfor
    
        ccs=sfit_2deg(curve_arr,deg1a,deg2a,xgrid=x_curve,ygrid=findgen(Ny),kx=kx)
        good_pnts=where(finite(curve_arr) eq 1)
        dcurve=robust_sigma((curve_arr-ccs)[good_pnts])
        bcurve=where(abs(curve_arr-ccs) gt 3*dcurve,cbcurve)
        if(cbcurve gt 0) then begin
            curve_arr_tmp=curve_arr
            curve_arr_tmp[bcurve]=!values.f_nan
            ccs=sfit_2deg(curve_arr_tmp,deg1a,deg2a,xgrid=x_curve,ygrid=findgen(Ny),kx=kx)
    
            dcurve=robust_sigma((curve_arr-ccs)[good_pnts])
            bcurve=where(abs(curve_arr-ccs) gt 3*dcurve,cbcurve)
            if(cbcurve gt 0) then begin
                curve_arr_tmp=curve_arr
                curve_arr_tmp[bcurve]=!values.f_nan
                ccs=sfit_2deg(curve_arr_tmp,deg1a,deg2a,xgrid=x_curve,ygrid=findgen(Ny),kx=kx)
            endif
        endif
        curve_par={deg1:deg1a,deg2:deg2a,kx:kx}
    endelse

    curve_img=poly2d(findgen(Nx),findgen(Ny),kx,deg1=deg1a,deg2=deg2a)
    mean_curve=curve_img[Nx/2,*]

    y=findgen(Ny)
    max_curve=abs(min(mean_curve,/nan)-max(mean_curve,/nan))

    ;;; finding emission lines, starting from the slit centre, going up then down the slit
    pos_lines_tmp=dblarr(Ny,5000)
    pos_lines_all=dblarr(Ny,5000)
    obsflux_lines_all=dblarr(Ny,5000)
    pos_number=intarr(Ny)
    kzz=1.0 ;;; 1.0
    oversample=5.0
    kzz=kzz*oversample
    for k=Ny/2,Ny-1 do begin
        vector=(order_img[*,k] > 0)
        find_peak,x,vector,0,ipix,xpk,ypk,bkpk,ipk 
        s_int=reverse(sort(ypk))
        cntrd_1d_gauss,congrid(vector,Nx*oversample,cub=-0.5),xpk*oversample,xpk_cnt,fwhm*kzz,/silent,fast=fast,quadpoly=quadpoly
        ypk=ypk[s_int]
        xpk=xpk[s_int]
        xpk_cnt=xpk_cnt[s_int]/oversample
        pos_lines_tmp[k,0:ipk-1]=xpk_cnt-curve_img[(0 > xpk_cnt < (Nx-1)),k]
        pos_lines_all[k,0:ipk-1]=xpk_cnt
        obsflux_lines_all[k,0:ipk-1]=ypk
        if(k eq Ny/2) then intens=ypk
        pos_number[k]=ipk
    endfor
    for k=0,Ny/2-1 do begin
        vector=(order_img[*,k] > 0)
        find_peak,x,vector,0,ipix,xpk,ypk,bkpk,ipk
        s_int=reverse(sort(ypk))
        cntrd_1d_gauss,congrid(vector,Nx*oversample,cub=-0.5),xpk*oversample,xpk_cnt,fwhm*kzz,/silent,fast=fast,quadpoly=quadpoly
        ypk=ypk[s_int]
        xpk=xpk[s_int]
        xpk_cnt=xpk_cnt[s_int]/oversample
        pos_lines_tmp[k,0:ipk-1]=xpk_cnt-curve_img[(0 > xpk_cnt < (Nx-1)),k]
        pos_lines_all[k,0:ipk-1]=xpk_cnt
        obsflux_lines_all[k,0:ipk-1]=ypk
        pos_number[k]=ipk
    endfor

    ;; sorting line positions
    ;; print,'Max(pos_number)=',max(pos_number)
    w=fwhm
    if(n_elements(maxrms) ne 1) then maxrms=fwhm
    good_pos=fltarr(400)
    rms_arr=fltarr(400)
    good_pos_all=fltarr(Ny,400)
    good_flux_all=fltarr(Ny,400)
    j=0
    ;;d_Ny=0.33*Ny ;;; maximal number of missing pixels along the slit (33%)
    d_Ny=0.25*Ny ;;; max.num of missing pixels along the slit

    ;; finding the nearest peak to the position of every emission line in the reference
    ;; spectrum taking into account the curvature information
    for k=0,max(pos_number) do begin
        pos_line_cur=pos_lines_tmp[Ny/2,k]
        good_line=where(abs(pos_lines_tmp-pos_line_cur) lt w, index)
        rmsline=((index gt Ny-d_Ny) and (pos_line_cur gt 0))? $
            sqrt(total((pos_lines_tmp[good_line]-pos_line_cur)^2)/index) : maxrms*2.
        if ((index gt Ny-d_Ny) and (pos_line_cur gt 0) and (rmsline lt maxrms)) then begin
            rms_arr[j]=rmsline
            good_pos[j]=pos_line_cur
            good_pos_all[*,j]=!values.f_nan
            good_flux_all[*,j]=!values.f_nan
            for n=0L,index-1L do begin
                idx=array_indices([Ny,5000],good_line[n],/dim)
                good_pos_all[idx[0],j]=pos_lines_all[good_line[n]]
                good_flux_all[idx[0],j]=obsflux_lines_all[good_line[n]]
            endfor
;            print,'adding k,n_r',k,index,pos_line_cur,pos_lines_all[*,k],pos_lines_all[good_line]
            j=j+1
        endif
    endfor
    r=where(good_pos gt 0,n_line)
    print,'Number of accepted lines: ',N_line
    if(n_line gt 0) then begin
        good_pos=good_pos[r]
        rms_arr=rms_arr[r]
        good_pos_all=good_pos_all[*,r]
        good_flux_all=good_flux_all[*,r]
    endif else return,-1


    max_N_line = 100

    if(N_line gt max_N_line) then begin
        N_line=max_N_line
        good_pos=good_pos[0:max_N_line-1]
        good_pos_all=good_pos_all[*,0:max_N_line-1]
    endif
    return,good_pos_all
end
