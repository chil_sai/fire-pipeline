function poly3d_echelle,x,y,z,c0,deg1=deg1,deg2=deg2,deg3=deg3,irregular=irregular,$
    legendre=legendre,crd_map=crd_map,cbadpoints=cb_crd,m0=m0,o_dim=o_dim

if(n_elements(m0) ne 1) then m0=1 ;; top order is N=1 (11 for FIRE, 6 for MagE and ESI)
if(n_elements(o_dim) ne 1) then o_dim=1 ;; dimension of the order axis (1 to 3), default=2
if(n_elements(crd_map) ne 6) then crd_map=[[0d,0d,0d],[1d,1d,1d]]

c=c0
s=size(c)
if(s[0] eq 3) then begin
    deg1a=s[1]-1L
    deg2a=s[2]-1L
    deg3a=s[3]-1L
    c1=c ;;;;transpose(c)
endif else message,'Only 3D coefficient arrays are supported for a moment'

nx=n_elements(x)
ny=n_elements(y)
nz=n_elements(z)
if(keyword_set(irregular) and ((nx ne ny) or (nx ne nz))) then message,'X and Y should have equal number of elements when IRREGULAR is set'

if(keyword_set(irregular)) then begin
    x_crd=x
    y_crd=y
    z_crd=z
endif else begin
    x_crd=reform(transpose(congrid(transpose([x]),ny,nx,nz)),nx*ny*nz)
    y_crd=reform(congrid(transpose(y),nx,ny,nz),nx*ny*nz)
    z_crd=reform(congrid(transpose(z),nx,ny,nz),nx*ny*nz)
endelse

case o_dim of
    1 : k0=1d/(m0+(x_crd))
    2 : k0=1d/(m0+(y_crd))
    3 : k0=1d/(m0+(z_crd))
endcase

if(crd_map[0,0] ne 0d and crd_map[0,1] ne 1d) then x_crd=(x_crd-crd_map[0,0])/crd_map[0,1]
if(crd_map[1,0] ne 0d and crd_map[1,1] ne 1d) then y_crd=(y_crd-crd_map[1,0])/crd_map[1,1]
if(crd_map[2,0] ne 0d and crd_map[2,1] ne 1d) then z_crd=(z_crd-crd_map[2,0])/crd_map[2,1]

if(keyword_set(legendre)) then begin
    g_crd=where((x_crd ge -1d and x_crd le 1d) and $
                (y_crd ge -1d and y_crd le 1d) and $
                (z_crd ge -1d and z_crd le 1d),cg_crd,compl=b_crd,ncompl=cb_crd)
    if(cb_crd gt 0) then message,/inf,string(cb_crd,format='(i)')+' points have coordinates outside the [-1,1] range. Returning NaNs for them.'
endif else begin
    cb_crd=0l
    b_crd=[-1l]
    g_crd=lindgen(n_elements(x_crd))
    cg_crd=n_elements(x_crd)
endelse

res = (keyword_set(irregular))? dblarr(nx) : dblarr(nx*ny*nz)

xpoly_cache=dblarr(cg_crd,deg1a+1)
xpoly_cache_flag=bytarr(deg1a+1)
ypoly_cache=dblarr(cg_crd,deg2a+1)
ypoly_cache_flag=bytarr(deg2a+1)
zpoly_cache=dblarr(cg_crd,deg3a+1)
zpoly_cache_flag=bytarr(deg3a+1)

for i=0, deg1a do begin
    if(xpoly_cache_flag[i] eq 0) then begin
        xpoly_cache[*,i]=(keyword_set(legendre))? legendre(x_crd[g_crd],i) : x_crd^i
        xpoly_cache_flag[i]=1
    endif
    for j=0, deg2a do begin
        if(ypoly_cache_flag[j] eq 0) then begin
            ypoly_cache[*,j]=(keyword_set(legendre))? legendre(y_crd[g_crd],j) : y_crd^j
            ypoly_cache_flag[j]=1
        endif
        for k=0, deg3a do begin
            if(zpoly_cache_flag[k] eq 0) then begin
                zpoly_cache[*,k]=(keyword_set(legendre))? legendre(z_crd[g_crd],k) : z_crd^k
                zpoly_cache_flag[k]=1
            endif
            if(c1[i,j,k] eq 0d) then continue
            res[g_crd]+= xpoly_cache[*,i]*ypoly_cache[*,j]*zpoly_cache[*,k]*c1[i,j,k]
        endfor
    endfor
endfor
res[g_crd]*=k0[g_crd]

if(cb_crd gt 0) then res[b_crd]=!values.d_nan

if(not keyword_set(irregular)) then res=reform(res,nx,ny,nz)
return,res
end
