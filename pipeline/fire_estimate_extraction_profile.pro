function fire_fitfunc_extraction_profile,p,prof_data=prof_data,prof_ref=prof_ref,prof_model=prof_model
;;;; profile fitting function to evaluate by MPFIT
    n_points=n_elements(prof_data)
    gp=where(finite(prof_data) eq 1, cgp, compl=bp, ncompl=cbp)
    if(p[2] gt 0.1d) then begin
        krnl=psf_gaussian(ndim=1,npix=n_points,st_dev=p[2],centroid=(p[1]+(n_points-1)/2d),/norm,/double)
        prof_model=convol(prof_ref,krnl,/edge_wrap)
    endif else begin
        prof_model=shift_s(prof_ref,p[1])
    endelse
;    prof_model=p[0]*prof_model+p[3]
;    plot,prof_data,xs=1,ys=3,chars=2
;    oplot,prof_model,col=128,thick=3
    err=(prof_data-prof_model)^2
    if(cbp gt 0) then err[bp]=0d
    return,err
end

function fire_extraction_profile_slit,slit_lin_2d,wl,$
    slitmask=slitmask,maskoh=maskoh,$
    nsegments=nsegments,fulltrace=fulltrace,$
    slit_profile_seg=slit_profile_seg,$
    m0=m0,legendre=legendre,crd_map=crd_map
    
    if(n_elements(nsegments) ne 1) then nsegments=16

    s_slit=size(slit_lin_2d)
    if(n_elements(slitmask) ne 1) then slitmask=fix(slit_lin_2d*0)
    if(s_slit[1] ne n_elements(wl)) then begin
        message,/inf,'Extracted slit size does not correspond to the wavelength vector'
        return,!values.f_nan
    endif
    slit_lin_op=slit_lin_2d
    if (keyword_set(maskoh)) then begin
        lines_oh=read_asc(getenv('FIRE_PIPELINE_PATH')+'calib_FIRE/linelists/linesOH_R2k_HITRAN_H2O_nm.tab')
        lines_oh=lines_oh[*,where(lines_oh[2,*] gt 1e-1)]
        flag=mask_emission_lines(wl,transpose(lines_oh[0,*]),50.0,/kms)
        slitmask=slitmask+transpose(congrid(transpose(flag),s_slit[2],s_slit[1]))
    
        mask_bad=where(slitmask gt 0, cmask_bad)
        if(cmask_bad gt 0) then slit_lin_op[mask_bad]=!values.d_nan
    endif

    if(keyword_set(fulltrace)) then begin
        slit_profile_seg=dblarr(nsegments,s_slit[2])
        skippix=10
        overlap=0.2
        for i=0,nsegments-1 do begin
            nmin = (i eq 0)? skippix : fix((double(i)-overlap)*s_slit[1]/nsegments)
            nmax = (i eq nsegments-1)? s_slit[1]-skippix-1 : fix((double(i)+1.0+overlap)*s_slit[1]/nsegments)
            if(nmin lt 0) then nmin=0
            if(nmax ge s_slit[1]) then nmax=s_slit[1]-1
            slit_profile_seg[i,*]=transpose(median(slit_lin_op[nmin:nmax,*],dim=1))
            bad_slit=where(finite(slit_profile_seg[i,*]) ne 1, cbad_slit)
            if(cbad_slit gt 0) then slit_profile_seg[i,bad_slit]=0d
            slit_profile_seg[i,*]=slit_profile_seg[i,*]/total(abs(slit_profile_seg[i,*]))
;oplot,slit_profile_seg[i,*],col=40+i*10
        endfor
        slit_profile=congrid(slit_profile_seg,s_slit[1],s_slit[2],/interp)
    endif else begin
        slit_profile=median(slit_lin_op,dim=1)
        bad_slit=where(finite(slit_profile) ne 1, cbad_slit)
        if(cbad_slit gt 0) then slit_profile[bad_slit]=0d
        slit_profile=slit_profile/total(abs(slit_profile))
    endelse

    return,slit_profile
end

function fire_estimate_extraction_profile,star_extr_2d_orig,kwl3d,$
    flatn_extr_2d=flatn_extr_2d_orig,flat_thr=flat_thr,$
    slit_function=slit_function,slit_corrected=slit_corrected,$
    ref_ord=ref_ord,truen=truen,maskoh=maskoh,$
    original_extraction_profiles=extr_prof_all,$
    m0=m0,legendre=legendre,crd_map=crd_map,$
    slits_lin_2d=slits_lin_2d,wl_lin=wl_new,wlmap_orders=wlmap_orders,$
    nsegments=nsegments,fulltrace=fulltrace,verbose=verbose

if(n_elements(nsegments) ne 1) then nsegments=16
if(n_elements(flat_thr) ne 1) then flat_thr=0.0d
star_extr_2d=star_extr_2d_orig
s_star=size(star_extr_2d)
flatn_extr_2d = (n_elements(flatn_extr_2d_orig) ne n_elements(star_extr_2d))? star_extr_2d*0d +1d : flatn_extr_2d_orig
badflat=where(flatn_extr_2d lt flat_thr, cbadflat)
if(cbadflat gt 0) then star_extr_2d[badflat]=!values.f_nan

n_ord=s_star[3]
slits_lin_2d=star_extr_2d*0.0
slits_mask_2d=fix(star_extr_2d*0.0)

if(n_elements(ref_ord) ne 1) then ref_ord=19-1 ;; K band
ref_ord_n=ref_ord
if(keyword_set(truen)) then ref_ord_n=32-1-ref_ord_n

if(keyword_set(slit_corrected)) then begin
    slit_function_2d=1d
endif else begin
    if(n_elements(slit_function) ne 1) then $
        slit_function=mrdfits(getenv('FIRE_PIPELINE_PATH')+'calib_FIRE/slits/slit_function_e0_45.fits',1,/silent)
    slit_function_2d=rebin(transpose(slit_function.slit_fit),s_star[1],s_star[2])
endelse

wl_new=dblarr(s_star[1],n_ord)
wlmap_orders=dblarr(s_star[1],s_star[2],n_ord)

extr_prof_all=(keyword_set(fulltrace))? dblarr(s_star[1],s_star[2],n_ord) : dblarr(s_star[2],n_ord)
if(keyword_set(fulltrace)) then extr_prof_seg=dblarr(nsegments,s_star[2],n_ord)

for i=0,n_ord-1 do begin
    if(keyword_set(verbose)) then message,/inf,'Linearising order: '+string(31-i,format='(i2)')
    slits_lin_cur=fire_order_linearisation(star_extr_2d[*,*,i]/slit_function_2d,i,kwl3d,wl,/autowl,wlmap=wlmap_cur,legendre=legendre,m0=m0,crd_map=crd_map)
    slits_lin_2d[*,*,i]=slits_lin_cur
    wl_new[*,i]=wl
    wlmap_orders[*,*,i]=wlmap_cur
    flat_norm_lin=fire_order_linearisation(flatn_extr_2d[*,*,i],i,kwl3d,wl,/autowl,legendre=legendre,m0=m0,crd_map=crd_map) ;;; slit_function_2d
    flat_norm_lin=flat_norm_lin/max((median(flat_norm_lin,7))[7:s_star[1]-9,7:s_star[2]-8],/nan)
    flat_mask_lin=fix(0*flat_norm_lin)
    low_flat=where(finite(flat_norm_lin) ne 1 or flat_norm_lin lt 0.1, clow_flat)
    if(clow_flat gt 0) then flat_mask_lin[low_flat]=1
    slits_mask_2d[*,*,i]=flat_mask_lin
    prof_cur=fire_extraction_profile_slit(slits_lin_cur,wl,$
        slitmask=slits_mask_2d[*,*,i],maskoh=maskoh,nsegments=nsegments,$
        fulltrace=fulltrace,slit_profile_seg=prof_seg_cur,m0=m0,legendre=legendre,crd_map=crd_map)
    if(keyword_set(fulltrace)) then begin
        extr_prof_all[*,*,i]=prof_cur 
        extr_prof_seg[*,*,i]=prof_seg_cur
    endif else extr_prof_all[*,i]=prof_cur
endfor

prof_ref=(keyword_set(fulltrace))? transpose(extr_prof_all[s_star[1]/2,*,ref_ord_n]) : extr_prof_all[*,ref_ord_n]
extr_prof_fit=extr_prof_all

parinfo=[{step:1d-2,limits:[1d-2,3d],limited:[1,1],fixed:0,relstep:0},$
         {step:1d-2,limits:[-10d,10d],limited:[1,1],fixed:0,relstep:0},$
         {step:1d-2,limits:[0d,3d],limited:[1,1],fixed:0,relstep:0},$
         {step:1d-2,limits:[-1d,1d],limited:[1,1],fixed:0,relstep:0}]
start=[1d,0d,0.4d,0d]

if(keyword_set(verbose)) then message,/inf,'Using order #'+string(31-ref_ord_n,format='(i2)')+' as a PSF reference'

if(keyword_set(fulltrace)) then extr_prof_fit_seg=dblarr(nsegments,s_star[2],n_ord)

for i=0,n_ord-1 do begin
    if(i eq ref_ord_n and (not keyword_set(fulltrace))) then continue
    for x=0,((keyword_set(fulltrace))? nsegments-1 : 0) do begin
        prof_data=((keyword_set(fulltrace))? transpose(extr_prof_seg[x,*,i]):extr_prof_all[*,i])
        functArgs={prof_data:prof_data,prof_ref:prof_ref}
        c=mpfit('fire_fitfunc_extraction_profile',start,functargs=functArgs,parinfo=parinfo,status=mpfstat,/qui); ,xtol=1d-10,ftol=5d-7)
        if(mpfstat ne 0) then begin
            t=fire_fitfunc_extraction_profile(c,prof_data=prof_data,prof_ref=prof_ref,prof_model=prof_model)
            if(keyword_set(fulltrace)) then extr_prof_fit_seg[x,*,i]=transpose(prof_model) else extr_prof_fit[*,i]=prof_model
        endif else begin
            message,/inf,'MPFIT failed for the order: '+string(31-i,format='(i2)')+' segment='+string(x+1,format='(i3)')+' with status='+string(mpfstat,format='(i4)')+'. Keeping the empirically determined profile'
            extr_prof_fit[*,i]=prof_data
        endelse
    endfor
    if(keyword_set(fulltrace)) then begin
        extr_prof_fit[*,*,i]=congrid(extr_prof_fit_seg[*,*,i],s_star[1],s_star[2],cubic=-0.5)
        extr_prof_fit[*,*,i]=extr_prof_fit[*,*,i]/congrid(reform(total(abs(extr_prof_fit[*,*,i]),2),s_star[1],1),s_star[1],s_star[2])
    endif
endfor

return,(keyword_set(fulltrace)?extr_prof_fit : rebin(reform(extr_prof_fit,1,s_star[2],n_ord),s_star[1],s_star[2],n_ord))

end
