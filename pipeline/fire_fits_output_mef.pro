pro fire_fits_output_mef,filename,data,wl,hdr,$
    error=error,atmtrans=atmtrans,exptime=exptime,$
    fluxcorr=fluxcorr,fcorr_vec=flux_vec,texp=texp,verbose=verbose

    if(n_elements(exptime) ne 1) then exptime=1.0

    hdr_out=hdr
    sxdelpar,hdr_out,'NAXIS2'
    sxdelpar,hdr_out,'NAXIS1'
;    hdr_out=hdr_out[3:*]
;    mkhdr,h_tst,data[*,*,0],/extend,/image
;    hdr_out=[h_tst[0:4],hdr_out]
    sxaddpar,hdr_out,'CTYPE1','WAVE'
    sxaddpar,hdr_out,'CUNIT1','nm'
    sxaddpar,hdr_out,'CRPIX1',1d
    if(n_elements(texp) ne 1) then texp=sxpar(hdr,'EXPTIME')
    airmass=sxpar(hdr,'AIRMASS')

    if(keyword_set(fluxcorr) and n_elements(flux_vec) ne 1) then begin
        message,/inf,'No flux calibration information is provided. Flux calibration is disabled'
        fluxcorr=0
    endif
        ;flux_vec=mrdfits(getenv('FIRE_PIPELINE_PATH')+'calib_FIRE/sensitivity/echelleQH_flux_corr.fits',1,/silent)

    sxaddpar,hdr_out,'BUNIT',(keyword_set(fluxcorr)?'1e-17 erg/cm^2/c/Angstrom':'counts'),after='CUNIT1'

    writefits,filename,0

    s_data=size(data)
    n_wl=s_data[1]
    n_pix=(s_data[0] eq 1)? 1 : s_data[2]
    n_ord=(s_data[0] gt 2)? s_data[3] : 1
    sxaddpar,hdr_out,'NAXIS1',n_wl,after='NAXIS'
    if(n_pix gt 1) then begin
        sxaddpar,hdr_out,'NAXIS2',n_pix,after='NAXIS1'
        sxaddpar,hdr_out,'CTYPE2',''
        sxaddpar,hdr_out,'CUNIT2',''
        sxaddpar,hdr_out,'CRPIX2',1.0
        sxaddpar,hdr_out,'CRVAL2',0.0
        sxaddpar,hdr_out,'CDELT2',1.0
        sxaddpar,hdr_out,'CD2_2',1.0
    endif

    for i=0,n_ord-1 do begin
        ordname=(n_ord gt 2)? 'FIREOR'+string(31-i,format='(i2.2)') : (keyword_set(error)?'ERROR':'FLUX')
        sxaddpar,hdr_out,'EXTVER',1,after='BITPIX'
        sxaddpar,hdr_out,'EXTNAME',ordname,after='EXTVER'
        sxaddpar,hdr_out,'CRVAL1',wl[0,i]
        sxaddpar,hdr_out,'CDELT1',wl[1,i]-wl[0,i]
        sxaddpar,hdr_out,'CD1_1',wl[1,i]-wl[0,i]
        if(keyword_set(fluxcorr)) then begin
            corrvec=interpol(flux_vec.flux_erg,flux_vec.wave,wl[*,i]*10d)/mage_calc_ext_lco(wl[*,i]*10d,airmass)
            corr_arr=rebin(corrvec,n_wl,n_pix)/texp*1d17
        endif else corr_arr=1d
        mwrfits,float(data[*,*,i]*corr_arr/exptime),filename,hdr_out,silent=(i gt 0 or ~keyword_set(verbose))
        if(i eq 0 and n_ord eq 1 and n_elements(error) eq n_elements(data)) then begin
            sxaddpar,hdr_out,'EXTNAME','ERROR'
            mwrfits,float(error*corr_arr/exptime),filename,hdr_out,/silent
        endif
        if(i eq 0 and n_ord eq 1 and n_elements(atmtrans) eq n_elements(data)) then begin
            hdr_out_tel=hdr_out
            sxaddpar,hdr_out_tel,'EXTNAME','ATMTRANS'
            sxaddpar,hdr_out_tel,'BUNIT',''
            mwrfits,float(atmtrans),filename,hdr_out_tel,/silent
        endif
    endfor
end
