function fire_convol_order,wl,spectrum,sigma_center,nm=nm,_extra=extra_kw

    if(sigma_center le 0) then return,spectrum

    n_wl=n_elements(wl)
    velvec=dblarr(n_wl)
    ;;disvec=velvec + sigma_center*(0.55d - 0.45d*cos(dindgen(n_wl)/(n_wl-1)*2*!dpi))
    disvec = interpol([7.0,8.5,14.,12.,7.1,5.0]*sigma_center/14.,$
;;    disvec = interpol(([7.0/2.,8.,14.,9.,7.1,5.0/2.]*sigma_center/14.)>2.0,$
                      (0.5+findgen(6))/6.,findgen(n_wl)/(n_wl-1))
    if(~keyword_set(nm)) then disvec=disvec/299792.458d*wl

    spec_conv=conv_multi(spectrum,velScale=wl[1]-wl[0],$
        velvec=velvec,disvec=disvec,nseg=6,_extra=extra_kw)    

    return, spec_conv
end
