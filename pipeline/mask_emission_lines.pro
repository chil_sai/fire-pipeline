function mask_emission_lines,wl,lines_wl,w,kms=kms
    if(n_params() eq 2) then w=0.15 ;;; 0.15nm
    flag=byte(wl*0)
    for i=0,n_elements(lines_wl)-1 do begin 
        b_l=(keyword_set(kms))? $
            where(299792.5d*abs(wl-lines_wl[i])/wl lt w, cb_l) : $
            where(abs(wl-lines_wl[i]) lt w, cb_l)
        if(cb_l gt 0) then flag[b_l]=1
    endfor
    return,flag
end
