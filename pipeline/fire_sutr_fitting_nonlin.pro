;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;  FIRE_SUTR_FITTING_NONLIN -- IDL implementation of the up-the-ramp fitting routine
;     including the non-linearity correction using pre-computed calibrations
;     for the FIRE (Folded Port Infrared Echelette) spectrograph
;  Based on the mmfixen_nonlin procedure from the CfA MMIRS pipeline
;
;
;
;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

pro fire_sutr_fitting_nonlin,inpfile0,datapath=datapath,outfile,linear=linear,keepfirst=keepfirst,$
    verbose=verbose,debug=debug,$
    compress=compress,tmpdir=tmpdir,clean=clean

keepfirst=1
if(n_elements(tmpdir) ne 1) then tmpdir='/tmp/'
if(n_elements(first) ne 1) then first=1
if(keyword_set(debug)) then verbose=1

tmpfilename=''
if(~file_test(inpfile0)) then begin ;;; no file under the "inpfile" name
    inp_f_arr=file_search(inpfile0+'*')
    if(strlen(inp_f_arr[0]) gt 0) then begin
        inpext=strmid(inp_f_arr[0],strlen(inp_f_arr[0])-3)
        if(inpext eq '.xz') then inpcompress='unxz'
        if(inpext eq '.gz') then inpcompress='gunzip'
        if(inpext eq 'bz2') then inpcompress='bunzip2'
        tmpfilename=tmpdir+'fire_pipe_'+string(randomu(seed)*1d+7,format='(i8.8)')+'.fits'
        if(keyword_set(verbose)) then print,'Decompressing '+inp_f_arr[0]+' ...',format='(a,$)'
        spawn,inpcompress+' -c '+inp_f_arr[0]+' >'+tmpfilename
        if(keyword_set(verbose)) then print,'done'
        inpfile=tmpfilename
    endif else begin
        message,'Input file '+inpfile0+' not found. Cannot continue up-the-ramp fitting.'
    endelse 
endif else inpfile=inpfile0

h_img=headfits(inpfile)
sxaddpar,h_img,'XTENSION','IMAGE',after='SIMPLE'
sxaddpar,h_img,'EXTNAME','FIRESPECTRUM',after='XTENSION'
sxdelpar,h_img,'SIMPLE'
if(n_elements(datapath) ne 1) then datapath='/Volumes/ENFUEGO/FIRE/BAADE/H2RG-C001-ASIC-FIRE001/'
readmode=sxpar(h_img,'READMODE')
datadir=datapath
datadir+= (readmode eq 'SUTR')? '/UpTheRamp/' : '/FSRamp/'
datadir+= sxpar(h_img,'RAWPATH')+'/'
gain_str=sxpar(h_img,'GAIN')
gain=double(strmid(gain_str,0,3))
rdnoise=15.0
n_img=(readmode eq 'SUTR')? sxpar(h_img,'NRAMP') : 2
ramp=10.6086d

h_dummy=['SIMPLE  =                    T',$
         'BITPIX  =                   16',$
         'NAXIS   =                    0',$
         'EXTEND  =                    T',$
         'END                           ']

fowl_k=(strmid(readmode,0,6) eq 'Fowler')? fix(strmid(readmode,7)) : 1 ;; for a moment this is ignored and is always assumed to be 1
rd_filenames=datadir+'H2RG_R01_M'+string(indgen(n_img)+1,format='(i2.2)')+'_N01.fits'
if(n_img gt 99) then $
    rd_filenames[99:*]=datadir+'H2RG_R01_M'+string(indgen(n_img-99)+100,format='(i3.3)')+'_N01.fits'
files_tmp=file_search(datadir+'*.fits*')
if(strmid(files_tmp[0],strlen(files_tmp[0])-3) eq '.gz') then rd_filenames+='.gz'

detector_id = 'H2RG'
if(detector_id eq 'H2RG') then clean=1 ; 1 ;;; perform sigma-clipping for cleaning read-outs

h_img=h_img[where(strlen(strcompress(h_img,/remove_all)) gt 0)]

sxaddpar,h_img,'LONGSTRN','OGIP 1.0',' The OGIP long string convention may be used.',after='XTENSION'

inttime=fltarr(n_img)
img=float(mrdfits(rd_filenames[0],0,h_raw,/silent))+32768.0
img=refpix(img,namps=namps)
nx=long(sxpar(h_img,'NAXIS1'))
ny=long(sxpar(h_img,'NAXIS2'))
sxdelpar,h_img,'BZERO'
sxaddpar,h_img,'BUNIT','counts/s'

img_cube=fltarr(nx,ny,n_img)
img_cube[*,*,0]=img
inttime[0]=sxpar(h_raw,'INTTIME')
if(keyword_set(verbose)) then print,'Reading up-the-ramp readouts...',format='(a,$)'
for i=1,n_img-1 do begin
    t=float(mrdfits(rd_filenames[i],0,h_tmp,/silent))+32768.0
    inttime[i]=sxpar(h_tmp,'INTTIME')
    t=refpix(t,namps=namps)
    img_cube[*,*,i-(first-1)]=t
endfor
if(keyword_set(verbose)) then print,'done'

if(strlen(tmpfilename) gt 5) then file_delete,tmpfilename,/allow_nonexistent

if(not keyword_set(keepfirst)) then begin
    img_cube=img_cube[*,*,1:*]
    n_img=n_img-1
endif

sat_mask=intarr(nx,ny) ;;; saturation
crh_mask=intarr(nx,ny) ;;; cosmic ray hits
mask_image=bytarr(nx,ny)

sxaddpar,h_img,'SOFTWARE',get_fire_pipeline_version()
sxaddpar,h_img,'NREADOUT',n_img,' number of readouts used'

cbad_diff_2=0
cgood_diff_2=n_img-1
cbad_readouts=0
cgood_readouts=n_img
good_readouts=indgen(cgood_readouts)
bad_readouts=[-1]

if(keyword_set(debug)) then stop

if(not keyword_set(linear)) then begin
    gain_suffix=(abs(gain-3.8d) lt 0.1)? '_logain' : ''
    t_cal=mrdfits(getenv('FIRE_PIPELINE_PATH')+'calib_FIRE/detector/'+detector_id+'/nonlinearity/ramp'+strcompress(string(ramp,format='(i)'),/remove)+gain_suffix+'_nonlin.fits',1,/silent)
    if(gain lt 3.7) then t_cal.saturation*=1.15
    for i=0,n_img-1 do begin
        if(keyword_set(verbose)) then print,'Nonlin correction for readout N=',string(i+1,form='(i3)'),format='(a,a,%"\r",$)'
        corr_img=fire_correct_nonlinearity(img_cube[*,*,i],calib=t_cal,/nobias) 
        corr_img=transpose(corr_img)
        img_cube[*,*,i]=corr_img
        sat_pix=where(finite(corr_img) ne 1 and sat_mask eq 0,csat_pix)
        if(csat_pix gt 1) then sat_mask[sat_pix]=i+1
    endfor
    if(keyword_set(verbose)) then print,''
endif

if(keyword_set(debug)) then stop

sxaddpar,h_img,'NLINCORR',1-keyword_set(linear),' non-linearity correction status'

if(n_img eq 1) then begin
    img_final=img_cube[*,*,0]/ramp
endif 
if(n_img eq 2) then begin
    img_final=(img_cube[*,*,1]-img_cube[*,*,0])/(inttime[1]-inttime[0])
endif
if(n_img gt 2) then begin
    img_final=img_cube[*,*,0]*!values.f_nan
    if(keyword_set(verbose)) then print,'Computing the difference...',format='(a,$)'
    for k=0,n_img-2 do img_cube[*,*,k]=(img_cube[*,*,k+1]-img_cube[*,*,k])/ramp
    if(keyword_set(verbose)) then print,'done'

    block_size=64L
    n_bl_max=2048L/block_size
    cbadpixtot=0L
    nsig=9.0


    for iter=0,keyword_set(clean)*2 do begin
        if(keyword_set(clean) and keyword_set(verbose)) then print,'Removing cosmic ray hits. Iteration '+string(iter+1,format='(i2)')
        for n_bl=0L,n_bl_max-1L do begin
            ymin=n_bl*block_size
            ymax=ymin+block_size-1L
            idx_off=block_size*2048L*n_bl
            if(keyword_set(clean) and iter gt 0) then begin
;                print,'Cleaning: '
                img_fit_frag=img_final[*,ymin:ymax]
                for n=0L,n_img-2L do begin
                    medval=0.0; median(img_cube[*,ymin:ymax,n]-img_fit_frag)
                    stdval=robust_sigma(img_cube[*,ymin:ymax,n]-img_fit_frag)
;;;;                    badpix=where(abs(img_cube[*,ymin:ymax,n]-img_fit_frag-medval) gt sqrt((nsig*stdval)^2+(rdnoise/ramp)^2*0.),cbadpix)
                    badpix=where(abs(img_cube[*,ymin:ymax,n]-img_fit_frag-medval) gt nsig*sqrt(abs(img_fit_frag)*ramp+16.0*rdnoise^2)/ramp,cbadpix)
                    cbadpixtot=cbadpixtot+cbadpix
                    if(cbadpix gt 0) then img_cube[badpix+idx_off+nx*ny*n]=img_fit_frag[badpix]
;                    print,n_bl,n,cbadpix,n_elements(img_fit_frag),nsig*rdnoise/ramp
                endfor
            endif
            for n=0L,n_img do begin
                sat_pix=where(sat_mask[*,ymin:ymax] eq n,csat_pix)
                if(keyword_set(verbose)) then print,'block=',n_bl,' N=',n,' csat_pix=',csat_pix,format='(a,i4,a,i3,a,i11,%"\r",$)'
                if(csat_pix gt 0) then begin
                    np_max=(n eq 0)? n_img-1L : ((n-2L) > 1L)
                    idx_arr=reform((sat_pix # (lonarr(np_max)+1L))+$
                                   ((lonarr(csat_pix)+1L) # (lindgen(np_max)*nx*ny)), $
                                       csat_pix*np_max)
                    flux_arr=reform(img_cube[idx_arr+idx_off],csat_pix,np_max)
                    ;;;ierr_arr=ramp*gain/(2.0*((flux_arr*gain*ramp) > 0) + rdnoise^2)*0.0+1d ;;; something wrong here

                    if(np_max gt 2) then begin
;;;; in the read-noise limited case, the inverse weighting of diff read-outs
;;;; is described by a quadratic function as a number of readout
;;;; it is symmetric, with the maximum value of 1 in the middle of the
;;;; exposure declining on both sides. The value at 0 (and at N_max=N) is
;                        w_min = 1d / (0.51550615d + n*0.24976740d)
;;;; the quadratic function is then described as:
;                        a_coeff = (1d -w_min)*4d/(np_max-1d)
;                        b_coeff = -a_coeff/(np_max-1d)
;                        w_arr = 1.0/transpose(w_min+a_coeff*dindgen(np_max)+b_coeff*dindgen(np_max)^2)^2
;;;;;;;;
                        v_mat=diag_matrix(dblarr(np_max)+2d) +diag_matrix(dblarr(np_max-1)-1,-1)+diag_matrix(dblarr(np_max-1)-1,1)
                        w_arr = (dblarr(np_max)+1d) # invert(v_mat)
                        ierr_arr = congrid(w_arr,csat_pix,np_max)
                    endif else $
                        ierr_arr = flux_arr*0+1d

                    flux_vec=((n ge 1) and (n le 3))? flux_arr : total(flux_arr*ierr_arr,2)/total(ierr_arr,2)
                    if(keyword_set(debug)) then stop
                    img_final[sat_pix+idx_off]=flux_vec[*]
                endif
            endfor
        endfor
    endfor
    if(keyword_set(verbose)) then print,''
    if(keyword_set(verbose)) then print,'Number of bad pixels: ',cbadpixtot
endif

if(not keyword_set(linear)) then img_final=transpose(img_final)
img_final=refpix(img_final)
writefits,outfile,0,h_dummy
mwrfits,img_final,outfile,h_img
if(keyword_set(debug)) then stop

if(not keyword_set(compress)) then return

case compress of
    'gz': spawn,'gzip -f '+outfile
    '.gz': spawn,'gzip -f '+outfile
    'bz2': spawn,'bzip2 -f '+outfile
    'zip': spawn,'zip '+outfile+'.zip '+outfile
    ELSE: spawn,compress+' '+outfile
endcase

end

