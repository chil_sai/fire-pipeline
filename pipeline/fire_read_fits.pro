function fire_read_fits,filename,header,silent=silent,correct_header=correct_header

    result=readfits(filename,header,silent=silent)
    preproc = (sxpar(header,'NAXIS') eq 0)
    
    if(preproc) then begin
        h0=header[0]
        result=mrdfits(filename,1,header,silent=silent)
        bunit=sxpar(header,'BUNIT',count=cb)
        if(cb eq 1) then begin
            exptime=sxpar(header,'EXPTIME')
            if(bunit eq 'counts/s' and exptime gt 0) then begin
                result*=exptime
                sxaddpar,header,'BUNIT','counts'
            endif
        endif
        if(keyword_set(correct_header)) then begin
            sxdelpar,header,'EXTNAME'
            header[0]=h0
        endif
    endif
    result[*,0:3]=!values.f_nan
    result[0:3,*]=!values.f_nan
    result[2044:*,*]=!values.f_nan
    result[*,2044:*]=!values.f_nan

    return, result
end
