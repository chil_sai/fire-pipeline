function fire_clean_trace_array,xpos,trace_array,ndeg=ndeg,ythr=ythr
    if(n_elements(ndeg) ne 1) then ndeg=4
    if(n_elements(ythr) ne 1) then ythr=3.0 ;;; max deviation from the poly trace
    s_t=size(trace_array)
    n_x=s_t[1]
    n_ord=s_t[2]

    trace_array_clean=trace_array
    for z=0,1 do begin
        for i=0,n_ord-1 do begin
            xx=xpos[*,i]
            minx=min(xx,max=maxx)
            yy=trace_array[*,i,z]
            gg=where(finite(yy) eq 1 and $
                     ((xx-minx) gt 0.3*(maxx-minx)) and $
                     ((xx-minx) lt 0.7*(maxx-minx)), cgg)
            if(cgg gt ndeg+1) then begin
                p=robust_poly_fit(xx[gg],yy[gg],ndeg)
                gval=where(finite(yy) eq 1)
                trace_array_clean[gval,i,z]=poly(xx[gval],p)
            endif else begin
                print,'Bad trace: n_ord,bottom/top=',i,z
                trace_array_clean[*,i,z]=!values.f_nan
            endelse
        endfor
    endfor    

    return,trace_array_clean
end


function fire_flat_trace,im,n_seg=n_seg,n_poly=n_poly,$
    trace_2d=trace_2d,x_vec=x_vec,o_arr=o_arr

    if(n_elements(n_seg) ne 1) then n_seg=48
    if(n_elements(n_poly) ne 2) then n_poly=[4,9]
    s_im=size(im)

    ;;; FIRE specific settings
    mask=bytarr(24,24)
    mask[*,23]=1
    mask[0:3,22]=1
    mask[21:*,22]=1

    order_first=31 ;; orders start from 31 in the bottom of the frame
    order_step=-1  ;; number decreasing upwards
    order_offset=-20
    ;;; end of FIRE specific settings

    blaze=compute_binned_flat(im,nx=24,ny=24,/fit,deg_fit=[4,4],mask=mask)

    im_flat=im/blaze
    im_m=median(im_flat,5)

    im_diff=shift(im_m,0,-2)-shift(im_m,0,2)
    im_diff[*,0:5]=0.0
    im_diff[*,s_im[2]-6:*]=0.0

    x_vec=fix((s_im[1]/n_seg)*(findgen(n_seg)+0.5))

    prof_mid=median(im_diff[x_vec[fix(n_seg/2)]-2:x_vec[fix(n_seg/2)]+2,*],dim=1)
    prof_mid_pos = (prof_mid > 0)
    prof_mid_neg = ((-prof_mid) > 0)

    find_1d,prof_mid_pos,x_mid_pos,flux,sharp,0.2,5.0,[0.2,1.0],/silent
    x_mid_pos=x_mid_pos[where(flux gt 0.4)]
    flux=flux[where(flux gt 0.4)]
    x_mid_pos=filter_1d(x_mid_pos,20.0) ;; 10.0

    for j=0,n_elements(x_mid_pos)-1 do begin
        if(x_mid_pos[j] gt 7 and x_mid_pos[j] lt s_im[2]-8) then begin
            xx=findgen(15)-7+fix(x_mid_pos[j])
            prof_frag=prof_mid_pos[fix(x_mid_pos[j])-7:fix(x_mid_pos[j])+7]
            prof_frag = (prof_frag - 0.25*max(prof_frag)) > 0
            t=mpfitpeak(/gauss,/positive,xx,prof_frag,g_coeff,nterm=4) ;; t=gaussfit(xx,prof_frag,g_coeff,nterm=4)
            x_mid_pos[j]=g_coeff[1]
        endif
    endfor
;    print,x_mid_pos
    
    find_1d,prof_mid_neg,x_mid_neg,flux,sharp,0.2,5.0,[0.2,1.0],/silent
    x_mid_neg=x_mid_neg[where(flux gt 0.4)]
    flux=flux[where(flux gt 0.4)]
    x_mid_neg=filter_1d(x_mid_neg,20.0) ;; 10.0
    x_mid_neg=x_mid_neg[where(x_mid_neg gt min(x_mid_pos))]
    for j=0,n_elements(x_mid_neg)-1 do begin
        if(x_mid_neg[j] gt 7 and x_mid_neg[j] lt s_im[2]-8) then begin
            xx=findgen(15)-7+fix(x_mid_neg[j])
            prof_frag=prof_mid_neg[fix(x_mid_neg[j])-7:fix(x_mid_neg[j])+7]
            prof_frag = (prof_frag - 0.25*max(prof_frag)) > 0
            t=mpfitpeak(/gauss,/positive,xx,prof_frag,g_coeff,nterm=4) ; t=gaussfit(xx,prof_frag,g_coeff,nterm=4)
            x_mid_neg[j]=g_coeff[1]
        endif
    endfor
;    print,x_mid_neg

    n_orders=n_elements(x_mid_pos)    
    trace_array=fltarr(n_seg,n_orders,2)+!values.f_nan
    trace_array[fix(n_seg/2),*,0]=x_mid_pos
    trace_array[fix(n_seg/2),*,1]=x_mid_neg

    w_prof=30

    for i=fix(n_seg/2)+1,n_seg-1 do begin
        prof=median(im_diff[x_vec[i]-2:x_vec[i]+2,*],dim=1)
        prof_pos = (prof > 0)
        prof_neg = ((-prof) > 0)
        
        for j=0,n_orders-1 do begin
            x_pos_cur=fix(trace_array[i-1,j,0])
            if(x_pos_cur gt w_prof and x_pos_cur lt s_im[2]-(w_prof+1)) then begin
                xx=findgen((2*w_prof+1))-w_prof+x_pos_cur
                prof_pos_frag=prof_pos[x_pos_cur-w_prof:x_pos_cur+w_prof]
                t=mpfitpeak(/gauss,xx,prof_pos_frag,g_coeff,nterm=4)
                if(g_coeff[0] gt 0 and g_coeff[2] gt 0.5) then $
                    trace_array[i,j,0]=g_coeff[1]
            endif
            x_neg_cur=fix(trace_array[i-1,j,1])
            if(x_neg_cur gt w_prof and x_neg_cur lt s_im[2]-(w_prof+1)) then begin
                xx=findgen((2*w_prof+1))-w_prof+x_neg_cur
                prof_neg_frag=prof_neg[x_neg_cur-w_prof:x_neg_cur+w_prof]
                t=mpfitpeak(/gauss,xx,prof_neg_frag,g_coeff,nterm=4)
                if(g_coeff[0] gt 0 and g_coeff[2] gt 0.5) then $
                    trace_array[i,j,1]=g_coeff[1]
            endif
        endfor
    endfor

    for i=fix(n_seg/2)-1,0,-1 do begin
        prof=median(im_diff[x_vec[i]-2:x_vec[i]+2,*],dim=1)
        prof_pos = (prof > 0)
        prof_neg = ((-prof) > 0)
        
        for j=0,n_orders-1 do begin
            x_pos_cur=fix(trace_array[i+1,j,0])
            if(x_pos_cur gt w_prof and x_pos_cur lt s_im[2]-(w_prof+1)) then begin
                xx=findgen((2*w_prof+1))-w_prof+x_pos_cur
                prof_pos_frag=prof_pos[x_pos_cur-w_prof:x_pos_cur+w_prof]
                t=mpfitpeak(/gauss,xx,prof_pos_frag,g_coeff,nterm=4)
                if(g_coeff[0] gt 0 and g_coeff[2] gt 0.5) then $
                    trace_array[i,j,0]=g_coeff[1]
            endif
            x_neg_cur=fix(trace_array[i+1,j,1])
            if(x_neg_cur gt w_prof and x_neg_cur lt s_im[2]-(w_prof+1)) then begin
                xx=findgen((2*w_prof+1))-w_prof+x_neg_cur
                prof_neg_frag=prof_neg[x_neg_cur-w_prof:x_neg_cur+w_prof]
                t=mpfitpeak(/gauss,xx,prof_neg_frag,g_coeff,nterm=4)
                if(g_coeff[0] gt 0 and g_coeff[2] gt 0.5) then $
                    trace_array[i,j,1]=g_coeff[1]
            endif
        endfor
    endfor

    x_arr = (x_vec-s_im[1]/2.0) # (dblarr(n_orders)+1d)
    o_arr=findgen(n_orders)*order_step+order_first+order_offset
    n_arr = (dblarr(n_seg)+1d) # (o_arr)

    data_arr_pos=dblarr(3,n_orders*n_seg)
    data_arr_pos[0,*]=reform(x_arr,1,n_orders*n_seg)
    data_arr_pos[1,*]=reform(n_arr,1,n_orders*n_seg)
    data_arr_pos[2,*]=reform(trace_array[*,*,0],n_orders*n_seg)
    good_data_pos=where(finite(data_arr_pos[2,*]) eq 1, cgood_data_pos)
    data_arr_pos=data_arr_pos[*,good_data_pos]
    t_pos = sfit_2deg(data_arr_pos,/irreg,n_poly[0],n_poly[1],kx=kx_pos,/max)

    data_arr_neg=dblarr(3,n_orders*n_seg)
    data_arr_neg[0,*]=reform(x_arr,1,n_orders*n_seg)
    data_arr_neg[1,*]=reform(n_arr,1,n_orders*n_seg)
    data_arr_neg[2,*]=reform(trace_array[*,*,1],n_orders*n_seg)
    good_data_neg=where(finite(data_arr_neg[2,*]) eq 1, cgood_data_neg)
    data_arr_neg=data_arr_neg[*,good_data_neg]
    t_neg = sfit_2deg(data_arr_neg,/irreg,n_poly[0],n_poly[1],kx=kx_neg,/max)


    kx_pos_2d=dblarr(n_poly[0]+1,n_poly[1]+1)
    kx_neg_2d=dblarr(n_poly[0]+1,n_poly[1]+1)
    ii=0L
    for i=0, n_poly[0] do begin
        for j=0, n_poly[1] do begin
            if(i+j gt (n_poly[0]>n_poly[1])) then continue
            kx_pos_2d[i,j]=kx_pos[ii]
            kx_neg_2d[i,j]=kx_neg[ii]
            ii=ii+1L
        endfor
    endfor

    trace_2d=dblarr(n_poly[0]+1,n_poly[1]+1,2)
    trace_2d[*,*,0]=kx_pos_2d
    trace_2d[*,*,1]=kx_neg_2d
    trace_coeff=dblarr(n_poly[0]+1,n_orders,2)+!values.f_nan

    for i=0,n_poly[0] do begin
        trace_coeff[i,*,0]=poly(o_arr,kx_pos_2d[i,*])
        trace_coeff[i,*,1]=poly(o_arr,kx_neg_2d[i,*])
    endfor

;    for j=0,n_orders-1 do begin
;        g_pos=where(finite(trace_array[*,j,0]) eq 1, cg_pos)
;        if(cg_pos gt 6) then begin
;            c_tr=(n_poly[0] le 5)? robust_poly_fit(x_vec[g_pos],trace_array[g_pos,j,0],n_poly[0]) : poly_fit(x_vec[g_pos],trace_array[g_pos,j,0],n_poly[0])
;            trace_coeff[*,j,0]=c_tr
;        endif
;        g_neg=where(finite(trace_array[*,j,1]) eq 1, cg_neg)
;        if(cg_neg gt 6) then begin
;            c_tr=(n_poly[0] le 5)? robust_poly_fit(x_vec[g_neg],trace_array[g_neg,j,1],n_poly[0]) : poly_fit(x_vec[g_neg],trace_array[g_neg,j,1],n_poly[0])
;            trace_coeff[*,j,1]=c_tr
;        endif
;    endfor

     return, trace_coeff
end
