function compute_binned_flat,im,nx=nx,ny=ny,med_w=med_w,$
    fit=fit,deg_fit=deg_fit,mask=mask

    if(n_elements(nx) ne 1) then nx=24
    if(n_elements(ny) ne 1) then ny=24
    if(n_elements(med_w) ne 1) then med_w=11

    bin_flat=fltarr(nx,ny)
    x_arr=fltarr(nx,ny)
    y_arr=fltarr(nx,ny)
    s_im=size(im)

    med_im=median(im,med_w)
    med_im[0:med_w-1,*]=!values.f_nan
    med_im[*,0:med_w-1]=!values.f_nan
    med_im[s_im[1]-med_w-1:*,*]=!values.f_nan
    med_im[*,s_im[2]-med_w-1:*]=!values.f_nan
    for x=0,nx-1 do begin
        xmin=0 > (s_im[1]*(float(x)/nx)) < (s_im[1]-1)
        xmax=0 > (s_im[1]*(float(x+1)/nx)) < (s_im[1]-1)
        x_arr[x,*]=(xmin+xmax)/2.0
        for y=0,ny-1 do begin
            ymin=0 > (s_im[2]*(float(y)/ny)) < (s_im[2]-1)
            ymax=0 > (s_im[2]*(float(y+1)/ny)) < (s_im[2]-1)
            if(x eq 0) then y_arr[*,y]=(ymin+ymax)/2.0

            bin_flat[x,y]=max(med_im[xmin:xmax,ymin:ymax],/nan)
        endfor
    endfor

    if(keyword_set(fit)) then begin
        if(n_elements(deg_fit) ne 2) then deg_fit=[4,4]
        data_arr=dblarr(3,nx*ny)
        data_arr[0,*]=transpose(reform(x_arr,nx*ny))
        data_arr[1,*]=transpose(reform(y_arr,nx*ny))
        data_arr[2,*]=transpose(reform(alog10((bin_flat > 10)),nx*ny))
        if(n_elements(mask) eq nx*ny) then begin
            good_mask=where(mask eq 0, cgood_mask)
            if(cgood_mask gt (deg_fit[0]+1)*(deg_fit[1]+1)+1) then $
                data_arr=data_arr[*,good_mask]
        endif
        
        res=sfit_2deg(data_arr, deg_fit[0], deg_fit[1], /irreg,kx=kx)
        
        bin_flat=10^(poly2d(dindgen(s_im[1]),dindgen(s_im[2]),kx))
    endif

    return,congrid(bin_flat,s_im[1],s_im[2],/interp)
end
