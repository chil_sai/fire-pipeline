function fire_wlsol_filter_good_lines,fit_data,err_data,kwl3d,wl_lines,good_id_max,$
    good_idx=good_idx,dlam_thr=dlam_thr,rms_thr=rms_thr,min_rms=min_rms,cres_idx=cres_idx,pixels=pixels,$
    m0=m0,legendre=legendre,crd_map=crd_map,red_corner=red_corner

    slith=51l
    cgood_id_max=n_elements(good_id_max)
    n_pnt=n_elements(fit_data[0,*])
    if(n_elements(good_idx) le 1 or n_elements(good_idx) gt n_pnt) then good_idx=lindgen(n_pnt)
    data_mask=bytarr(slith,cgood_id_max)
    data_mask[good_idx]=1

    lam_fit=reform(poly3d_echelle(transpose(fit_data[0,*]),transpose(fit_data[1,*]),transpose(fit_data[2,*]),kwl3d,/irreg,m0=m0,legendre=legendre,crd_map=crd_map),slith,cgood_id_max)
    lam_off_fit=reform(poly3d_echelle(transpose(fit_data[0,*]),transpose(fit_data[1,*])+1.0,transpose(fit_data[2,*]),kwl3d,/irreg,m0=m0,legendre=legendre,crd_map=crd_map),slith,cgood_id_max)
    disp_fit=abs(lam_off_fit-lam_fit)
    dlam_fit=lam_fit*!values.d_nan
    med_dlam=lam_fit*!values.d_nan
    med_disp=lam_fit*!values.d_nan
    std_dlam=lam_fit*!values.d_nan

    min_g_frac=0.75 ;;; 25% of pixels along the slit may be missing
    for k=0,cgood_id_max-1 do if(finite(wl_lines[good_id_max[k]]) eq 1) then begin
          dlam_fit[*,k]=wl_lines[good_id_max[k]]-lam_fit[*,k]
          good_pix_line=where(finite(dlam_fit[*,k]) eq 1 and data_mask[*,k] eq 1, cgood_pix_line)
          if(cgood_pix_line gt slith*min_g_frac) then begin
              med_dlam[*,k]=median(dlam_fit[good_pix_line,k])
              std_dlam[*,k]=stddev(dlam_fit[good_pix_line,k])
              med_disp[*,k]=median(disp_fit[good_pix_line,k])
          endif
    endif

    res_idx=(keyword_set(pixels))? $
        where(((data_mask eq 1) and $
                   ((abs(med_dlam)/med_disp le dlam_thr and std_dlam/med_disp le rms_thr) or (err_data/med_disp lt min_rms))) or $
              (keyword_set(red_corner) and (fit_data[0,*] le 12) and (fit_data[1,*] lt 180)),cres_idx) : $
        where((data_mask eq 1) and $
                   ((abs(med_dlam) le dlam_thr and std_dlam le rms_thr) or (err_data lt min_rms)) or $
              (keyword_set(red_corner) and (fit_data[0,*] le 12) and (fit_data[1,*] lt 180)),cres_idx)

    return,res_idx
end


function fire_fit_wlsol,arc_thar_orig,flat_orig,oh_frame=oh_frame,use_oh=use_oh,$
    min_rms_thr=min_rms_thr,dwl_thr=dwl_thr,large_offset=large_offset,$
    deg_fit=deg_fit,max_degree=max_degree,detect_fwhm=detect_fwhm,low_order=low_order,no_calib_offset=no_calib_offset,$
    trace_coeff=trace_coeff,display=display,dy_vis=dy_vis,verbose=verbose,$
    input_wlsol_struct=input_wlsol_struct,crd_map=crd_map,$
    output_flat=output_flat,output_arc_thar=output_arc_thar,output_wlsol_struct=output_wlsol_struct,wlsol_quality=wlsol_quality

    if(n_elements(dy_vis) ne 1) then dy_vis=0
    if(n_elements(deg_fit) ne 3) then deg_fit=[8,6,1]
    if(n_elements(detect_fwhm) ne 1) then detect_fwhm=4.0

    if(n_elements(low_order) ne 1) then low_order=0

    s_inp_wl=size(input_wlsol_struct)
    inp_wl_flag=0
    if(s_inp_wl[0] eq 1 and s_inp_wl[2] eq 8) then $
        inp_wl_flag=(tag_exist(input_wlsol_struct,'deg1') and $
                     tag_exist(input_wlsol_struct,'deg2') and $
                     tag_exist(input_wlsol_struct,'deg3') and $
                     tag_exist(input_wlsol_struct,'kwl3d')) ? 1 : 0

    if(low_order gt 0) then begin
        kwl3d_ini_struct=(inp_wl_flag eq 1)? input_wlsol_struct : mrdfits(getenv('FIRE_PIPELINE_PATH')+'calib_FIRE/wavelength/fire_wl3d_ini.fits',1,/silent)
        deg_fit=[kwl3d_ini_struct.deg1,kwl3d_ini_struct.deg2,kwl3d_ini_struct.deg3]
        kwl3d_ini=kwl3d_ini_struct.kwl3d
        fix_coeff=bytarr(deg_fit[0]+1,deg_fit[1]+1,deg_fit[2]+1)+1
        for i=0,deg_fit[0] do $
            for j=0,deg_fit[1] do $
                for k=0,deg_fit[2] do if(i+j+k le low_order) then fix_coeff[i,j,k]=0
    endif else begin
        fix_coeff=bytarr(deg_fit[0]+1,deg_fit[1]+1,deg_fit[2]+1)
        kwl3d_ini=double(fix_coeff)*0d
    endelse

    slith=51l

    arc_thar=arc_thar_orig
    flat=flat_orig
    pixflat=readfits(getenv('FIRE_PIPELINE_PATH')+'calib_FIRE/detector/H2RG/pixflat.fits',/silent)
    s_arc=size(arc_thar)
    s_oh=size(oh_frame)
    if(s_oh[1] ne s_arc[1] or s_oh[2] ne s_arc[2]) then use_oh=0

;    wl_ini=(inp_wl_flag eq 1)? $
;        {deg1:input_wlsol_struct.deg1, deg2:input_wlsol_struct.deg2, kx:input_wlsol_struct.kwl3d[*,*,0]+0.5*input_wlsol_struct.kwl3d[*,*,1], stdev:1e-7, robsig:1e-7, n_lines:1000L} : $
     wl_ini=   mrdfits(getenv('FIRE_PIPELINE_PATH')+'calib_FIRE/wavelength/fire_wl_ini.fits',1,/silent)

    for wl_iter=0,1 do begin
        trace_coeff=fire_flat_trace(flat*1e+4,x_vec=x_vec,o_arr=o_arr,n_poly=[6,9]) ;; [5,9]
        n_ord=(size(o_arr))[1]
        if(n_ord gt 21) then begin
            trace_coeff=trace_coeff[*,n_ord-21:*,*]
            o_arr=o_arr[n_ord-21:*]
            n_ord=21
        endif

        mask=bytarr(24,24)
        mask[*,23]=1
        mask[0:3,21:22]=1
        mask[21:*,22]=1
        mask[0:16,0]=1
        mask[0:10,0]=1
        blaze=compute_binned_flat(flat*1e4,nx=24,ny=24,/fit,deg_fit=[4,5],mask=mask)
        flat_norm=congrid(blaze/max(blaze,/nan),2048,2048,/interp)
        
        if(n_elements(crd_map) ne 6) then crd_map=[[22d,1024d,0.5d],[12d,1024d,2d]]
        if(wl_iter eq 0) then begin
            t=fire_detect_arc_lines(smooth(arc_thar/flat_norm,3,/nan),trace_coeff,detect_fwhm,$
                                    maxrms=2.0*1.5,maxvisflux=100.0,$
                                    obj=smooth(arc_thar/flat_norm,3,/nan),$
                                    smy=5,dy_vis=dy_vis,ndeg_y=1,display=display,/ord_red) ;;; fwhm=4,maxrms=2.5
;            t.order-=1
        endif else begin
            for lidx=0,n_elements(t)-1 do begin
                t[lidx].xpos=t[lidx].xpos+poly(t[lidx].xpos,xdtr)
                t[lidx].ypos=t[lidx].ypos+poly(t[lidx].ypos,ydtr)
                t[lidx].xfit=t[lidx].xfit+poly(t[lidx].xfit,xdtr)
                t[lidx].yfit=t[lidx].yfit+poly(t[lidx].yfit,ydtr)
                t[lidx].xmean=t[lidx].xmean+poly(t[lidx].xmean,xdtr)
            endfor
            wl_ini=fire_get_wl_ini_from_data(t,lin_iter3,kwl3d,deg1=deg_fit[0],deg2=deg_fit[1],/mean,/legendre,crd_map=crd_map,m0=11)
            if(keyword_set(use_oh)) then $
                toh=fire_detect_arc_lines(smooth((arc_thar+oh_frame)/flat_norm,3,/nan),trace_coeff,detect_fwhm+0.0,$
                                          maxrms=2.0*1.0,maxvisflux=700.0,$
                                          obj=smooth((oh_frame)/flat_norm,3,/nan),$
                                          smy=5,/ord_red,dy_vis=dy_vis,ndeg_y=1,display=display)
;                toh.order-=1
        endelse

        zoom=400.0
        min_rms_thr=(n_elements(min_rms_thr) eq 1)? min_rms_thr : 0.3 ; always accept strong lines whose RMS along the slit is <0.3pix. the smaller the number the brighter the line
        dwl=(n_elements(dwl_thr) eq 1)? dwl_thr : 0.5*2         ; maximal acceptable residual of a line position in pixels before it gets rejected (0.5pix)
        for fit_iter=0,3-4*wl_iter do begin
;            if(fit_iter gt 0) then min_rms_thr=0.0
            wl_lines=echelle_ident_arc_2d(t,minflux=100.0/3.0/(1.0+fit_iter^2),wl_ini=wl_ini,verbose=verbose,$
                fwhm=6.0/(0.7+1.5*(fit_iter ge 1))*(1.0+keyword_set(large_offset)*(fit_iter eq 0)),$
                nwl_step=((fit_iter eq 0)? 150*(1.0+5*keyword_set(large_offset)) : 20),/fire,/merge_blends_fire) ;;; wl_iter eq 0
;                fwhm=4.0/(0.7+2.0*(fit_iter ge 1)),nwl_step=((fit_iter eq 0)? 150 : 20)) ;;; wl_iter eq 0
            good_id_max=where(wl_lines gt 100.0 and finite(wl_lines) eq 1 and $
                             ((t.rms lt 1.2) or ((t.rms lt 2.0) and (t.xmean lt 680) and (t.ypos[25] gt 1500)) or (t.order le 15)), cgood_id_max)
            print,'lines identified: ',cgood_id_max,' at iteration',fit_iter
            fit_data=dblarr(4,slith*cgood_id_max)
            linid_data=reform((lonarr(slith)+1l) # good_id_max,slith*cgood_id_max)
            fit_data[0,*]=reform((lonarr(slith)+1l) # t[good_id_max].order,slith*cgood_id_max)
            fit_data[1,*]=reform(t[good_id_max].xpos,slith*cgood_id_max)
            fit_data[2,*]=reform((dindgen(slith)/(slith-1d)) # (lonarr(cgood_id_max)+1l),slith*cgood_id_max)
            fit_data[3,*]=reform((lonarr(slith)+1l) # wl_lines[good_id_max],slith*cgood_id_max)
            err_data=reform((lonarr(slith)+1l) # t[good_id_max].rms,slith*cgood_id_max)^1.0
            err_data_fit=err_data/(1.0+2d*(((fit_data[1,*]-1500d)/1024d)>0)^2)
            top_frame=where(fit_data[1,*] gt 1500d)
            err_data_fit[top_frame]=err_data_fit[top_frame]/(1.0+2.0*(((fit_data[0,top_frame]-1024d)/1024d)>0)^2)
            red_ord_idx=where(fit_data[0,*] le 12, cred_ord_idx)
            if(cred_ord_idx gt 0) then err_data_fit[red_ord_idx]/=3.0
;            bot_left_frame=where(fit_data[0,*] lt 1024 and fit_data[1,*] lt 768d, cbot_left_frame)
;            if(cbot_left_frame gt 0) then err_data_fit[bot_left_frame]=err_data_fit[bot_left_frame]/(1.0+3.0*(((fit_data[0,bot_left_frame]-1024d)/1024d)>0)^2)
            g_data=where(finite(total(fit_data,1)) eq 1,cg_data)

            ttt=vfit_3deg_err_fixpar_echelle(fit_data[*,g_data],err=err_data_fit[g_data],deg_fit[0],deg_fit[1],deg_fit[2],kx=kwl3d,/irreg,max=(low_order eq 0),ini_kx=(fit_iter eq 3? -1 : kwl3d_ini),fix_coeff=(fit_iter eq 3? -1 : fix_coeff),/legendre,crd_map=crd_map,m0=11)
            print,'RMS(wlsol)[nm] =',robust_sigma(fit_data[3,g_data]-ttt),cg_data
            if (keyword_set(display)) then fire_display_arc_lines,arc_thar/flat_norm,fit_data,kwl3d,t,wl_lines,good_id_max,dy_vis=dy_vis,/legendre,m0=11,crd_map=crd_map,flmax=100,/verbose

            iter1_data=fire_wlsol_filter_good_lines(fit_data,err_data,kwl3d,wl_lines,good_id_max,$
                good_idx=g_data,dlam_thr=dwl*20.0/(1.0+0.4*fit_iter),rms_thr=3.0*dwl,min_rms=min_rms_thr,cres_idx=citer1_data,/pix,/legendre,m0=11,crd_map=crd_map)

            ttt1=vfit_3deg_err_fixpar_echelle(fit_data[*,iter1_data],err=err_data_fit[iter1_data],deg_fit[0],deg_fit[1],deg_fit[2],kx=kwl3d,/irreg,max_degree=max_degree,ini_kx=(fit_iter eq 3? -1 : kwl3d_ini),fix_coeff=(fit_iter eq 3? -1 : fix_coeff),/legendre,crd_map=crd_map,m0=11)
            print,'RMS(wlsol)[nm] =',robust_sigma(fit_data[3,iter1_data]-ttt1),citer1_data
            if (keyword_set(display)) then fire_display_arc_lines,arc_thar/flat_norm,fit_data,kwl3d,t,wl_lines,good_id_max,dy_vis=dy_vis,/legendre,m0=11,crd_map=crd_map,flmax=100,/verbose

            min_rms_thr_final=min_rms_thr/(0.2*fit_iter+1)
            iter2_data=fire_wlsol_filter_good_lines(fit_data,err_data,kwl3d,wl_lines,good_id_max,$
                good_idx=iter1_data,dlam_thr=dwl*5.0/(1.0+10.0*0.2*(fit_iter>1)),rms_thr=dwl*2.0,min_rms=min_rms_thr_final,cres_idx=citer2_data,/pix,/legendre,m0=11,crd_map=crd_map)
            ttt2=vfit_3deg_err_fixpar_echelle(fit_data[*,iter2_data],err=err_data_fit[iter2_data],deg_fit[0],deg_fit[1],deg_fit[2],kx=kwl3d,/irreg,max_degree=max_degree,ini_kx=(fit_iter eq 3? -1 : kwl3d_ini),fix_coeff=(fit_iter eq 3? -1 : fix_coeff),/legendre,crd_map=crd_map,m0=11)
            print,'RMS(wlsol)[nm] =',robust_sigma(fit_data[3,iter2_data]-ttt2),citer2_data
            if (keyword_set(display)) then fire_display_arc_lines,arc_thar/flat_norm,fit_data,kwl3d,t,wl_lines,good_id_max,dy_vis=dy_vis,/legendre,m0=11,crd_map=crd_map,flmax=100,/verbose

            lin_iter2=linid_data[iter2_data[uniq(linid_data[iter2_data],sort(linid_data[iter2_data]))]]
            wl_ini=fire_get_wl_ini_from_data(t,lin_iter2,kwl3d,deg1=deg_fit[0],deg2=deg_fit[1],/mean,/legendre,crd_map=crd_map,m0=11)
        endfor

        if(wl_iter gt 0) then $
            ttt2=vfit_3deg_err_fixpar_echelle(fit_data[*,iter2_data],err=err_data_fit[iter2_data],deg_fit[0],deg_fit[1],deg_fit[2],kx=kwl3d,/irreg,max_degree=max_degree,/legendre,crd_map=crd_map,m0=11)
        iter3_data=fire_wlsol_filter_good_lines(fit_data,err_data,kwl3d,wl_lines,good_id_max,$
            good_idx=iter2_data,dlam_thr=dwl*1.5/1.5,rms_thr=dwl/1.5,min_rms=min_rms_thr_final*0.0,cres_idx=citer3_data,/pix,/legendre,m0=11,crd_map=crd_map,/red_corner)
        ttt3=vfit_3deg_err_fixpar_echelle(fit_data[*,iter3_data],err=err_data_fit[iter3_data],deg_fit[0],deg_fit[1],deg_fit[2],kx=kwl3d,/irreg,max_degree=max_degree,/legendre,crd_map=crd_map,m0=11)
;;        plot,fit_data[1,iter3_data],fit_data[3,iter3_data]-ttt3,xs=1,ys=1,psym=4
        print,'RMS(wlsol)[nm] =',robust_sigma(fit_data[3,iter3_data]-ttt3),citer3_data

        lin_iter3=linid_data[iter3_data[uniq(linid_data[iter3_data],sort(linid_data[iter3_data]))]]
        wl_ini=fire_get_wl_ini_from_data(t,lin_iter3,kwl3d,deg1=deg_fit[0],deg2=deg_fit[1],/mean,/legendre,crd_map=crd_map,m0=11)

        if (keyword_set(display)) then fire_display_arc_lines,arc_thar/flat_norm,fit_data,kwl3d,t,wl_lines,good_id_max,dy_vis=dy_vis,/legendre,m0=11,crd_map=crd_map,flmax=300,/verbose
        data_used=fire_xnl2y(transpose(fit_data[1,*]),$
                             transpose(fit_data[0,*]),$
                             transpose(fit_data[2,*]),trace_coeff,/truen)
        if(keyword_set(display)) then plots,fit_data[1,iter3_data],data_used[iter3_data]-dy_vis,/dev,col=192,psym=4,syms=0.3
        ord_fit=fit_data[0,iter3_data[uniq(fit_data[0,iter3_data],sort(fit_data[0,iter3_data]))]]
        n_ord_fit=n_elements(ord_fit)
        wlsol_quality=replicate({order:-1,xmin:-1,xmax:-1,n_lines:-1,rms_nm:-1.0,rms_kms:-1.0,dv_kms:0.0},n_ord_fit)
        for oo=0,n_ord_fit-1 do begin
            ord_cur_idx=where(fit_data[0,iter3_data] eq ord_fit[oo])
            ord_data_idx=iter3_data[ord_cur_idx]
            ord_cur_line_ids=linid_data[uniq(linid_data[ord_data_idx],sort(linid_data[ord_data_idx]))]
            ord_cur_min_x=min(fit_data[1,ord_data_idx],max=ord_cur_max_x)
            wlsol_quality[oo].order=ord_fit[oo]
            wlsol_quality[oo].xmin=ord_cur_min_x
            wlsol_quality[oo].xmax=ord_cur_max_x
            wlsol_quality[oo].n_lines=n_elements(ord_cur_line_ids)
            wlsol_quality[oo].rms_nm=robust_sigma(fit_data[3,ord_data_idx]-ttt3[ord_cur_idx])
            wlsol_quality[oo].rms_kms=299792.5*robust_sigma((fit_data[3,ord_data_idx]-ttt3[ord_cur_idx])/fit_data[3,ord_data_idx])
            wlsol_quality[oo].dv_kms=299792.5*median((fit_data[3,ord_data_idx]-ttt3[ord_cur_idx])/fit_data[3,ord_data_idx])
            print,'Ord#',ord_fit[oo],' xrange=',ord_cur_min_x,ord_cur_max_x,$
                ' nlin=',n_elements(ord_cur_line_ids),$
                ' RMS=',wlsol_quality[oo].rms_nm,'nm/',wlsol_quality[oo].rms_kms,'km/s',$
                ' <dv>=',wlsol_quality[oo].dv_kms,'km/s',$
                format='(a,i2,a,i4,1x,i4,a,i3,a,f6.4,a,f5.1,a,a,f5.1,a)'
        endfor

        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ; The following block is executed only if the /use_oh keyword is set
        ; it fits the wavelength solution using a combined ThAr+OH frame to
        ; cover shorter wavelengths with ThAr and longer wavelengths with OH
        ; It is algorithmically identical to the previous block and it
        ; performs a scattered 3D fitting with a manual sigma clipping
        ;
        if((wl_iter eq 1) and keyword_set(use_oh)) then begin
            wl_ini_oh=wl_ini
            for oh_fit_iter=0,2+7+2*(low_order gt 0) do begin
;                wl_lines_oh=fire_ident_arc_2d(toh,wl_ini=wl_ini_oh,fwhm=3.5-1.0*(oh_fit_iter ge 1),minfl=0.2d,nwl_step=(oh_fit_iter eq 0? 30 : 5),/oh,/ord12)
                wl_lines_oh=echelle_ident_arc_2d(toh,wl_ini=wl_ini_oh,verbose=verbose,fwhm=4.0/(0.7+2.*(oh_fit_iter ge 1)),$
                                              minfl=0.2d,nwl_step=(oh_fit_iter eq 0 or oh_fit_iter eq 4? 30 : 5),/oh,/fire,/merge_blends_fire) ;,/ord12)
                nn=11
                wp=where(toh.order eq nn,cwp)
                sl=(sort(toh[wp].xmean))
                for i=0,cwp-1 do print,toh[wp[sl[i]]].xmean,toh[wp[sl[i]]].flux[25],toh[wp[sl[i]]].rms,toh[wp[sl[i]]].wavelength

                good_id_max_oh=where(wl_lines_oh gt 100.0 and finite(wl_lines_oh) eq 1 and $
                                    ((toh.rms lt 1.0/(1.0+0.2*oh_fit_iter)) or ((toh.rms lt 3.0/1.5/(1.0+0.2*oh_fit_iter)) and (toh.ypos[25] gt 1500))) and $
;;;                                    ((toh.rms lt 1.0) or ((toh.rms lt 1.0+3.0/(1.0+2*oh_fit_iter)) and (toh.xmean lt 500) and (toh.ypos[25] gt 1500))) and $
                                    (toh.order le 24) and (toh.order ge 11-(oh_fit_iter gt 1)), cgood_id_max_oh)
                print,'Number of OH lines identified in orders <=24:',string(cgood_id_max_oh,format='(i4)')
                fit_data_oh=dblarr(4,slith*cgood_id_max_oh)
                linid_data_oh=reform((lonarr(slith)+1l) # good_id_max_oh,slith*cgood_id_max_oh)
                fit_data_oh[0,*]=reform((lonarr(slith)+1l) # toh[good_id_max_oh].order,slith*cgood_id_max_oh)
                fit_data_oh[1,*]=reform(toh[good_id_max_oh].xpos,slith*cgood_id_max_oh)
                fit_data_oh[2,*]=reform((dindgen(slith)/(slith-1d)) # (lonarr(cgood_id_max_oh)+1l),slith*cgood_id_max_oh)
                fit_data_oh[3,*]=reform((lonarr(slith)+1l) # wl_lines_oh[good_id_max_oh],slith*cgood_id_max_oh)
                err_data_oh=reform((lonarr(slith)+1l) # toh[good_id_max].rms,slith*cgood_id_max)^1.0
                err_data_oh_fit=err_data_oh/(1.0+2d*(((fit_data_oh[1,*]-1500d)/1024d)>0)^2)
                top_frame_oh=where(fit_data_oh[1,*] gt 1500d)
                err_data_oh_fit[top_frame_oh]=err_data_oh_fit[top_frame_oh]/(1.0+2.0*(((fit_data_oh[0,top_frame_oh]-1024d)/1024d)>0)^2)
                g_data_oh=where(finite(total(fit_data_oh,1)) eq 1,cg_data_oh)

                fit_data_comb=transpose([transpose(fit_data_oh),transpose(fit_data[*,*])])
                err_data_comb=[reform(err_data_oh_fit,n_elements(err_data_oh_fit)),err_data_fit[*]]
                linid_data_comb=[linid_data_oh,linid_data[*]]
                g_data_comb=where(finite(total(fit_data_comb,1)) eq 1,cg_data_comb)
                tttoh=vfit_3deg_err_fixpar_echelle(fit_data_comb[*,g_data_comb],err=err_data_comb[g_data_comb],deg_fit[0],deg_fit[1],deg_fit[2],kx=kwl3d_oh,max_degree=max_degree,/irreg,ini_kx=kwl3d_ini,fix_coeff=fix_coeff,/legendre,crd_map=crd_map,m0=11)
                print,'RMS(wlsol)[nm] =',robust_sigma(fit_data_comb[3,g_data_comb]-tttoh),cg_data_comb

                iter1_data_oh=fire_wlsol_filter_good_lines(fit_data_oh,err_data_oh,kwl3d_oh,wl_lines_oh,good_id_max_oh,$
                    good_idx=g_data_oh,dlam_thr=dwl*20.0/1.5/(1.0+0.4*oh_fit_iter),rms_thr=3.0*dwl,min_rms=min_rms_thr,cres_idx=citer1_data_oh,/pix,/legendre,m0=11,crd_map=crd_map)
                iter1_datacomb=[iter1_data_oh,iter3_data+n_elements(err_data_oh_fit)]
                tttoh1=vfit_3deg_err_fixpar_echelle(fit_data_comb[*,iter1_datacomb],err=err_data_comb[iter1_datacomb],deg_fit[0],deg_fit[1],deg_fit[2],kx=kwl3d_oh,max_degree=max_degree,/irreg,ini_kx=kwl3d_ini,fix_coeff=fix_coeff,/legendre,crd_map=crd_map,m0=11)
                print,'RMS(wlsol)[nm] =',robust_sigma(fit_data_comb[3,iter1_datacomb]-tttoh1),n_elements(tttoh1)

                iter2_data_oh=fire_wlsol_filter_good_lines(fit_data_oh,err_data_oh,kwl3d_oh,wl_lines_oh,good_id_max_oh,$
                    good_idx=iter1_data_oh,dlam_thr=dwl*5.0/1.5/(1.0+0.2*oh_fit_iter),rms_thr=1.5*dwl,min_rms=min_rms_thr_final,cres_idx=citer2_data_oh,/pix,/legendre,m0=11,crd_map=crd_map)
                iter2_datacomb=[iter2_data_oh,iter3_data+n_elements(err_data_oh_fit)]
                tttoh2=vfit_3deg_err_fixpar_echelle(fit_data_comb[*,iter2_datacomb],err=err_data_comb[iter2_datacomb],deg_fit[0],deg_fit[1],deg_fit[2],kx=kwl3d_oh,max_degree=max_degree,/irreg,ini_kx=kwl3d_ini,fix_coeff=fix_coeff,/legendre,crd_map=crd_map,m0=11)
                print,'RMS(wlsol)[nm] =',robust_sigma(fit_data_comb[3,iter2_datacomb]-tttoh2),n_elements(tttoh2)

                iter3_data_oh=fire_wlsol_filter_good_lines(fit_data_oh,err_data_oh,kwl3d_oh,wl_lines_oh,good_id_max_oh,$
                    good_idx=iter2_data_oh,dlam_thr=dwl*1.5,rms_thr=dwl,min_rms=min_rms_thr_final*1.0,cres_idx=citer3_data_oh,/pix,/legendre,m0=11,crd_map=crd_map)
                iter3_datacomb=[iter3_data_oh,iter3_data+n_elements(err_data_oh_fit)]
                tttoh3=vfit_3deg_err_fixpar_echelle(fit_data_comb[*,iter3_datacomb],err=err_data_comb[iter3_datacomb],deg_fit[0],deg_fit[1],deg_fit[2],kx=kwl3d_oh,max_degree=max_degree,/irreg,ini_kx=kwl3d_ini,fix_coeff=fix_coeff,/legendre,crd_map=crd_map,m0=11)
                print,'RMS(wlsol)[nm] =',robust_sigma(fit_data_comb[3,iter3_datacomb]-tttoh3),n_elements(tttoh3)

                if (keyword_set(display)) then fire_display_arc_lines,(oh_frame+arc_thar)/flat_norm,fit_data_oh,kwl3d_oh,toh,wl_lines_oh,good_id_max_oh,flmax=300,dy_vis=dy_vis,/legendre,m0=11,crd_map=crd_map,/verbose
                if (keyword_set(display)) then fire_display_arc_lines,(oh_frame)/flat_norm,fit_data,kwl3d_oh,t,wl_lines,good_id_max,flmax=300,dy_vis=dy_vis,/noimage,colred=220,colblue=30,/legendre,m0=11,crd_map=crd_map,/verbose
                lin_iter_comb=linid_data_comb[iter3_datacomb[uniq(linid_data_comb[iter3_datacomb],sort(linid_data_comb[iter3_datacomb]))]]
                wl_ini_oh=fire_get_wl_ini_from_data([toh,t],lin_iter_comb,kwl3d_oh,deg1=deg_fit[0],deg2=deg_fit[1],/mean,/legendre,crd_map=crd_map,m0=11)
            endfor
            kwl3d=kwl3d_oh
            data_used_comb=fire_xnl2y(transpose(fit_data_comb[1,*]),$
                                 transpose(fit_data_comb[0,*]),$
                                 transpose(fit_data_comb[2,*]),trace_coeff,/truen)
            if(keyword_set(display)) then plots,fit_data_comb[1,iter3_datacomb],data_used_comb[iter3_datacomb]-dy_vis,/dev,col=192,psym=4,syms=0.3
            ord_fit=fit_data_comb[0,iter3_datacomb[uniq(fit_data_comb[0,iter3_datacomb],sort(fit_data_comb[0,iter3_datacomb]))]]
            n_ord_fit=n_elements(ord_fit)
            wlsol_quality=replicate({order:-1,xmin:-1,xmax:-1,n_lines:-1,rms_nm:-1.0,rms_kms:-1.0,dv_kms:0.0},n_ord_fit)
            for oo=0,n_ord_fit-1 do begin
                ord_cur_idx=where(fit_data_comb[0,iter3_datacomb] eq ord_fit[oo])
                ord_data_idx=iter3_datacomb[ord_cur_idx]
                ord_cur_line_ids=linid_data_comb[uniq(linid_data_comb[ord_data_idx],sort(linid_data_comb[ord_data_idx]))]
                ord_cur_min_x=min(fit_data_comb[1,ord_data_idx],max=ord_cur_max_x)
                wlsol_quality[oo].order=ord_fit[oo]
                wlsol_quality[oo].xmin=ord_cur_min_x
                wlsol_quality[oo].xmax=ord_cur_max_x
                wlsol_quality[oo].n_lines=n_elements(ord_cur_line_ids)
                wlsol_quality[oo].rms_nm=robust_sigma(fit_data_comb[3,ord_data_idx]-tttoh3[ord_cur_idx])
                wlsol_quality[oo].rms_kms=299792.5*robust_sigma((fit_data_comb[3,ord_data_idx]-tttoh3[ord_cur_idx])/fit_data_comb[3,ord_data_idx])
                wlsol_quality[oo].dv_kms=299792.5*median((fit_data_comb[3,ord_data_idx]-tttoh3[ord_cur_idx])/fit_data_comb[3,ord_data_idx])
                print,'Ord#',ord_fit[oo],' xrange=',ord_cur_min_x,ord_cur_max_x,$
                    ' nlin=',n_elements(ord_cur_line_ids),$
                    ' RMS=',wlsol_quality[oo].rms_nm,'nm/',wlsol_quality[oo].rms_kms,'km/s',$
                    ' <dv>=',wlsol_quality[oo].dv_kms,'km/s',$
                    format='(a,i2,a,i4,1x,i4,a,i3,a,f6.4,a,f5.1,a,a,f5.1,a)'
            endfor
        endif

        ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        ; the following block is executed at the end of the first iteration
        ; to measure offsets between daytime calibrations and a science frame
        ; it measures offsets in three pre-defined regions of the frame
        ; and then applies a linear transformation (skew+shift+scale)
        ;
        if(wl_iter eq 0) then begin
;            corners=double([[310,1285,535,1475],[830,1330,1120,1550],[1500,1255,1720,1480]]) ;;;,[915,1635,1140,1840]])
            corners=double([[50,1285,50+220,1475],[830,1330,1120,1550],[1770,1255,1770+220,1480]]) ;;;,[915,1635,1140,1840]])
            xy_all = (keyword_set(no_calib_offset))? dblarr(2,3) : fire_measure_offset_2d(smooth((oh_frame),3,/nan)*pixflat,kwl3d,trace_coeff,/verb,flat=flat_norm,syn=ff,corr=mtx,/pix,max_corr=max_corr,corners=corners,/legendre,crd_map=crd_map,m0=11,/fourier,mag=1.0,/fit) ;,/int5p,/cle)
;            xy_all = (keyword_set(no_calib_offset))? dblarr(2,3) : fire_measure_offset_2d(median((oh_frame),3)*pixflat,kwl3d,trace_coeff,/verb,flat=flat_norm,syn=ff,corr=mtx,/pix,max_corr=max_corr,corners=corners) ;,/int5p,/cle)
            print,'XY shift:', xy_all

            xdtr=linfit((corners[0,0:2]+corners[2,0:2])/2.0,xy_all[0,0:2])
            ydtr=linfit((corners[0,0:2]+corners[2,0:2])/2.0,xy_all[1,0:2])

            xtr_p2d=linfit((corners[0,0:2]+corners[2,0:2])/2.0+xy_all[0,0:2],(corners[0,0:2]+corners[2,0:2])/2.0)
            ytr=linfit((corners[0,0:2]+corners[2,0:2])/2.0,(corners[0,0:2]+corners[2,0:2])/2.0+xy_all[1,0:2])
            ;;xy=[poly([1024.],xtr),poly([1024.],ytr)]

            p_p2d=dblarr(2,2)
            p_p2d[0,*]=transpose(xtr_p2d)
            q_p2d=dblarr(2,2)
            q_p2d[0,0]=-ydtr[0]
            q_p2d[1,0]=1d
            q_p2d[0,1]=-ydtr[1]

            arc_thar=poly_2d(arc_thar,p_p2d,q_p2d,1)
            flat=poly_2d(flat,p_p2d,q_p2d,1)
            fit_data[1,*]=fit_data[1,*]+poly(fit_data[1,*],xdtr)
        endif
    endfor

    output_flat=flat
    output_arc_thar=arc_thar
    output_wlsol_struct={deg1:deg_fit[0],deg2:deg_fit[1],deg3:deg_fit[2],kwl3d:kwl3d,$
                         n_lines:total(wlsol_quality.n_lines),stdev:median(wlsol_quality.rms_nm),robsig:median(wlsol_quality.rms_nm)}

    return,kwl3d
end
