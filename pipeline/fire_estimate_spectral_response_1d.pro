function fire_estimate_spectral_response_1d,star_obs_inp,wl_inp,error=estar_obs_inp,$
    star_list=star_list,star_weights=star_weights,v0=v0,vsini=vsini,$
    maskoh=maskoh,bk_coarse=bk_coarse,$
    magv=magv,texp=texp,airmass=airmass,plot=plot

    if(n_elements(star_list) eq 0) then $
        star_list=[$
            getenv('FIRE_PIPELINE_PATH')+'calib_FIRE/stellar_templates/uni_vac/phx20_a+0.0_10000+0.00-4.5.fits'$
        ]
    n_star=n_elements(star_list)

    if(n_elements(airmass) ne 1) then airmass=1d
    if(n_elements(texp) ne 1) then texp=1d
    if(n_elements(magv) ne 1) then magv=5.00d

    if(n_elements(star_weights) ne n_star) then star_weights=dblarr(n_star)+1d
    if(n_elements(v0) ne 1) then v0=0.0
    if(n_elements(vsini) ne 1) then vsini=0.5
    if(n_elements(bk_coarse) ne 1) then bk_coarse_everyn=400/2. ;;40

    wl=wl_inp*10d

    for i=0,n_star-1 do begin
        star_cur=readfits(star_list[i],hstar,/silent)/1.52d24*10d^(-0.4d*(magv-0.03)) ;;; converting to Vega fluxes (erg/cm^2/s/A) and accounting for the visual magnitude of the star
        star_tmp=(i eq 0)? star_cur*star_weights[i] : star_tmp+star_cur*star_weights[i]
    endfor
    parse_spechdr,hstar,wl=wlstar,velscale=velScStar

    star_krnl=lsf_rotate_v0(velScStar,vsini,v0=-v0) 
    star_conv_tmp=convol(star_tmp,star_krnl)
    ; star_conv=interpol(star_conv_tmp,wlvac2atm(wlstar,/inv),wl,/spl)
    star_conv=interpol(star_conv_tmp,wlstar,wl,/spl)
    
    ext_vec=mage_calc_ext_lco(wl,airmass)
    mask_spec=byte(star_obs_inp*0)

    S=!pi*((650.0/2)^2-(100.0/2)^2) ;total square mirror of telescope in cm^2

    ;star_obs=total(spec2d[*,slitreg],2)/ext_vec/texp
    ;estar_obs=sqrt(total(error2d[*,slitreg]^2,2))/ext_vec/texp
    star_obs=star_obs_inp/ext_vec/texp
    estar_obs=estar_obs_inp/ext_vec/texp

    xx=wl
    yy=star_obs/(star_conv*S*wl*1d-8/(6.625d-27*2.99792458d10))
    wlmax=25000.

    iv=1/estar_obs^2/(star_conv*S*wl*1d-8/(6.625d-27*2.99792458d10))

    if(keyword_set(maskoh)) then begin
        lines_oh=read_asc(getenv('FIRE_PIPELINE_PATH')+'calib_FIRE/linelists/linesOH_R2k_HITRAN_H2O_nm.tab')
        lines_oh=lines_oh[*,where(lines_oh[2,*] gt 1e-1)]
        flag=mask_emission_lines(wl/10d,transpose(lines_oh[0,*]),0.15)
        mask_bad=where(flag gt 0, cmask_bad)
        if(cmask_bad gt 0) then mask_spec[mask_bad]=1
    endif
    mask_spec[where($
;                abs(wl-9350) lt  25.0 or $
                wl gt wlmax)]=1
    good_mask=where(mask_spec eq 0, cgood_mask, compl=bad_mask)
    g_vec=where(finite(yy+iv) eq 1 and yy gt 1d-4 and mask_spec eq 0,cg_vec,compl=b_vec)
    yy[b_vec]=interpol(yy[g_vec],xx[g_vec],xx[b_vec])
    iv[b_vec]=interpol(iv[g_vec],xx[g_vec],iv[b_vec])
    gwl=where(wl lt wlmax)
    sset2=bspline_iterfit(xx[g_vec]^0.5,yy[g_vec],ivar=iv[g_vec]^2,everyn=bk_coarse_everyn)
    dqe=bspline_valu(wl^0.5,sset2)

    flux_erg=1d/(dqe*S*wl*1d-8/(6.625d-27*2.99792458d10))
    flux_mJy=flux_erg*wl^2*3.3356E7

    flux_corr={wave:wl,dqe_blaze:dqe,flux_erg:flux_erg,flux_mJy:flux_mJy}

    if(keyword_set(plot)) then begin
        plot,xx,yy,xs=1,ys=1,xr=[8300,25000],/yl,yr=[0.01,1.0]
        oplot,wl,dqe,col=254,thick=3
;        oplot,wl,dqe2,col=128
    endif
    return,flux_corr
end
