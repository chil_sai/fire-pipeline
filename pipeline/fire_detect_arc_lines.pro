function fire_detect_arc_lines,arc_image,trace_coeff,fwhm,$
    obj_image=obj_image,maxrms=maxrms,ny=ny,x0=x0,wdir=wdir,ndeg_y=ndeg_y,$
    n_iter=n_iter,maxvisflux=maxvisflux,smy=smy,ord_red=ord_red,$
    display=display,dy_vis=dy_vis

if(n_elements(smy) ne 1) then smy=3
if(n_elements(n_iter) ne 1) then n_iter=2
if(n_elements(ndeg_y) ne 1) then ndeg_y=2
if(n_elements(wdir) ne 1) then wdir='./'
if(n_elements(ny) ne 1) then ny=51
if(n_elements(x0) ne 1) then x0=1024.0
if(n_elements(maxvisflux) ne 1) then maxvisflux=1000.0
if(n_elements(dy_vis) ne 1) then dy_vis=0
yh=(ny-1)/2

n_ord=(size(trace_coeff))[2]

line_struct={order:-1l,$
        xpos:dblarr(ny)+!values.d_nan,ypos:dblarr(ny)+!values.d_nan,$
        xfit:dblarr(ny)+!values.d_nan,yfit:dblarr(ny)+!values.d_nan,$
        flux:fltarr(ny)+!values.f_nan,rms:0d,$
        xmean:!values.d_nan,wavelength:!values.d_nan,species:''}
line_array=[line_struct]

if(keyword_set(display)) then window,0,xs=2048,ys=2048,xpos=832
x_vec=dindgen(65)*32d

;openw,u,wdir+'lines_detected_test.txt',/get_lun

for iter=0,n_iter-1 do begin
    arc_frame = ((iter eq n_iter-1) and (n_elements(arc_image) eq n_elements(obj_image)))? obj_image : arc_image
    if(keyword_set(display)) then begin
        loadct,0
        ;tv,bytscl(arc/flat_norm,0,800.0)
        tv,bytscl(arc_frame[*,dy_vis:*],0,maxvisflux)
        loadct,39
    endif

    for i=0,n_ord-1 do begin
        if(keyword_set(display)) then begin
            plots,x_vec,poly(x_vec-x0,trace_coeff[*,i,0])-dy_vis,col=128,/dev,thick=3
            plots,x_vec,poly(x_vec-x0,trace_coeff[*,i,1])-dy_vis,col=80,/dev,thick=3
        endif
        ord_cur=fire_extract_order(arc_frame,trace_coeff,i,ny=ny)
        if(iter eq 0) then begin
            lines_cur=fire_order_detect_lines(ord_cur,fwhm,smy=smy,curve_par=curve_par,flux=flux_all_cur,rms_arr=rms_arr,deg1=2,deg2=1,/fast,/quadpoly)
            if(i eq 0) then curve_arr=replicate(curve_par,n_ord)
        endif else begin
            k_rms=(keyword_set(ord_red) and (i ge n_ord-2))? 1.7 : 1.0
            k_fwhm=(keyword_set(ord_red) and (i ge n_ord-2))? 1.5 : 1.0
            lines_cur=fire_order_detect_lines(ord_cur,fwhm*k_fwhm,maxrms=maxrms*k_rms,smy=smy,inp_curve_par=curve_arr[i],curve_par=curve_par,flux=flux_all_cur,rms_arr=rms_arr,/fast,quadpoly=0);,/quadpoly)
        endelse
        curve_arr[i]=curve_par
        if(n_elements(lines_cur) eq 1) then continue
        n_lines=n_elements(lines_cur[0,*])
        if(n_lines gt 0) then begin
            for n=0,n_lines-1 do begin
                g_lines=where(finite(lines_cur[*,n]) eq 1 and $
                              lines_cur[*,n] gt 0 and $
                              lines_cur[*,n] lt 2047, cg_lines)
                if(cg_lines gt 0) then begin
                    ylines = fire_xnl2y(lines_cur[g_lines,n],g_lines*0d +i,double(g_lines)/(ny-1d),trace_coeff)
                    line_struct_curr=line_struct
                    line_struct_curr.order=32-1-i
                    line_struct_curr.xpos[g_lines]=lines_cur[g_lines,n]
                    line_struct_curr.ypos[g_lines]=ylines
                    if(iter lt n_iter-1) then begin
                        c=robust_poly_fit(double(g_lines),lines_cur[g_lines,n],ndeg_y)
                        line_struct_curr.xfit=poly(dindgen(ny),c)
                        line_struct_curr.yfit=fire_xnl2y(poly(dindgen(ny),c),dblarr(ny)*0d +i,dindgen(ny)/(ny-1d),trace_coeff)
                    endif
                    c=robust_poly_fit(double(g_lines),lines_cur[g_lines,n],1)
                    line_struct_curr.xmean=poly((double(ny)-1d)/2d,c)
                    line_struct_curr.flux=flux_all_cur[*,n]

                    line_struct_curr.rms=rms_arr[n]
                    if(iter eq n_iter-1) then line_array=[line_array,line_struct_curr]

                    if(keyword_set(display)) then plots,col=127+iter*127,psym=7,syms=0.5,/dev,$
                        [lines_cur[g_lines,n]],[ylines]-dy_vis
;                    if(iter eq n_iter-1) then begin
;                        printf,u,32-1-i,lines_cur[yh,n],flux_all_cur[yh,n],format='(i5,f10.2,f10.1)'
;                    endif
                endif
            endfor
            if(iter eq n_iter-1) then begin
                g_lines_order=where(line_array.order eq 32-1-i, cg_lines_order)
                if(cg_lines_order gt 1) then begin
                    line_data=dblarr(3,long(cg_lines_order)*long(ny))
                    line_data[0,*]=reform(((dblarr(ny)+1d)#line_array[g_lines_order].xmean),cg_lines_order*ny)
                    line_data[1,*]=reform(dindgen(ny) # (dblarr(cg_lines_order)+1d),cg_lines_order*ny)
                    line_data[2,*]=reform(line_array[g_lines_order].xpos,cg_lines_order*ny)
                    err_data=reform((line_array[g_lines_order].rms # (dblarr(ny) + 1d)),cg_lines_order*ny)
                    gldata=where(finite(total(line_data,1)) eq 1,cgldata)
                    f=sfit_2deg(line_data[*,gldata],err=err_data[gldata],1,ndeg_y,kx=kx2d,/irreg)
                    for l=0,cg_lines_order-1 do $
                        line_array[g_lines_order[l]].xfit=poly2d(dblarr(ny)+line_array[g_lines_order[l]].xmean,dindgen(ny),kx2d,deg1=1,deg2=ndeg_y,/irreg)
                endif
            endif
        endif
    endfor
    if(iter lt n_iter-1) then begin
        for x=0,curve_arr[0].deg2 do begin
            for y=0,curve_arr[0].deg1 do begin
                kk=robust_poly_fit(findgen(n_ord),curve_arr[*].kx[x,y],2)
                curve_arr[*].kx[x,y]=poly(findgen(n_ord),kk)
            endfor
        endfor
    endif
endfor

;close,u
;free_lun,u

if(n_elements(line_array) gt 1) then line_array=line_array[1:*]
return,line_array

end
