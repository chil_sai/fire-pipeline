function refpix, img, namps=namps, noedge=noedge

    if(n_elements(namps) ne 1) then namps=32
    smolen = 10
    nsig = 3

    ;message,/inf,'Running refpix on a H2RG frame'
    
    img_out=img

    s=size(img)
    ampsize=s[1]/namps

    refpix1=[1,2,3]
    refpix2=[findgen(4)+s[1]-4]
    refpix_all=[0,1,2,3,findgen(4)+s[1]-4]
    refvec=total(img[refpix_all,*],1)/n_elements(refpix_all)

;    refpix_all_1=refpix_all[findgen(4)*2]
;    refpix_all_2=refpix_all[findgen(4)*2+1]
;    refvec=(median(img[refpix_all_1,*],dim=1)+$
;            median(img[refpix_all_2,*],dim=1))/2.0

    if(not keyword_set(noedge)) then begin
        svec=smooth(refvec,11,/nan)
        img_out=img-congrid(transpose(svec),s[1],s[2])
    endif

    for amp=0,namps-1 do begin
        img_out_ref=img_out[*,[refpix1,refpix2]]
        resistant_mean,img_out_ref[amp*ampsize+2*findgen(ampsize/2),*],3.0,ref1,std1
        resistant_mean,img_out_ref[amp*ampsize+2*findgen(ampsize/2)+1,*],3.0,ref2,std2
        ref12=(ref1+ref2)/2.
;        print,'amp,ref=',amp,ref1,ref2,ref12

        img_out[amp*ampsize:(amp+1)*ampsize-1,*]=img_out[amp*ampsize:(amp+1)*ampsize-1,*]-ref12

    endfor


    return, img_out
end
