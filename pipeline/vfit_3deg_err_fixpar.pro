function vfit_3deg_err_fixpar, u, err=uerr, degree1, degree2, degree3, KX=kx_f, $
    IRREGULAR=irreg_p, MAX_DEGREE=max_p, zero_coeff=zero_coeff, fix_coeff=fix_coeff_orig, ini_kx=ini_kx_orig,$
    legendre=legendre,crd_map=crd_map
;+
; NAME:
;	VFIT_3DEG
;
; PURPOSE:
;	This function determines a polynomial fit to a surface sampled
;	  over a regular or irregular grid.
;
; CATEGORY:
;	Curve and surface fitting.
;
; CALLING SEQUENCE:
;	Result = SFIT(Data, Degree1, Degree2)    ;Regular input grid
;	Result = SFIT(Data, Degree1, Degree2, /IRREGULAR)  ;Irregular input grid
;
; INPUTS:
; 	Data:	The array of data to fit. If IRREGULAR
; 	is not set, the data are assumed to be sampled over a regular 2D
; 	grid, and should be in an Ncolumns by Nrows array.  In this case, the
; 	column and row subscripts implicitly contain the X and Y
; 	location of the point.  The sizes of the dimensions may be unequal.
;	If IRREGULAR is set, Data is a [3,n] array containing the X,
;	Y, and Z location of each point sampled on the surface.  
;
;	Degree:	The maximum degree of fit (in one dimension).
;
; KEYWORDS:
; 	IRREGULAR: If set, Data is [3,n] array, containing the X, Y,
; 	  and Z locations of n points sampled on the surface.  See
; 	  description above.
; 	MAX_DEGREE: If set, the Degree parameter represents the
; 	    maximum degree of the fitting polynomial of all dimensions
; 	    combined, rather than the maximum degree of the polynomial
; 	    in a single variable. For example, if Degree is 2, and
; 	    MAX_DEGREE is not set, then the terms returned will be
; 	    [[K, y, y^2], [x, xy, xy^2], [x^2, x^2 y, x^2 y^2]].
; 	    If MAX_DEGREE is set, the terms returned will be in a
; 	    vector, [K, y, y^2, x, xy, x^2], in which no term has a
; 	    power higher than two in X and Y combined, and the powers
; 	    of Y vary the fastest. 
;
;
; OUTPUT:
;	This function returns the fitted array.  If IRREGULAR is not
;	set, the dimensions of the result are the same as the
;	dimensions of the Data input parameter, and contain the
;	calculated fit at the grid points.  If IRREGULAR is set, the
;	result contains n points, and contains the value of the
;	fitting polynomial at the sample points.
;
; OUTPUT KEYWORDS:
;	Kx:	The array of coefficients for a polynomial function
;		of x and y to fit data. If MAX_DEGREE is not set, this
;		parameter is returned as a (Degree+1) by (Degree+1)
;		element array.  If MAX_DEGREE is set, this parameter
;		is returned as a (Degree+1) * (Degree+2)/2 element
;		vector. 
;
; PROCEDURE:
; 	Fit a 2D array Z as a polynomial function of x and y.
; 	The function fitted is:
;  	    F(x,y) = Sum over i and j of kx[j,i] * x^i * y^j
; 	where kx is returned as a keyword.  If the keyword MAX_DEGREE
; 	is set, kx is a vector, and the total of the X and Y powers will
; 	not exceed DEGREE, with the Y powers varying the fastest.
;
;-

   on_error, 2

   if(n_elements(crd_map) ne 6) then crd_map=[[0d,0d,0d],[1d,1d,1d]]

   s = size(u)
   if(n_elements(uerr) ne s[2]) then uerr=u[3,*]*0+1d
   irreg = keyword_set(irreg_p)
   max_deg = keyword_set(max_p)
   if(n_params() eq 2) then begin
      degree2=degree1
      degree3=degree1
   endif
   degrees=[degree1,degree2,degree3]
   srtdeg=sort(degrees) ;; determine the largest dimension
   n2 = (degree1+1)*(degree2+1)*(degree3+1) ;# of coeff to solve
   if(n_elements(fix_coeff_orig) ne n2) then begin
       if(n_elements(fix_coeff_orig) ne 0) then message,/inf,'fix_coeff has wrong size'
       fix_coeff=bytarr(degree1+1,degree2+1,degree3+1)
   endif else fix_coeff=fix_coeff_orig
   if(n_elements(ini_kx_orig) ne n2) then begin
       if(n_elements(ini_kx_orig) ne 0) then message,/inf,'ini_kx has wrong size'
       ini_kx=dblarr(degree1+1,degree2+1,degree3+1)
   endif else ini_kx=ini_kx_orig
   if(arg_present(zero_coeff)) then begin
       if(n_elements(zero_coeff) eq n2) then begin
           fix_coeff=zero_coeff
           ini_kx=double(zero_coeff)*0d
       endif
   endif

   if(max_deg) then begin
       for i=0,degree1 do $
           for j=0,degree2 do $
               for k=0,degree3 do $
                   if((i+j+k) gt degrees[srtdeg[2]]) then begin
                       ini_kx[i,j,k]=0d
                       fix_coeff[i,j,k]=1
                   endif
   endif
   good_coeff=where(fix_coeff eq 0, n2_red, compl=fixed_coeff, ncompl=n2_fix)

   if irreg then begin
       if (s[0] ne 2) or (s[1] ne 4) then $
         message, 'For IRREGULAR grids, input must be [4,n]'
       m = n_elements(u) / 4    ;# of points
       x = double(u[0,*])       ;Do it in double...
       y = double(u[1,*])
       z = double(u[2,*])
       if(crd_map[0,0] ne 0d and crd_map[0,1] ne 1d) then x=(x-crd_map[0,0])/crd_map[0,1]
       if(crd_map[1,0] ne 0d and crd_map[1,1] ne 1d) then y=(y-crd_map[1,0])/crd_map[1,1]
       if(crd_map[2,0] ne 0d and crd_map[2,1] ne 1d) then z=(z-crd_map[2,0])/crd_map[2,1]

       uu = double(u[3,*])
       if(n2_fix gt 0) then begin
           ini_kx_tmp=ini_kx
           ini_kx_tmp[good_coeff]=0d
           d_uu=poly3d(x,y,z,ini_kx_tmp,/irreg,legendre=legendre,crd_map=crd_map)
       endif else d_uu=0d
   endif else begin             ;Regular
       if s[0] ne 3 then message, 'For regular grids, input must be [nx, ny, nz]'
       nx = s[1]
       ny = s[2]
       nz = s[3]
       m = nx * ny * nz		;# of points to fit
       x = reform(reform(dindgen(nx) # replicate(1d, ny),nx*ny) # $
                  replicate(1d,nz),nx,ny,nz) ;X values at each point
       y = reform(reform(replicate(1d,nx) # dindgen(ny),nx*ny) # $
                  replicate(1d,nz),nx,ny,nz) ; Y values at each point
       z = reform(replicate(1d,nx*ny) # $
                  dindgen(nz),nx,ny,nz) ; Z values at each point
       if(crd_map[0,0] ne 0d and crd_map[0,1] ne 1d) then x=(x-crd_map[0,0])/crd_map[0,1]
       if(crd_map[1,0] ne 0d and crd_map[1,1] ne 1d) then y=(y-crd_map[1,0])/crd_map[1,1]
       if(crd_map[2,0] ne 0d and crd_map[2,1] ne 1d) then z=(z-crd_map[2,0])/crd_map[2,1]
       if(n2_fix gt 0) then begin
           ini_kx_tmp=ini_kx
           ini_kx_tmp[good_coeff]=0d
           d_u=poly3d(x,y,z,ini_kx_tmp,legendre=legendre,crd_map=crd_map)
       endif else d_u=0d
   endelse

   if(keyword_set(legendre)) then begin
       g_crd=where((x_crd ge -1d and x_crd le 1d) and $
                   (y_crd ge -1d and y_crd le 1d) and $
                   (z_crd ge -1d and z_crd le 1d),cg_crd,compl=b_crd,ncompl=cb_crd)
       if(cb_crd gt 0) then message,/inf,string(cb_crd,format='(i)')+' points have coordinates outside the [-1,1] range. Returning NaNs for them.'
   endif else begin
       cb_crd=0l
       b_crd=[-1l]
       g_crd=lindgen(m)
       cg_crd=m
   endelse

   if n2_red gt m then message, 'Fitting degrees of '+strtrim(degree1,2)+','+strtrim(degree2,2)+','+strtrim(degree3,2)+$
     ' requires ' + strtrim(n2_red,2) + ' points.'
   ut = dblarr(n2_red, m, /nozero)
   k = 0L
   k_f = 0L
   for h=0,degree3 do begin
       for j=0,degree2 do begin ;Fill each column of basis
           for i=0, degree1 do begin
               ;if max_deg and (i+j+h gt degrees[srtdeg[2]]) then continue
               k_f = k_f + 1L
               if ((reform(fix_coeff))[k_f-1L] ne 0) then continue
               if(keyword_set(legendre)) then $
                   ut[k, g_crd] = reform(legendre(x[g_crd],i) * legendre(y[g_crd],j) * legenrde(z[g_crd],h), 1, cg_crd)/transpose(uerr[g_crd]) $
               else $
                   ut[k, 0] = reform(x^i * y^j * z^h, 1, m)/transpose(uerr)
               k = k + 1L
           endfor
       endfor
   endfor

   kx = la_least_squares(ut[*,g_crd],(reform(irreg ? (uu-d_uu) : (u-d_u), m))[g_crd]/reform(uerr[g_crd],cg_crd),/double,resid=chi2)

   kx_f = dblarr(n2)
   kx_f[good_coeff] = kx
   if(n2_fix gt 0) then kx_f[fixed_coeff]=ini_kx[fixed_coeff]
   kx_f = reform(kx_f, degree1+1, degree2+1, degree3+1)

   return, irreg ? reform(reform(kx_f[good_coeff],n2_red) # ut, m)*uerr+d_uu : $ ;Return the fit
                   reform((reform(kx_f[good_coeff],n2_red) # ut)*uerr, nx, ny, nz)+d_u
end
