function create_sky_echelle_order,sky_img,isky_img=isky_img,$
    wlmap,l_map,n_map,n,sky_mask=sky_mask,dimensions=dimensions,$
    gain=gain, rdnoise=rdnoise, npoly=npoly,_Extra=extra_kw

    if(n_elements(sky_mask) eq 0) then sky_mask=1
    if(n_elements(dimensions) ne 1) then dimensions=1
    if(dimensions eq 2 and n_elements(npoly) ne 1) then npoly=1

    sky_model=sky_img*0d

    l_map_mask=byte(l_map*0.0)
    n_sky_mask=n_elements(sky_mask)
    for l=0,n_sky_mask-1 do begin
        l_min=0.0 > (l/double(n_sky_mask)) < 1.0
        l_max=0.0 > ((l+1)/double(n_sky_mask)) < 1.0
        if(sky_mask[l] eq 1) then begin
            l_cur=where(l_map ge l_min and l_map le l_max, cl_cur)
            if(cl_cur gt 0) then l_map_mask[l_cur]=1
        endif
    endfor

    if(n_elements(gain) ne 1) then gain=1.0
    if(n_elements(rdnoise) ne 1) then rdnoise=1.0

    if(n_elements(isky_img) ne n_elements(sky_img)) then isky_img= gain^2 / (gain *abs(sky_img) + rdnoise^2)

    for i=0,n_elements(n)-1 do begin
        pix_cur=where(n_map eq n[i] and l_map_mask eq 1 and finite(sky_img+isky_img) eq 1, cpix_cur)
        pix_cur_all=where(n_map eq n[i], cpix_cur_all)
        if(cpix_cur lt 200) then begin
            print,'order='+string(n[i],form='(i3)')+' contains too few points to sample sky Np='+string(cpix_cur,form='(i3)')
            continue
        endif
        x_sky=wlmap[pix_cur]
        sky=sky_img[pix_cur]
        isky=isky_img[pix_cur]
        y_sky=l_map[pix_cur]
        s_x=sort(x_sky)
        x_sky=x_sky[s_x]
        sky=sky[s_x]
        isky=isky[s_x]
        y_sky=y_sky[s_x]

        
        kk=where(finite(isky) eq 1, ckk)
        if(ckk lt 200) then begin
            print,'order='+string(n[i],form='(i3)')+' contains too few points to sample sky Np='+string(cpix_cur,form='(i3)')
            continue
        endif


        print,'Creating an oversampled sky model for order n='+string(n[i],form='(i3)')
        sset = (dimensions eq 1)? $
            bspline_iterfit(x_sky, sky, invvar=isky, _Extra=extra_kw) : $
            bspline_iterfit(x_sky, sky, x2=y_sky, invvar=isky, npoly=npoly, _Extra=extra_kw)

        x_sky_all=wlmap[pix_cur_all]
        y_sky_all=l_map[pix_cur_all]
;stop
        print,'Evaluating sky model for order n='+string(n[i],form='(i3)')
        sky_model_cur = (dimensions eq 1)? $
            bspline_valu(x_sky_all,sset) : $
            bspline_valu(x_sky_all,x2=y_sky_all, sset)
        sky_model[pix_cur_all]=sky_model_cur
    endfor
    

    return,sky_model
end
