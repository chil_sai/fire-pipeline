function fire_clean_frame_med2d,image,w1,w2,thrx=thrx,thry=thry,subtact=subtract,nan=nan
    if(n_params() lt 3) then w2=3
    if(n_params() lt 2) then w1=3
    if(w1 lt 3) then w1=3
    if(w2 lt 3) then w2=3
    if(n_elements(thrx) ne 1) then thrx=0.08
    if(n_elements(thry) ne 1) then thry=0.08

    s_im=size(image)
    image_out=image
    image_medx=image
    for y=0,s_im[2]-1 do image_medx[*,y]=median(image[*,y],w1)
    image_medy=image
    for x=0,s_im[1]-1 do image_medy[x,*]=transpose(median(transpose(image[x,*]),w2))
    image_med=median(image,(w1>w2))

    bpix=(keyword_set(subtract))? $
        where(abs(image_medy-image) ge thry and abs(image_medx-image) ge thrx, cbpix) : $
        where(abs(image_medy/image-1d) ge thry and abs(image_medx/image-1d) ge thrx, cbpix)

    if(cbpix gt 0) then image_out[bpix]=(keyword_set(nan))? !values.d_nan : image_med[bpix]

    return,image_out
end
