function fire_merge_orders,star_extr_2d,flatn_extr_2d,$
    extr_prof_2d,kwl3d,$
    ord_corr,wl_out=wl_m,ord_wl_shift=ord_wl_shift,$
    gain=gain,rdnoise=rdnoise,star_p_extr_2d=star_p_extr_2d,$
    ord_all=ord_all,sky_all=sky_all,err_ord_all=err_ord_all,flatn_thr=flatn_thr,$
    autowl=autowl,sky_merged=sky_merged,specerr_merged=specerr_merged,$
    clean=clean,$
    no_illum_corr=no_illum_corr,$
    ord_conv_sigma=ord_conv_sigma,$
    correct_telluric=correct_telluric,$
    telluric_data=telluric_data,$
    spec_2d_merged=spec_2d_merged,$
    specerr_2d_merged=specerr_2d_merged,$
    spline=spline,quadratic=quadratic,lsquadratic=lsquadratic,$
    legendre=legendre,m0=m0,crd_map=crd_map

    if(keyword_set(autowl)) then $
        wl_m=830.0d +dindgen(37500l)*0.045d

    if(n_elements(gain) ne 1) then gain=3.8d
    if(n_elements(rdnoise) ne 1) then rdnoise=17d
    if(n_elements(star_p_extr_2d) ne n_elements(star_extr_2d)) then star_p_extr_2d=abs(star_extr_2d)

    if(n_elements(ord_conv_sigma) ne 1) then ord_conv_sigma=0d
    n_wl_m=n_elements(wl_m)
    s_extr=size(star_extr_2d)
    ny_ord=s_extr[2]
    n_ord=s_extr[3]

    if(n_elements(flatn_thr) ne 1) then flatn_thr=0.01

    ic_wl_arr=(keyword_set(no_illum_corr))? dblarr(nx,n_ord)+1d : fire_model_illumination_correction(/twod)

    ord_all=dblarr(n_wl_m,n_ord)
    err_ord_all=dblarr(n_wl_m,n_ord)
    ord_2d_all=dblarr(n_wl_m,ny_ord,n_ord)
    err_ord_2d_all=dblarr(n_wl_m,ny_ord,n_ord)
    sky_all=dblarr(n_wl_m,n_ord)
    wgt_all=dblarr(n_wl_m,n_ord)
    for i=0,n_ord-1 do begin
        print,'Preparing order #'+string(31-i,format='(i3)')
        wl_ord_idx=where(wl_m gt min(ord_corr[i].wl,max=wlmaxcur) and wl_m lt wlmaxcur, cwl_ord_idx)
        if(cwl_ord_idx eq 0) then continue
        wl_ord_cur=wl_m[wl_ord_idx]
        if(n_elements(ord_wl_shift) eq n_ord) then begin
            case n_elements(ord_wl_shift[i].wl_shift) of
                0 : p=i
                1 : wl_ord_cur=wl_ord_cur-ord_wl_shift[i].wl_shift[0]
                2 : wl_ord_cur=wl_ord_cur-interpol(ord_wl_shift[i].wl_shift,ord_wl_shift[i].wl_coord,wl_m[wl_ord_idx])
                3 : wl_ord_cur=wl_ord_cur-interpol(ord_wl_shift[i].wl_shift,ord_wl_shift[i].wl_coord,wl_m[wl_ord_idx],/quad)
                else : wl_ord_cur=wl_ord_cur-interpol(ord_wl_shift[i].wl_shift,ord_wl_shift[i].wl_coord,wl_m[wl_ord_idx],/spl)
            endcase
        endif
        ic_wl_vec=interpol(ic_wl_arr[*,i],ord_corr[i].wl,wl_ord_cur,spline=spline,quadratic=quadratic,lsquadratic=lsquadratic)
        order_cur=fire_order_linearisation(star_extr_2d[*,*,i],i,kwl3d,wl_ord_cur,spline=spline,quadratic=quadratic,lsquadratic=lsquadratic,legendre=legendre,m0=m0,crd_map=crd_map)/rebin(ic_wl_vec,cwl_ord_idx,ny_ord)
        order_p_cur=fire_order_linearisation(star_p_extr_2d[*,*,i],i,kwl3d,wl_ord_cur,spline=spline,quadratic=quadratic,lsquadratic=lsquadratic,legendre=legendre,m0=m0,crd_map=crd_map)/rebin(ic_wl_vec,cwl_ord_idx,ny_ord)
        flatn_cur=fire_order_linearisation(flatn_extr_2d[*,*,i],i,kwl3d,wl_ord_cur,spline=spline,quadratic=quadratic,lsquadratic=lsquadratic,legendre=legendre,m0=m0,crd_map=crd_map)/rebin(ic_wl_vec,cwl_ord_idx,ny_ord)
        extr_fit_cur=dblarr(cwl_ord_idx,ny_ord)
        extr_flat_cur=dblarr(cwl_ord_idx,ny_ord)
        extr_flat_cur[*,5:ny_ord-6]=1d/double(ny_ord-10)
        for y=0,ny_ord-1 do extr_fit_cur[*,y]=interpol(extr_prof_2d[*,y,i],ord_corr[i].wl,wl_ord_cur,spline=spline,quadratic=quadratic,lsquadratic=lsquadratic)
        ord_corr_cur=(ord_corr[i].lin_order eq 0)? interpol(ord_corr[i].correction_vector,ord_corr[i].wl,wl_ord_cur) : congrid(ord_corr[i].correction_vector_lin,n_elements(wl_ord_cur),/interp)
        ord_corr_cur_2d=rebin(ord_corr_cur,n_elements(ord_corr_cur),ny_ord)
        order_cur_var=(((order_p_cur*flatn_cur)>0)*gain+rdnoise^2)/flatn_cur^2/gain^2/((wl_m[1]-wl_m[0])/(ord_corr[i].wl[1]-ord_corr[i].wl[0]))

        order_cur/=ord_corr_cur_2d
        order_p_cur/=ord_corr_cur_2d
        order_cur_var/=ord_corr_cur_2d
        ord_x_cur=fire_extract_lin_order(order_cur,extr_fit_cur,var2d=order_cur_var,sky=sky_cur,clean=clean,error=err_ord_x_cur)
        tmp_flat_x_cur=fire_extract_lin_order(flatn_cur,extr_flat_cur,sum=flat_x_cur,clean=clean)

        if(ord_conv_sigma gt 0) then begin
            ord_x_cur=fire_convol_order(wl_m[wl_ord_idx],ord_x_cur,ord_conv_sigma)
            sky_cur=fire_convol_order(wl_m[wl_ord_idx],sky_cur,ord_conv_sigma)
            err_ord_x_cur=fire_convol_order(wl_m[wl_ord_idx],err_ord_x_cur,ord_conv_sigma)
            order_cur=fire_convol_order(wl_m[wl_ord_idx],order_cur,ord_conv_sigma)
            order_cur_var=fire_convol_order(wl_m[wl_ord_idx],order_cur_var,ord_conv_sigma)
        endif
        if(keyword_set(correct_telluric) and n_elements(telluric_data) eq n_ord) then begin
            telluric_order=interpol(telluric_data[i].transmission,telluric_data[i].wl,wl_m[wl_ord_idx],spline=spline,quadratic=quadratic,lsquadratic=lsquadratic) ;,/spl)
            ord_x_cur/=telluric_order
            err_ord_x_cur/=telluric_order
            order_cur/=congrid(reform(telluric_order,n_elements(telluric_order),1),n_elements(telluric_order),ny_ord)
            order_cur_var/=congrid(reform(telluric_order,n_elements(telluric_order),1),n_elements(telluric_order),ny_ord)
        endif
        ord_all[wl_ord_idx,i]=ord_x_cur
        err_ord_all[wl_ord_idx,i]=err_ord_x_cur
        sky_all[wl_ord_idx,i]=sky_cur
        ord_2d_all[wl_ord_idx,*,i]=order_cur
        err_ord_2d_all[wl_ord_idx,*,i]=sqrt(order_cur_var)
        wgt_all[wl_ord_idx,i]=1d
        bflat=where(flat_x_cur le flatn_thr or (finite(ord_corr_cur) ne 1),cbflat)
        if(cbflat gt 0) then wgt_all[wl_ord_idx[bflat],i]=0d
        blue_end_overlap_idx=where((finite(ord_corr_cur) eq 1) and $
                                   (flat_x_cur gt flatn_thr) and $
                                   (wl_ord_cur le ord_corr[i].wl[ord_corr[i].overlap_end]),cblue_end_overlap_idx)
        if(i eq 0) then cblue_end_overlap_idx=0
        if(cblue_end_overlap_idx gt 1) then begin
            wgt_all[wl_ord_idx[blue_end_overlap_idx],i]=dindgen(cblue_end_overlap_idx)/double(cblue_end_overlap_idx-1)*flat_x_cur[blue_end_overlap_idx]
        endif
        red_end_overlap_idx= where((finite(ord_corr_cur) eq 1) and $
                                   (flat_x_cur gt flatn_thr) and $
                                   (wl_ord_cur gt ord_corr[i].wl[ord_corr[i].overlap_start]),cred_end_overlap_idx)
        if(i eq n_ord-1) then cred_end_overlap_idx=0
        if(cred_end_overlap_idx gt 1) then begin
            wgt_all[wl_ord_idx[red_end_overlap_idx],i]=(1d -dindgen(cred_end_overlap_idx)/double(cred_end_overlap_idx-1))*flat_x_cur[red_end_overlap_idx]
        endif
    endfor

    spec_merged=total(ord_all*wgt_all,2,/nan)/total(wgt_all,2)
    specerr_merged=sqrt(total((err_ord_all^2)*wgt_all,2,/nan)/total(wgt_all,2))
    sky_merged=total(sky_all*wgt_all,2,/nan)/total(wgt_all,2)

    wgt_2d_all=congrid(reform(wgt_all,n_wl_m,1,n_ord),n_wl_m,ny_ord,n_ord)
    spec_2d_merged=total(ord_2d_all*wgt_2d_all,3,/nan)/total(wgt_2d_all,3)
    specerr_2d_merged=sqrt(total((err_ord_2d_all^2)*wgt_2d_all,3,/nan)/total(wgt_2d_all,3))

    return,spec_merged
end
