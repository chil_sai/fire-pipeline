function fire_model_illumination_correction,twod=twod
    if(keyword_set(twod)) then begin
    ;;;; Internal flat illumination correction in the wavelength direction 
    ;;;; determined from the telluric star HIP86098 observed on 2016/07/19
        k_red=poly(findgen(21),[1.72983,0.00401200,-0.00424468])
        k_red[18:20]=[1.0,4.0,10.0]
        ic_wlflat=dblarr(2048,21)
        ic_vv2=dblarr(2048)+1d
        ic_a0=dblarr(21)+0.8d
        ic_a0[8:15]+=0.021
        ic_a0[18]-=0.010
        for o=0,20 do begin
            ic_xc=490d +210.-(o<6)*35
            ic_ww=130d ;+10
            ic_xtr=ic_xc-ic_ww
            ic_vv1=0.2*dindgen(2048)/ic_xc+ic_a0[o]
            if(o le 4) then ic_vv1[0:199-20*(o eq 4)]-=(0.1-0.02*(o eq 4))*cos(dindgen(200-20*(o eq 4))/(200-20*(o eq 4))*(!dpi/2d))^2
            if(o eq 3) then ic_vv1[0:299]-=0.025*sin(dindgen(300)/300*!dpi)
;            if(o eq 20) then ic_vv1[0:199]+=0.08*cos(dindgen(200)/200*(!dpi/2d))^2
            ic_wlflat[*,o]=ic_vv1+(1d +tanh((1d/ic_ww)*(dindgen(2048)-ic_xtr)))/2.0*(ic_vv2-ic_vv1) 
            ic_wlflat[1300:*,o]+=(dindgen(748)/3500.)^2*k_red[o]
        endfor
;        ic_wlflat[*,20]=ic_wlflat[*,19]
    endif else begin
    ;;;; Internal flat illumination correction in the wavelength direction 
    ;;;; determined from the telluric star HIP25280 observed on 2015/02/05
        ic_a0=0.8d
        ic_xc=490d
        ic_ww=130d +10
        ic_xtr=ic_xc-ic_ww
        ic_vv1=0.2*dindgen(2048)/ic_xc+ic_a0
        ic_vv2=dblarr(2048)+1d
        ic_wlflat=ic_vv1+(1d +tanh((1d/ic_ww)*(dindgen(2048)-ic_xtr)))/2.0*(ic_vv2-ic_vv1) 
        ic_wlflat[1300:*]+=(dindgen(748)/3500.)^2*1.6 ;; 2.0
    endelse

    return, ic_wlflat
end
