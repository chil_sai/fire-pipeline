function fire_get_wloffset_telluric,wl_new,ord_extr_fit,exclreg=exclreg,$
    star_list=star_list,err_ord_extr_fit=err_ord_extr_fit,atm_model_grid_file=atm_model_grid_file,$
    airmass=airmass,diff_airmass=diff_airmass,fit_airmass=fit_airmass,fit_vsini=fit_vsini,maskoh=maskoh,maskh2o=maskh2o,$
    vr0=vr0,vsini0=vsini0,novsini=novsini,lsf_moments=lsf_moments,lsf_nsegments=lsf_nsegments,$
    ord_vel_par=ord_vel_par,ord_lsf_par=ord_lsf_par,ord_airmass=ord_airmass,$
    plot=plot,fix_lsf_sig=fix_lsf_sig,fix_pwv=fix_pwv,fit_pwv=fit_pwv,ord_pwv=ord_pwv,fire_red_lsf=fire_red_lsf,fire_lsf_045=fire_lsf_045,$
    mdegree=mdegree,swlnpoints=swlnpoints,flag_swlstart=flag_swlstart,$
    ord_conv_sigma=ord_conv_sigma,omin=omin,omax=omax,first_ord_n=first_ord_n,$
    gausslosvd=gausslosvd,no_lsf_sig_rel=no_lsf_sig_rel,mage=mage,esi=esi,$
    calib_path=calib_path,fix_star_weights=fix_star_weights,star_weights=weights,atmwl=atmwl,clean=clean,$
    maxwl_global=maxwl_global,minwl_global=minwl_global,trans_thr=trans_thr,pixconv=pixconv

    n_wl=n_elements(wl_new[*,0])
    n_ord=n_elements(wl_new[0,*])

    if(keyword_set(mage) or keyword_set(esi)) then begin
        if(n_elements(first_ord_n) ne 1) then first_ord_n=(keyword_set(esi))? 15-(10-n_ord) : 19-(14-n_ord)
        if(n_elements(ord_vel_par) ne 1) then ord_vel_par=n_ord-1
        if(n_elements(ord_lsf_par) ne 1) then ord_lsf_par=n_ord-1
        if(n_elements(swlnpoints) ne 1) then swlnpoints=3
        if(n_elements(calib_path) ne 1) then calib_path=getenv('MAGE_PIPELINE_PATH')+'calib_MagE/'
        if(~arg_present(atmwl)) then atmwl=1
        no_lsf_sig_rel=1
    endif
    if(n_elements(calib_path) ne 1) then calib_path=getenv('FIRE_PIPELINE_PATH')+'calib_FIRE/'
    if(n_elements(diff_airmass) ne 1) then diff_airmass=0d
    if(n_elements(first_ord_n) ne 1) then first_ord_n=32-1
    if(n_elements(vr0) ne 1) then vr0=5d
    if(n_elements(vsini0) ne 1) then vsini0=5d
    if(n_elements(mdegree) ne 1) then mdegree=7
    if(n_elements(swlnpoints) ne 1) then swlnpoints=7
    if(n_elements(ord_vel_par) ne 1) then ord_vel_par=2
    if(n_elements(ord_lsf_par) ne 1) then ord_lsf_par=8
    if(n_elements(airmass) ne 1) then airmass=1.1
    if(n_elements(lsf_nsegments) ne 1) then lsf_nsegments=1
    kx_lsf=[28.429274d,-0.031450734d,2.2347728d-05,-4.0259957d-09,-0.34411633d,0.00067631384d,-1.8351373d-07,0.0033947370d,-1.1595654d-05]
    deg1_lsf=2
    deg2_lsf=3
    if(keyword_set(fire_lsf_045)) then begin
        lsf_nsegments=1
        lsf_moments_ord=0
    endif else lsf_moments_ord=(n_elements(lsf_moments) eq 1)? lsf_moments : 2

    seg_overlap=0.2
    skippix=5
    if(n_elements(ord_conv_sigma) ne 1) then ord_conv_sigma=0d

    ;if(keyword_set(plot)) then window,0,xs=1400,ys=600

    if(n_elements(star_list) lt 1) then star_list=calib_path+'stellar_templates/phoenix/lte09600-4.50-0.0.PHOENIX-ACES-AGSS-COND-2011-HiRes.fits.gz'
    n_star=n_elements(star_list)
    ;;;lsf_sig_rel=poly(transpose(wl_new[1024,*]),[16.924702d,0.0086281d,-5.2605343d-06,1.0290791d-09])/21.449549d ;;; determined from HIP25280
    ;;;; the polynomial coefficients below were determined from the analysis of the HIP25280 telluric spectrum taken on 2015/Feb/05
    ;;;; using the following FIRE orders: 3,8,13,16,17,18,20
    no_lsf_sig_rel=1
    lsf_sig_rel=(keyword_set(no_lsf_sig_rel))? dblarr(n_ord)+1d : poly(transpose(wl_new[1024,*]),[30.161315d,-0.017694986d,1.0773215d-05,-1.9995672d-09])/21.026340d ;;; determined from HIP25280

    spec=ord_extr_fit[*,ord_lsf_par]
    errspec=(n_elements(ord_extr_fit) eq n_elements(err_ord_extr_fit))? err_ord_extr_fit[*,ord_lsf_par] : sqrt(spec>0.01)*10d
    if(ord_conv_sigma gt 0) then begin
        spec=fire_convol_order(wl_new[*,ord_lsf_par],spec,ord_conv_sigma)
        errspec=fire_convol_order(wl_new[*,ord_lsf_par],errspec,ord_conv_sigma)
    endif

    dwlkms=(wl_new[1]-wl_new[0])*299792.458d/wl_new[0]
    
    idxmin=lonarr(lsf_nsegments)
    idxmax=lonarr(lsf_nsegments)
    for n=0,lsf_nsegments-1 do begin
        idxmin[n] = 0L > ((n eq 0)? skippix : (double(n)-seg_overlap)*n_wl/lsf_nsegments) < (n_wl-1L)
        idxmax[n] = 0L > ((n eq lsf_nsegments-1)? n_wl-skippix-1L : (double(n)+1.0+seg_overlap)*n_wl/lsf_nsegments) < (n_wl-1L)
        p_tel_lsf_cur=fire_fit_telluric_spectrum(wl_new[idxmin[n]:idxmax[n],ord_lsf_par],$
            spec[idxmin[n]:idxmax[n]],error=errspec[idxmin[n]:idxmax[n]],lsf_moments=lsf_moments,maskoh=maskoh,maskh2o=maskh2o,$
            mdegree=mdegree,start=[vr0,vsini0,airmass,3.0d,(dwlkms*0.11d)> 20d < (dwlkms*14.99d)],fix=[0,keyword_set(novsini),1-keyword_set(fit_airmass),0,0],$
            star_list=star_list,atm_model_grid_file=atm_model_grid_file,exclreg=exclreg,$
            maxwl_global=maxwl_global,minwl_global=minwl_global,trans_thr=trans_thr,pixconv=pixconv,atmwl=atmwl,clean=0,$
            swlnpoints=swlnpoints,plot=plot,wl_s_v=wl_s_vec,wl_s_s=wl_s_str,err_p=ep_tel_lsf,chi2=chi2dof,$
            gausslosvd=gausslosvd,calib_path=calib_path,weights=weights,w_norm=w_norm)
        if(n eq 0) then p_tel_lsf=rebin(p_tel_lsf_cur,n_elements(p_tel_lsf_cur),lsf_nsegments) else p_tel_lsf[*,n]=p_tel_lsf_cur
    endfor    
    swlstart=(swlnpoints gt 0 and keyword_set(flag_swlstart))? wl_s_str.wl_shift : dblarr(swlnpoints>1)
    if((ord_lsf_par eq ord_vel_par)) then p_tel_vel=p_tel_lsf else begin
        spec=ord_extr_fit[*,ord_vel_par]
        errspec=(n_elements(ord_extr_fit) eq n_elements(err_ord_extr_fit))? err_ord_extr_fit[*,ord_vel_par] : sqrt(spec>0.01)*10d
        if(ord_conv_sigma gt 0) then begin
            spec=fire_convol_order(wl_new[*,ord_lsf_par],spec,ord_conv_sigma)
            errspec=fire_convol_order(wl_new[*,ord_lsf_par],errspec,ord_conv_sigma)
        endif

        p_tel_vel=fire_fit_telluric_spectrum(wl_new[skippix:n_wl-skippix-1L,ord_vel_par],$
            spec[skippix:n_wl-skippix-1L],error=errspec[skippix:n_wl-skippix-1L],lsf_moments=lsf_moments,maskoh=maskoh,maskh2o=maskh2o,$
            mdegree=mdegree,start=[vr0,vsini0,airmass>1.0,3.0d,(dwlkms*0.11d) > (p_tel_lsf[4]*lsf_sig_rel[ord_vel_par]/lsf_sig_rel[ord_lsf_par]) < (dwlkms*14.99d)],exclreg=exclreg,$
            fix=[0,0,1-keyword_set(fit_airmass)*(n_elements(ord_airmass) ne 1),0,1],star_list=star_list,atm_model_grid_file=atm_model_grid_file,$
            maxwl_global=maxwl_global,minwl_global=minwl_global,trans_thr=trans_thr,pixconv=pixconv,atmwl=atmwl,clean=clean,$
            swlnpoints=swlnpoints,swlstart=swlstart,plot=plot,wl_s_v=wl_s_vec,wl_s_s=wl_s_str,err_p=ep_tel_vel,chi2=chi2dof,$
            gausslosvd=gausslosvd,calib_path=calib_path,weights=out_weights,w_norm=w_norm)
;            inp_star_weights=(keyword_set(fix_star_weights)? weights : (dblarr(n_star+1)+1d)))
        ;if(~keyword_set(fix_star_weights)) then weights=out_weights
        weights=out_weights
        for n=0,lsf_nsegments-1 do begin
            p_tel_lsf[0:1,n]=p_tel_vel[0:1]
        endfor
    endelse

    p_tel_lsf_ref=(lsf_moments_ord gt 0)? p_tel_lsf[*,0] : p_tel_lsf[0:3,0]
    ep_tel_lsf_ref=(lsf_moments_ord gt 0)? ep_tel_lsf[*,0] : ep_tel_lsf[0:3,0]

    if(keyword_set(fit_airmass) and n_elements(ord_airmass) eq 1) then begin
        if(ord_airmass eq ord_vel_par) then p_tel_am=p_tel_vel else $
        if(ord_airmass eq ord_lsf_par) then p_tel_am=p_tel_lsf else begin
            spec=ord_extr_fit[*,ord_airmass]
            errspec=(n_elements(ord_extr_fit) eq n_elements(err_ord_extr_fit))? err_ord_extr_fit[*,ord_airmass] : sqrt(spec>0.01)*10d
            if(ord_conv_sigma gt 0) then begin
                spec=fire_convol_order(wl_new[*,ord_lsf_par],spec,ord_conv_sigma)
                errspec=fire_convol_order(wl_new[*,ord_lsf_par],errspec,ord_conv_sigma)
            endif

            p_tel_am=fire_fit_telluric_spectrum(wl_new[skippix:n_wl-skippix-1L,ord_airmass],$
                spec[skippix:n_wl-skippix-1L],error=errspec[skippix:n_wl-skippix-1L],lsf_moments=lsf_moments,maskoh=maskoh,maskh2o=maskh2o,exclreg=exclreg,$
                mdegree=mdegree,start=[p_tel_lsf_ref[0],p_tel_lsf_ref[1],airmass>1.0,3.0d,(dwlkms*0.11d) > (p_tel_lsf[4]*lsf_sig_rel[ord_airmass]/lsf_sig_rel[ord_lsf_par]) < (dwlkms*14.99d)],$
                fix=[1,1,1-keyword_set(fit_airmass),0,0],star_list=star_list,atm_model_grid_file=atm_model_grid_file,$
                maxwl_global=maxwl_global,minwl_global=minwl_global,trans_thr=trans_thr,pixconv=pixconv,atmwl=atmwl,clean=clean,$
                swlnpoints=swlnpoints,swlstart=swlstart,plot=plot,wl_s_v=wl_s_vec,wl_s_s=wl_s_str,err_p=ep_tel_am,$
                chi2=chi2dof,gausslosvd=gausslosvd,calib_path=calib_path,weights=out_weights,$
                inp_star_weights=(keyword_set(fix_star_weights)? weights : (dblarr(n_star+1)+1d)),w_norm=w_norm)
            p_tel_lsf[2,*]=p_tel_am[2]
            ;if(~keyword_set(fix_star_weights)) then weights=out_weights
        endelse
        airmass=(p_tel_lsf[2]>1.0)
        fit_airmass=0
    endif

    if(keyword_set(fix_pwv) and keyword_set(fit_pwv) and n_elements(ord_pwv) eq 1) then begin
        if(ord_pwv eq ord_vel_par) then p_tel_pwv=p_tel_vel else $
        if(ord_pwv eq ord_lsf_par) then p_tel_pwv=p_tel_lsf else $
        if(ord_pwv eq ord_airmass and n_elements(p_tel_am) gt 0) then begin
            p_tel_pwv=p_tel_am
            p_tel_lsf[3,*]=p_tel_am[3]
        endif else begin
            spec=ord_extr_fit[*,ord_pwv]
            errspec=(n_elements(ord_extr_fit) eq n_elements(err_ord_extr_fit))? err_ord_extr_fit[*,ord_pwv] : sqrt(spec>0.01)*10d
            if(ord_conv_sigma gt 0) then begin
                spec=fire_convol_order(wl_new[*,ord_lsf_par],spec,ord_conv_sigma)
                errspec=fire_convol_order(wl_new[*,ord_lsf_par],errspec,ord_conv_sigma)
            endif

            p_tel_pwv=fire_fit_telluric_spectrum(wl_new[skippix:n_wl-skippix-1L,ord_pwv],$
                spec[skippix:n_wl-skippix-1L],error=errspec[skippix:n_wl-skippix-1L],lsf_moments=lsf_moments,maskoh=maskoh,maskh2o=maskh2o,exclreg=exclreg,$
                mdegree=mdegree,start=[p_tel_lsf_ref[0],p_tel_lsf_ref[1],airmass>1.0,3.0d,p_tel_lsf[4]*lsf_sig_rel[ord_pwv]/lsf_sig_rel[ord_lsf_par]],$
                fix=[1,1,1-keyword_set(fit_airmass),0,0],star_list=star_list,atm_model_grid_file=atm_model_grid_file,$
                maxwl_global=maxwl_global,minwl_global=minwl_global,trans_thr=trans_thr,pixconv=pixconv,atmwl=atmwl,clean=clean,$
                swlnpoints=swlnpoints,swlstart=swlstart,plot=plot,wl_s_v=wl_s_vec,wl_s_s=wl_s_str,err_p=ep_tel_am,$
                chi2=chi2dof,gausslosvd=gausslosvd,calib_path=calib_path,weights=out_weights,$
                inp_star_weights=(keyword_set(fix_star_weights)? weights : (dblarr(n_star+1)+1d)),w_norm=w_norm)
            p_tel_lsf[3,*]=p_tel_pwv[3]
        endelse
    endif

    tel_res=(swlnpoints gt 0)? replicate({order:0l,p_tel:p_tel_lsf_ref,ep_tel:ep_tel_lsf_ref,wl_shift:wl_s_str,tellcorr:dblarr(n_wl)+1d,mcont:dblarr(n_wl)+1d,weights:dblarr(n_star),w_norm:1d,chi2dof:chi2dof},n_ord,lsf_nsegments) : $
                          replicate({order:0l,p_tel:p_tel_lsf_ref,ep_tel:ep_tel_lsf_ref,tellcorr:dblarr(n_wl)+1d,mcont:dblarr(n_wl)+1d,weights:dblarr(n_star),w_norm:1d,chi2dof:chi2dof},n_ord,lsf_nsegments)
    tel_res.order=first_ord_n-(lindgen(n_ord) # (lonarr(lsf_nsegments)+1))
    if(n_elements(omin) ne 1) then omin=0
    if(n_elements(omax) ne 1) then omax=n_ord-1
    for i=omin,omax do begin
        print,'Fitting order N=',string(first_ord_n-i,format='(i2)')
        spec=ord_extr_fit[*,i]
        errspec=(n_elements(ord_extr_fit) eq n_elements(err_ord_extr_fit))? err_ord_extr_fit[*,i] : sqrt(spec>0.01)*10d
        if(ord_conv_sigma gt 0) then begin
            spec=fire_convol_order(wl_new[*,i],spec,ord_conv_sigma,/pix)
            errspec=fire_convol_order(wl_new[*,i],errspec,ord_conv_sigma,/pix)
        endif

        vsini_start=(keyword_set(novsini))? vsini0 : p_tel_lsf[1]
        sig_start=(lsf_moments_ord gt 0)? p_tel_lsf[4]*lsf_sig_rel[i]/lsf_sig_rel[ord_lsf_par] : !values.f_nan
        fix_lsf_sig_flag=(n_elements(fix_lsf_sig) eq n_ord)? fix_lsf_sig : bytarr(n_ord)+keyword_set(fix_lsf_sig)
        fix_lsf_sig_value = ((keyword_set(fire_red_lsf) and keyword_set(fix_lsf_sig_flag[i]) and i ge n_ord-3) or ~keyword_set(fix_lsf_sig_flag[i]))? 0 : 1

        for n=0,lsf_nsegments-1 do begin
            fixpar=(keyword_set(fire_lsf_045))? $
                [1,1-keyword_set(fit_vsini),1-keyword_set(fit_airmass),keyword_set(fix_pwv)] : $
                [1,1-keyword_set(fit_vsini),1-keyword_set(fit_airmass),keyword_set(fix_pwv),fix_lsf_sig_value]
            start=(keyword_set(fire_lsf_045))? $
                [p_tel_lsf[0],vsini_start,(airmass+diff_airmass)>1.0,p_tel_lsf[3]] : $
                [p_tel_lsf[0],vsini_start,(airmass+diff_airmass)>1.0,p_tel_lsf[3],(dwlkms*0.11d)> sig_start < (dwlkms*14.99d)]
            if(keyword_set(fire_lsf_045)) then begin
                inp_lsf=dblarr(5,2)
                inp_lsf[*,1]=poly2d(dblarr(5)+i,(dindgen(5)+0.5)/5.0*2048.0,kx_lsf,/irreg,deg1=deg1_lsf,deg2=deg2_lsf)
            endif
            p_tmp=fire_fit_telluric_spectrum(wl_new[idxmin[n]:idxmax[n],i],spec[idxmin[n]:idxmax[n]],error=errspec[idxmin[n]:idxmax[n]],$
                lsf_moments=lsf_moments_ord,mdegree=mdegree,start=start,maskoh=maskoh,maskh2o=maskh2o,exclreg=exclreg,$
                inp_lsf=inp_lsf,/ilsf_kms,$
                fix=fixpar,star_list=star_list,atm_model_grid_file=atm_model_grid_file,$
                maxwl_global=maxwl_global,minwl_global=minwl_global,trans_thr=trans_thr,pixconv=pixconv,atmwl=atmwl,clean=clean,$
                swlnpoints=swlnpoints,swlstart=swlstart,plot=plot,wl_s_s=wl_s_tmp,err_p=ep_tmp,chi2=chi2tmp,tellcorr=tellcorr,$
                mcont=mcont,inp_star_weights=(keyword_set(fix_star_weights) ? weights : (dblarr(n_star+1)+1d)),$
                gausslosvd=gausslosvd,calib_path=calib_path,weights=out_weights,w_norm=w_norm)
            tel_res[i,n].p_tel=p_tmp
            tel_res[i,n].ep_tel=ep_tmp
            if(swlnpoints gt 0 and finite(p_tmp[0]) eq 1) then tel_res[i,n].wl_shift=wl_s_tmp
            tel_res[i,n].chi2dof=chi2tmp
            tel_res[i,n].tellcorr[idxmin[n]:idxmax[n]]=tellcorr/total(out_weights)
            tel_res[i,n].mcont=mcont
            tel_res[i,n].weights=(keyword_set(fix_star_weights) ? weights : out_weights)
            tel_res[i,n].w_norm=w_norm
        endfor
    endfor

    return,tel_res
end
