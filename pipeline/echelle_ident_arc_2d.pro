function echelle_ident_arc_2d,arcline_arr,$
    fwhm=fwhm,nx=nx,nwl_step=nwl_step,wl_step_pix=wl_step_pix,scl_rng=scl_rng,$
    linetab=linetab,weight_tab=weight_tab,el_lab_tab=el_lab_tab,$
    wl_ini=wl_ini,wl_order_shift=wl_order_shift,minflux=minflux,verbose=verbose,$
    fire=fire,ord_red_fire=ord_red_fire,merge_blends_fire=merge_blends_fire,$
    flux_w_thr=flux_w_thr,oh=oh,maxflux=maxflux,saturation=saturation,blue_ord_fwhm=blue_ord_fwhm,singlefwhm=singlefwhm

    if(n_elements(scl_rng) ne 1) then scl_rng=5
    if(n_elements(wl_step_pix) ne 1) then wl_step_pix=0.05 ;;0.01
    if(n_elements(nwl_step) ne 1) then nwl_step=150 ;;100
    if(n_elements(flux_w_thr) ne 1) then flux_w_thr=0.6 ;;;0.8
    if(n_elements(saturation) ne 1) then saturation=80000.0
    if(n_elements(blue_ord_fwhm) ne 1) then blue_ord_fwhm=1.0

    if(n_elements(wl_ini) ne 1) then $
        wl_ini=mrdfits((keyword_set(fire)? getenv('FIRE_PIPELINE_PATH')+'calib_FIRE/wavelength/fire_wl_ini.fits' : getenv('MAGE_PIPELINE_PATH')+'calib_MagE/wavelength/mage_wl_ini.fits'),1,/silent)

    if(n_elements(linetab) eq 0) then $
        linetab=(keyword_set(fire)? getenv('FIRE_PIPELINE_PATH')+'calib_FIRE/linelists/linesThAr.tab' : getenv('MAGE_PIPELINE_PATH')+'calib_MagE/linelists/linesThAr_0.75A.tab')
    if(keyword_set(oh)) then $
        linetab=(keyword_set(fire)? getenv('FIRE_PIPELINE_PATH')+'calib_FIRE/linelists/linesOH_R2k_HITRAN_H2O_nm.tab' : getenv('MAGE_PIPELINE_PATH')+'calib_MagE/linelists/linesOH_R2k_HITRAN_H2O_nm.tab')
    lines_arc=read_arc_list(linetab,weight_tab=weight_tab,el_lab_tab=el_lab_tab,labellist=labellist)

    if(keyword_set(merge_blends_fire)) then begin
        ;;;; merging close blends
        disp_coeff=[-0.0044096077d,6.5796362d-05,-1.8760240d-08,8.5937839d-12,-1.4005138d-15] ;; coefficients of the approximate dispersion (nm/pix) as a function of wavelength
        fwhm_blend=4.0
        lines_arc_orig=lines_arc
        labellist_orig=labellist

        s_l_idx=reverse(sort(lines_arc_orig[2,*]))
        lines_arc_orig=lines_arc_orig[*,s_l_idx]
        labellist_orig=labellist_orig[s_l_idx]
        lines_arc=lines_arc_orig
        for i=0,n_elements(lines_arc_orig[0,*])-1 do begin
            blend_idx = where((abs(lines_arc_orig[0,i]-lines_arc_orig[0,*]) lt fwhm_blend*poly(lines_arc_orig[0,i],disp_coeff)) and $
                              (lines_arc[1,*] ge 0), cblend_idx)
            if(cblend_idx gt 1) then begin            
                lines_arc[0,i]=total(lines_arc[0,blend_idx]*lines_arc[2,blend_idx])/total(lines_arc[2,blend_idx])
                lines_arc[1,i]=max(lines_arc[1,blend_idx])
                lines_arc[2,i]=total(lines_arc[2,blend_idx])
                blend_idx_r=where(blend_idx ne i)
                lines_arc[1,blend_idx_r]=-1 ;;; blended lines will be marked with flag=-1, so the labels will be automatically cleaned later on
            endif
        endfor
    endif

    lines_arc=lines_arc[*,where(lines_arc[1,*] gt 0)] ;;; remove "0" flag
    labellist=labellist[where(lines_arc[1,*] gt 0)]
    if(n_elements(minflux) eq 1) then begin
        lines_arc=lines_arc[*,where(lines_arc[2,*] gt minflux)]
        labellist=labellist[where(lines_arc[2,*] gt minflux)]
    endif
    if(n_elements(maxflux) eq 1) then begin
        lines_arc=lines_arc[*,where(lines_arc[2,*] lt maxflux)]
        labellist=labellist[where(lines_arc[2,*] lt maxflux)]
    endif
    lines=double(transpose(lines_arc[0,*]))
    index_line=transpose(lines_arc[1,*])*0+3
    flux_line=transpose(lines_arc[2,*])
    ;;; sorting by flux
    s_flux=reverse(sort(flux_line))
    lines_sort=lines[s_flux]
    lines_lab_sort=labellist[s_flux]
    index_line_sort=index_line[s_flux]
    flux_line_sort=flux_line[s_flux]
    arcline_medflux=median(arcline_arr.flux,dim=1)


    wl_lines=poly2d(double(arcline_arr.order),double(arcline_arr.xmean),$
        wl_ini.kx,deg1=wl_ini.deg1,deg2=wl_ini.deg2,/irreg)

    wl_id = dblarr(n_elements(arcline_arr))+!values.d_nan
    species_id = strarr(n_elements(arcline_arr))
    orders=arcline_arr[uniq(arcline_arr.order,sort(arcline_arr.order))].order
    n_ord=n_elements(orders)
    if(n_elements(fwhm) ne 1) then fwhm=4.0
    if(n_elements(nx) ne 1) then nx=2048
    wl_order_shift=dblarr(n_ord)+!values.d_nan
    nwl_best_all=lonarr(n_ord)

    for gsi=0,(nwl_step gt 0) do begin ;;;; gsi = global shift iteration
        if(gsi eq 1) then begin
            gs_fit=robust_poly_fit(double(orders),double(nwl_best_all),3)
            nwl_best_all=round(poly(double(orders),gs_fit))
        endif
        for o=0,n_ord-1 do begin
            print,'order:',orders[o]
            arc_ord_cur=where(arcline_arr.order eq orders[o], carc_ord_cur)
            if(carc_ord_cur lt 2) then continue
            lambda_orig=poly2d(dblarr(nx)+double(orders[o]),dindgen(nx),wl_ini.kx,deg1=wl_ini.deg1,deg2=wl_ini.deg2,/irreg)
            lambda=lambda_orig
            d_lambda=abs(lambda[nx/2]-lambda[nx/2-1])
            wlsel=where(lines_sort gt min(lambda,max=maxlam) and lines_sort lt maxlam, cwlsel)
            if(cwlsel eq 0) then continue
            good_pos=arcline_arr[arc_ord_cur].xmean ;;;xpos[25]
            lines_all=lines_sort[wlsel]
            lines_lab_all=lines_lab_sort[wlsel]
            index_line_all=index_line_sort[wlsel]
            flux_line_all=flux_line_sort[wlsel]
            dwl_line_all=flux_line_all*0d
            n_line=n_elements(lines_all)
            
            good_pos_orig=good_pos
            idx_global=arc_ord_cur
            idx_global_orig=idx_global
    
            ;; performing three iterations to identify arc lines
            nwl_best=0
            scl_best=0
            nwl_step_cur=(keyword_set(ord_red_fire) and orders[o] le 12 and keyword_set(oh))? (nwl_step<30) : nwl_step
    
            for iter=gsi,2 do begin
                if(gsi eq 1) then nwl_best=nwl_best_all[o]
                print,'order,ITER:',orders[o],iter+1,nwl_best
                if(iter eq 1) then begin
                    if(gsi eq 0) then nwl_best_all[o]=nwl_best
                    wl_order_shift[o]=double(nwl_best)*wl_step_pix*d_lambda
                endif
                index=3-iter
    
                nwl_min=(iter eq 0)? -nwl_step_cur : 0 ;;; 0*nwl_best
                nwl_max=(iter eq 0)? nwl_step_cur : 0  ;;; 0*nwl_best
    
                scl_min=(iter eq 0)? -scl_rng : 0
                scl_max=(iter eq 0)?  scl_rng : 0
                scl_step_pix=0.1
                n_line_best=-1
                good_line_flux_best=[-1.0]
                good_line_dwl_best=[-1d]
                for scl=scl_min,scl_max do begin
                    for nn=nwl_min,nwl_max do begin
                        indsel=where(index_line_all ge index)
                        lines=lines_all[indsel]
                        lines_lab=lines_lab_all[indsel]
                        lambda=((iter eq 0)? lambda_orig : lambda) + double(nn)*(wl_step_pix+2.0*scl*scl_step_pix*(findgen(nx)/(nx-1)-0.5))*d_lambda
                        index_line=index_line_all[indsel]
                        flux_line=flux_line_all[indsel]
    
                        used_lines=lines*0+1
                        good_line=fltarr(n_elements(good_pos_orig))
                        good_line_lab=strarr(n_elements(good_pos_orig))
                        good_line_flux=fltarr(n_elements(good_pos_orig))
                        good_line_dwl=dblarr(n_elements(good_pos_orig))
                        for k=0,n_elements(good_pos_orig)-1 do begin
                            n_fwhm=1.0*(o eq n_ord ? blue_ord_fwhm : 1.0) ;;; was 4.0
                            if(iter gt 1) then n_fwhm=1.0*(o eq n_ord ? blue_ord_fwhm : 1.0)
                            if(orders[o] le 13 and ~keyword_set(singlefwhm)) then n_fwhm*=3.0
                            r=where((abs(lines-lambda[good_pos_orig[k]]) lt n_fwhm*fwhm*d_lambda) and $
                                     (used_lines eq 1),ind)
                            if (ind eq 1) then begin
                                good_line[k]=lines[r]
                                good_line_lab[k]=lines_lab[r]
                                good_line_flux[k]=flux_line[r]
                                good_line_dwl[k]=lines[r]-lambda[good_pos_orig[k]]
                                used_lines[r]=0
                            endif
                            if (ind gt 1) then begin ;; more than one line found around the position
                                if(keyword_set(verbose)) then print,'>1 lines: lam,linepos=',lambda[good_pos_orig[k]],lines[r]
                                if(iter gt 0) then begin
                                    mm=min(abs((lines-lambda[good_pos_orig[k]])[r]),midx)
                                    good_line[k]=lines[r[midx]]
                                    good_line_lab[k]=lines_lab[r[midx]]
                                    good_line_flux[k]=flux_line[r[midx]]
                                    good_line_dwl[k]=lines[r[midx]]-lambda[good_pos_orig[k]]
                                    used_lines[r[midx]]=0
                                endif else begin
                                    r_add=where(flux_line[r]/flux_line[r[0]] gt flux_w_thr, cra)
                                    if(cra eq 1) then begin
                                        good_line[k]=lines[r[r_add]]
                                        good_line_lab[k]=lines_lab[r[r_add]]
                                        good_line_flux[k]=flux_line[r[r_add]]
                                        good_line_dwl[k]=lines[r[r_add]]-lambda[good_pos_orig[k]]
                                        used_lines[r[r_add]]=0
                                        if(keyword_set(verbose)) then print,'>1 lines: chosenA: ',lines[r[r_add]]
                                    endif else begin 
                                        mm=min(abs((lines-lambda[good_pos_orig[k]])[r[r_add]]),midx)
                                        good_line[k]=lines[r[r_add[midx]]]
                                        good_line_lab[k]=lines_lab[r[r_add[midx]]]
                                        good_line_flux[k]=flux_line[r[r_add[midx]]]
                                        good_line_dwl[k]=lines[r[r_add[midx]]]-lambda[good_pos_orig[k]]
                                        used_lines[r[r_add[midx]]]=0
                                        if(keyword_set(verbose)) then print,'>1 lines: chosenB: ',lines[r[r_add[midx]]]
                                    endelse
                                endelse
                            endif
                        endfor
                        r=where(good_line gt 0.001,n_line)
    ;print,'scl,nn,n_line=',scl,nn,n_line
                        if($
;                           (n_line ge n_line_best) $
;                           and (total(good_line_flux) gt total(good_line_flux_best)*flux_w_thr) $
                           (total(n_line*good_line_flux) gt total(n_line_best*good_line_flux_best)*sqrt(flux_w_thr)) $
;                           and ((total(good_line_dwl^2) lt total(good_line_dwl_best^2))) $
                           and ((total(good_line_dwl^2)/n_line lt total(good_line_dwl_best^2)/n_line_best)) $
                           ) or (scl eq scl_min and nn eq nwl_min) then begin
                            n_line_best=n_line
                            nwl_best=nn
                            good_line_best=good_line
                            good_line_best_lab=good_line_lab
                            good_line_flux_best=good_line_flux
                            good_line_dwl_best=good_line_dwl
                            used_lines_best=used_lines
                            lambda_best=lambda
                        endif
                    endfor
                endfor
                lambda=lambda_best
                r=where(good_line_best gt 0.001,n_line)
                good_line=good_line_best[r]
                good_line_lab=good_line_best_lab[r]
                good_pos=good_pos_orig[r]
                idx_global=idx_global_orig[r]
                good_line_dwl=good_line_dwl_best[r]
                print,n_elements(good_line),' lines identified after iteration ',iter
                if(n_elements(good_line) lt 2) then begin
                    message,/inf,'Warning: too few lines could be identified'
                    continue
                endif
                shift_lambda = (n_elements(good_line) le 3)? $
                    total(good_line-poly2d(good_pos*0d +double(orders[o]),double(good_pos),wl_ini.kx,/irreg))/n_elements(good_line) : $
                    poly(dindgen(Nx),robust_poly_fit(good_pos,good_line-poly2d(good_pos*0d +double(orders[o]),double(good_pos),wl_ini.kx,/irreg),1))
                lambda = lambda - shift_lambda*(iter eq 0)
            endfor
    
            wl_id[idx_global]=good_line
            species_id[idx_global]=good_line_lab
        endfor
    endfor

    if(saturation gt 0) then begin
        saturated_idx=where(arcline_medflux gt saturation and arcline_arr.order lt 10,csaturated_idx)
        if(csaturated_idx gt 0) then begin
            print,'Number of saturated lines in the 3 red orders: ',csaturated_idx
            wl_id[saturated_idx]=!values.f_nan
        endif
    endif
    arcline_arr.wavelength=wl_id
    arcline_arr.species=species_id

    return,wl_id
end
