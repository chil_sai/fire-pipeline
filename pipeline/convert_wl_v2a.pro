; This function converts vacuum<=>atmospheric wavelength following the IAU
; convention. It can accept input in A (default) or nm (if /nm is set)
; By default it converts vaccum (input) into atmospheric (output)
; The inverse transformation is done when /inverse is set

function convert_wl_v2a,wlvac,inverse=inverse,nm=nm
    k=(keyword_set(nm))? $
        (1.0d + 2.735182d-4 + 1.314182d/double(wlvac)^2 + 2.76249d+4/double(wlvac)^4) : $
        (1.0d + 2.735182d-4 + 131.4182d/double(wlvac)^2 + 2.76249d+8/double(wlvac)^4)
    return, keyword_set(inverse) ? wlvac*k : wlvac/k
end
