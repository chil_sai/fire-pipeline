function fire_ident_arc_2d,arcline_arr,$
    fwhm_inp=fwhm_inp,nwl_step=nwl_step,wl_step_pix=wl_step_pix,linetab=linetab,$
    wl_ini=wl_ini,wl_order_shift=wl_order_shift,minflux=minflux,verbose=verbose,$
    flux_w_thr=flux_w_thr,oh=oh,ord_red=ord_red

    if(n_elements(wl_step_pix) ne 1) then wl_step_pix=0.1 ;;0.01
    if(n_elements(nwl_step) ne 1) then nwl_step=150 ;;100
    if(n_elements(flux_w_thr) ne 1) then flux_w_thr=0.8

    if(n_elements(wl_ini) ne 1) then wl_ini=mrdfits(getenv('FIRE_PIPELINE_PATH')+'calib_FIRE/wavelength/fire_wl_ini.fits',1,/silent)
    wl_lines=poly2d(double(arcline_arr.order),double(arcline_arr.xmean),$
        wl_ini.kx,deg1=wl_ini.deg1,deg2=wl_ini.deg2,/irreg)

    ord_red_wlini=[2521.1402d,-0.10574658d,-2.0645587d-06,-5.0875908d-09,2.2899468d-12,-3.2130085d-16]
    if(n_elements(linetab) ne 1) then linetab=getenv('FIRE_PIPELINE_PATH')+'calib_FIRE/linelists/linesThAr.tab'
    if(keyword_set(oh)) then linetab=getenv('FIRE_PIPELINE_PATH')+'calib_FIRE/linelists/linesOH_R2k_HITRAN_H2O_nm.tab'
    lines_arc_orig=read_asc(linetab)

    ;;;; merging close blends
    disp_coeff=[-0.0044096077d,6.5796362d-05,-1.8760240d-08,8.5937839d-12,-1.4005138d-15] ;; coefficients of the approximate dispersion (nm/pix) as a function of wavelength
    fwhm_blend=4.0 ;; 3.0
    lines_arc_orig=lines_arc_orig[*,reverse(sort(lines_arc_orig[2,*]))]
    lines_arc=lines_arc_orig
    for i=0,n_elements(lines_arc_orig[0,*])-1 do begin
        blend_idx = where((abs(lines_arc_orig[0,i]-lines_arc_orig[0,*]) lt fwhm_blend*poly(lines_arc_orig[0,i],disp_coeff)) and $
                          (lines_arc[1,*] ge 0), cblend_idx)
;        if( (abs(lines_arc_orig[0,i+1]-lines_arc_orig[0,i]) lt fwhm_blend*poly(lines_arc_orig[0,i+1],disp_coeff)) and $
;            (abs(alog10(lines_arc_orig[2,i+1]/lines_arc_orig[2,i])) lt 1.0)) then begin
        if(cblend_idx gt 1) then begin            
            lines_arc[0,i]=total(lines_arc[0,blend_idx]*lines_arc[2,blend_idx])/total(lines_arc[2,blend_idx])
            lines_arc[1,i]=max(lines_arc[1,blend_idx])
            lines_arc[2,i]=total(lines_arc[2,blend_idx])
            blend_idx_r=where(blend_idx ne i)
            lines_arc[1,blend_idx_r]=-1
;            lines_arc[0,i]=(lines_arc_orig[0,i+1]*lines_arc_orig[2,i+1]+lines_arc_orig[0,i]*lines_arc_orig[2,i])/(lines_arc_orig[2,i+1]+lines_arc_orig[2,i])
;            lines_arc[1,i]=(lines_arc_orig[1,i+1]>lines_arc_orig[1,i])
;            lines_arc[2,i]=lines_arc_orig[2,i+1]+lines_arc_orig[2,i]
;            lines_arc[*,i+1]=-1
        endif
    endfor

    lines_arc=lines_arc[*,where(lines_arc[1,*] gt 0)] ;;; remove "0" flag
    if(n_elements(minflux) eq 1) then lines_arc=lines_arc[*,where(lines_arc[2,*] gt minflux)]
    lines=double(transpose(lines_arc[0,*]))
    index_line=transpose(lines_arc[1,*])*0+3
    flux_line=transpose(lines_arc[2,*])
    ;;; sorting by flux
    s_flux=reverse(sort(flux_line))
    lines_sort=lines[s_flux]
    index_line_sort=index_line[s_flux]
    flux_line_sort=flux_line[s_flux]

    wl_id = dblarr(n_elements(arcline_arr))+!values.d_nan
    orders=arcline_arr[uniq(arcline_arr.order,sort(arcline_arr.order))].order
    n_ord=n_elements(orders)
    if(n_elements(fwhm_inp) ne 1) then fwhm_inp=4.0
    nx=2048
    wl_order_shift=dblarr(n_ord)+!values.d_nan

    for o=0,n_ord-1 do begin
        print,'order:',orders[o]
        arc_ord_cur=where(arcline_arr.order eq orders[o], carc_ord_cur)
        if(carc_ord_cur lt 2) then continue
        lambda_orig=poly2d(dblarr(nx)+double(orders[o]),dindgen(nx),wl_ini.kx,deg1=wl_ini.deg1,deg2=wl_ini.deg2,/irreg)
        ;; if(keyword_set(ord_red) and orders[o] eq 12) then lambda_orig=poly(dindgen(nx),ord_red_wlini)
        lambda=lambda_orig
        d_lambda=abs(lambda[nx/2]-lambda[nx/2-1])
        wlsel=where(lines_sort gt min(lambda,max=maxlam) and lines_sort lt maxlam, cwlsel)
        if(cwlsel eq 0) then continue
        good_pos=arcline_arr[arc_ord_cur].xmean ;;;xpos[25]
        lines_all=lines_sort[wlsel]
        index_line_all=index_line_sort[wlsel]
        flux_line_all=flux_line_sort[wlsel]
        dwl_line_all=flux_line_all*0d
        n_line=n_elements(lines_all)
        
        good_pos_orig=good_pos
        idx_global=arc_ord_cur
        idx_global_orig=idx_global

        ;; performing three iterations to identify arc lines
        nwl_best=0
        nwl_step_cur=(keyword_set(ord_red) and orders[o] le 12 and keyword_set(oh))? (nwl_step<30) : nwl_step

        for iter=0,2 do begin
            print,'order,ITER:',orders[o],iter+1,nwl_best
            if(iter eq 1) then wl_order_shift[o]=double(nwl_best)*wl_step_pix*d_lambda
            index=3-iter

            nwl_min=(iter eq 0)? -nwl_step_cur : 0 ;;; 0*nwl_best
            nwl_max=(iter eq 0)? nwl_step_cur : 0  ;;; 0*nwl_best
            n_line_best=-1
            good_line_flux_best=[-1.0]
            good_line_dwl_best=[-1d]
            for nn=nwl_min,nwl_max do begin
                fwhm=fwhm_inp
                indsel=where(index_line_all ge index)
                lines=lines_all[indsel]
                lambda=((iter eq 0)? lambda_orig : lambda) + double(nn)*wl_step_pix*d_lambda
                index_line=index_line_all[indsel]
                flux_line=flux_line_all[indsel]

                used_lines=lines*0+1
                good_line=fltarr(N_elements(good_pos_orig))
                good_line_flux=fltarr(N_elements(good_pos_orig))
                good_line_dwl=dblarr(n_elements(good_pos_orig))
                for k=0,N_elements(good_pos_orig)-1 do begin
                    n_fwhm=1.0 ;;; was 4.0
                    if(iter gt 1) then n_fwhm=1.0
                    if((orders[o] le 13) or (orders[o] le 13 and ~keyword_set(oh))) then n_fwhm*=3.0
                    if(lambda[good_pos_orig[k]] gt 2270.0 and orders[o] eq 12 and keyword_set(oh)) then begin ;; faint OH in the 12th order
                        n_fwhm*=2.0 ;;;(5.0 * (3.0/(fwhm < 3.0))) 
                    endif
                    if(lambda[good_pos_orig[k]] gt 2490.0 and ~keyword_set(oh)) then begin ;; faint ThAr in the 11th order
                        fwhm=(fwhm > 3.0)
                        n_fwhm*=5.0 ; 3.0
                    endif
                    r=where((abs(lines-lambda[good_pos_orig[k]]) lt n_fwhm*fwhm*d_lambda) and $
                             (used_lines eq 1),ind)
                    if (ind eq 1) then begin
                        good_line[k]=lines[r]
                        good_line_flux[k]=flux_line[r]
                        good_line_dwl[k]=lines[r]-lambda[good_pos_orig[k]]
                        used_lines[r]=0
                    endif
                    if (ind gt 1) then begin ;; more than one line found around the position
                        if(keyword_set(verbose)) then print,'>1 lines: lam,linepos=',lambda[good_pos_orig[k]],lines[r]
                        if(iter gt 0) then begin
                            mm=min(abs((lines-lambda[good_pos_orig[k]])[r]),midx)
                            good_line[k]=lines[r[midx]]
                            good_line_flux[k]=flux_line[r[midx]]
                            good_line_dwl[k]=lines[r[midx]]-lambda[good_pos_orig[k]]
                            used_lines[r[midx]]=0
                        endif else begin
                            r_add=where(flux_line[r]/flux_line[r[0]] gt flux_w_thr, cra)
                            if(cra eq 1) then begin
                                good_line[k]=lines[r[r_add]]
                                good_line_flux[k]=flux_line[r[r_add]]
                                good_line_dwl[k]=lines[r[r_add]]-lambda[good_pos_orig[k]]
                                used_lines[r[r_add]]=0
                                if(keyword_set(verbose)) then print,'>1 lines: chosenA: ',lines[r[r_add]]
                            endif else begin 
                                mm=min(abs((lines-lambda[good_pos_orig[k]])[r[r_add]]),midx)
                                good_line[k]=lines[r[r_add[midx]]]
                                good_line_flux[k]=flux_line[r[r_add[midx]]]
                                good_line_dwl[k]=lines[r[r_add[midx]]]-lambda[good_pos_orig[k]]
                                used_lines[r[r_add[midx]]]=0
                                if(keyword_set(verbose)) then print,'>1 lines: chosenB: ',lines[r[r_add[midx]]]
                            endelse
                        endelse
                    endif
                endfor
                r=where(good_line gt 0.001,n_line)
                if($
;                   ((n_line gt n_line_best) and $
;                    (total(good_line_flux) gt total(good_line_flux_best)*flux_w_thr)) or $
;                   ((n_line eq n_line_best) and $
;                    (total(good_line_flux) eq total(good_line_flux_best)) and $
;                    (total(good_line_dwl^2) lt total(good_line_dwl_best^2)))) then begin
                    (total(n_line*good_line_flux) ge total(n_line_best*good_line_flux_best)*sqrt(flux_w_thr)) $
                    and ((total(good_line_dwl^2)/n_line lt total(good_line_dwl_best^2)/n_line_best)) $
                    ) or (n_line_best eq -1) then begin
                    n_line_best=n_line
                    nwl_best=nn
                    good_line_best=good_line
                    good_line_flux_best=good_line_flux
                    good_line_dwl_best=good_line_dwl
                    used_lines_best=used_lines
                    lambda_best=lambda
                endif
            endfor
            lambda=lambda_best
            r=where(good_line_best gt 0.001,n_line)
            good_line=good_line_best[r]
            good_pos=good_pos_orig[r]
            idx_global=idx_global_orig[r]
            good_line_dwl=good_line_dwl_best[r]
            print,N_elements(good_line),' lines identified after iteration ',iter
            if(n_elements(good_line) lt 2) then begin
                message,/inf,'Warning: too few lines could be identified'
                continue
            endif
            shift_lambda = (n_elements(good_line) le 3)? $
                total(good_line-poly2d(good_pos*0d +double(orders[o]),double(good_pos),wl_ini.kx,/irreg))/n_elements(good_line) : $
                poly(dindgen(Nx),robust_poly_fit(good_pos,good_line-poly2d(good_pos*0d +double(orders[o]),double(good_pos),wl_ini.kx,/irreg),1))
            lambda = lambda - shift_lambda*(iter eq 0)
        endfor

        wl_id[idx_global]=good_line
    endfor
    arcline_arr.wavelength=wl_id
    return,wl_id
end
