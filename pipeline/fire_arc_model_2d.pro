function fire_arc_model_2d,wl_all,fwhm,linetab=linetab,weight_tab=weight_tab,el_lab_tab=el_lab_tab,labellist=labellist,pixel_fwhm=pixel_fwhm

    if(n_elements(linetab) lt 1) then linetab=getenv('FIRE_PIPELINE_PATH')+'calib_FIRE/linlists/linesThAr.tab'
    s_wl=size(wl_all)
    n_ord=(s_wl[0] eq 2)? s_wl[2] : 1
    fwhm_arr=(n_elements(fwhm) eq n_ord)? fwhm : dblarr(n_ord)+fwhm

    ;table=read_asc(linetab)
    table=read_arc_list(linetab,weight_tab=weight_tab,el_lab_tab=el_lab_tab,labellist=labellist)
    wl_line=float(transpose(table[0,*]))
    flux_line=float(transpose(table[2,*]))

    arc_model_spectrum_2d=double(wl_all)*0d

    x=dindgen(s_wl[1])
    for i=0,n_ord-1 do begin
        lineidx=where(wl_line gt min(wl_all[*,i],max=maxwl) and wl_line lt maxwl, clineidx)
        if(clineidx eq 0) then continue
        dwl=wl_all[1:*,i]-wl_all[0:s_wl[1]-2,i]
        dwl=[dwl[0],dwl]
        x_lin=interpol(x,wl_all[*,i],wl_line[lineidx])
        fwhm_lines=abs(fwhm_arr[i]/dwl[fix(x_lin)])
        for l=0,clineidx-1 do begin
            sigma=(keyword_set(pixel_fwhm))? fwhm_arr[i]/2.355d : fwhm_lines[l]/2.355d
            xmin=(floor(x_lin[l]-sigma*7)) > 0
            xmax=(ceil(x_lin[l]+sigma*7)) < (s_wl[1]-1)
            if(xmax ge xmin) then $
                arc_model_spectrum_2d[xmin:xmax,i]=arc_model_spectrum_2d[xmin:xmax,i]+flux_line[lineidx[l]]*exp(-((x[xmin:xmax]-x_lin[l])/sigma)^2/2d)
;;;            arc_model_spectrum_2d[*,i]=arc_model_spectrum_2d[*,i]+flux_line[lineidx[l]]*exp(-((x-x_lin[l])/sigma)^2/2d)
        endfor
    endfor

    return,arc_model_spectrum_2d
end
