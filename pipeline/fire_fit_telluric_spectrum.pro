function convol_atm_transmission_lsf,atm_model_grid,inp_lsf,nsegments=nsegments,kms=kms
    common atm_model_grid_common, atm_model_grid_conv, clean_am_cache

    wl=atm_model_grid[0].wave
    n_wl=n_elements(wl)

    s_inp_lsf= (n_elements(inp_lsf) eq 1)? size([inp_lsf]) : size(inp_lsf)
    inp_lsf_vel=dblarr(n_wl)
    inp_lsf_sig=dblarr(n_wl)+0.001
    inp_lsf_h3=dblarr(n_wl)
    inp_lsf_h4=dblarr(n_wl)
    inp_lsf_h5=dblarr(n_wl)
    inp_lsf_h6=dblarr(n_wl)
    if(s_inp_lsf[0] eq 1) then inp_lsf_sig=(s_inp_lsf[1] eq 1)? dblarr(n_wl)+inp_lsf[0] : interpol(inp_lsf,dindgen(s_inp_lsf[1])/(s_inp_lsf[1]-1d),dindgen(n_wl)/(n_wl-1d))
    if(s_inp_lsf[0] eq 2) then begin
        inp_lsf_vel=(s_inp_lsf[1] eq 1)? dblarr(n_wl)+inp_lsf[0,0] : interpol(inp_lsf[*,0],dindgen(s_inp_lsf[1])/(s_inp_lsf[1]-1d),dindgen(n_wl)/(n_wl-1d))
        inp_lsf_sig=(s_inp_lsf[1] eq 1)? dblarr(n_wl)+inp_lsf[0,1] : interpol(inp_lsf[*,1],dindgen(s_inp_lsf[1])/(s_inp_lsf[1]-1d),dindgen(n_wl)/(n_wl-1d))
        if(s_inp_lsf[2] gt 2) then begin
            inp_lsf_h3=(s_inp_lsf[1] eq 1)? dblarr(n_wl)+inp_lsf[0,2] : interpol(inp_lsf[*,2],dindgen(s_inp_lsf[1])/(s_inp_lsf[1]-1d),dindgen(n_wl)/(n_wl-1d))
            inp_lsf_h4=(s_inp_lsf[1] eq 1)? dblarr(n_wl)+inp_lsf[0,3] : interpol(inp_lsf[*,3],dindgen(s_inp_lsf[1])/(s_inp_lsf[1]-1d),dindgen(n_wl)/(n_wl-1d))
        endif
        if(s_inp_lsf[2] gt 4) then begin
            inp_lsf_h5=(s_inp_lsf[1] eq 1)? dblarr(n_wl)+inp_lsf[0,4] : interpol(inp_lsf[*,4],dindgen(s_inp_lsf[1])/(s_inp_lsf[1]-1d),dindgen(n_wl)/(n_wl-1d))
            inp_lsf_h6=(s_inp_lsf[1] eq 1)? dblarr(n_wl)+inp_lsf[0,5] : interpol(inp_lsf[*,5],dindgen(s_inp_lsf[1])/(s_inp_lsf[1]-1d),dindgen(n_wl)/(n_wl-1d))
        endif
    endif

    trans_conv=conv_multi(atm_model_grid.transmission,$
        velScale=(keyword_set(kms)? 299792.458d*2d*(wl[1]-wl[0])/(wl[1]+wl[0]) : wl[1]-wl[0]),$
        velvec=inp_lsf_vel,disvec=inp_lsf_sig,$
        h3vec=inp_lsf_h3,h4vec=inp_lsf_h4,$
        h5vec=inp_lsf_h5,h6vec=inp_lsf_h6,nsegments=nsegments)

    atm_model_grid_conv=atm_model_grid
    atm_model_grid_conv.transmission=trans_conv
    clean_am_cache=1
    return,atm_model_grid_conv
end


function bin_hr_spec,wl_hr,spec_hr,wl_lr
    n_wl=n_elements(wl_lr)
    s_s=size(spec_hr)
    spec_lr=(s_s[0] eq 1)? dblarr(n_wl) : (s_s[0] eq 2)? dblarr(n_wl,s_s[2]) : dblarr(n_wl,s_s[2],s_s[3])
    wl_lr_cur=dblarr(n_wl)+!values.d_nan    
    spec_hr_cnt=histogram(wl_hr,min=wl_lr[0]-(wl_lr[1]-wl_lr[0])/2d,$
                                max=wl_lr[n_wl-1]+(wl_lr[n_wl-1]-wl_lr[n_wl-2])/2d,$
                                binsize=(wl_lr[1]-wl_lr[0]),reverse_ind=ind_spec_hr)
    n_bins=(n_elements(spec_hr_cnt) < n_wl)

    for i=1L,n_bins-1L do $
        if((ind_spec_hr[i]-ind_spec_hr[i-1]) gt 0) then begin
            case s_s[0] of
                1 : spec_lr[i-1]=total(spec_hr[ind_spec_hr[ind_spec_hr[i-1]:ind_spec_hr[i]-1L]])/double(ind_spec_hr[i]-ind_spec_hr[i-1])
                2 : spec_lr[i-1,*]=total(spec_hr[ind_spec_hr[ind_spec_hr[i-1]:ind_spec_hr[i]-1L],*],1)/double(ind_spec_hr[i]-ind_spec_hr[i-1])
                3 : spec_lr[i-1,*,*]=total(spec_hr[ind_spec_hr[ind_spec_hr[i-1]:ind_spec_hr[i]-1L],*,*],1)/double(ind_spec_hr[i]-ind_spec_hr[i-1])
            endcase
            
            wl_lr_cur[i-1]=total(wl_hr[ind_spec_hr[ind_spec_hr[i-1]:ind_spec_hr[i]-1L]])/double(ind_spec_hr[i]-ind_spec_hr[i-1])
        endif
    g_wl=where(finite(wl_lr_cur) eq 1, cg_wl)
    if(cg_wl eq n_elements(wl_lr_cur)) then return, spec_lr

    case s_s[0] of
        1 : spec_lr=interpol(spec_lr[g_wl],wl_lr_cur[g_wl],wl_lr)
        2 : for x=0,s_s[2]-1 do $
                spec_lr[*,x]=interpol(spec_lr[g_wl,x],wl_lr_cur[g_wl],wl_lr)
        3 : for x=0,s_s[2]-1 do $
                for y=0,s_s[3]-1 do $
                    spec_lr[*,x,y]=interpol(spec_lr[g_wl,x,y],wl_lr_cur[g_wl],wl_lr)
    endcase

    return, spec_lr
end

function interp_atm_model,airmass,pwv,grid_airmass=grid_airmass,grid_pwv=grid_pwv
    common atm_model_grid_common, atm_model_tab, clean_am_cache
    common atm_model_pwv_cache, atm_model_pwv, airmass_wv_val
    common atm_model_cache, cached_atm_model

    atm_model_int={wave:atm_model_tab.wave, pwv:0.0d, airmass:0.0d, transmission:atm_model_tab.transmission[*,0,0]}
    pwv_val = min(atm_model_tab.pwv) > pwv < max(atm_model_tab.pwv)
    atm_model_int.pwv=pwv_val
    airmass_val = min(atm_model_tab.airmass) > airmass < max(atm_model_tab.airmass)
    atm_model_int.airmass=airmass_val

    if(n_elements(cached_atm_model) eq 1 and n_elements(clean_am_cache) eq 1) then begin
        if(clean_am_cache eq 0) then $
            if(cached_atm_model.pwv eq pwv_val and $
                cached_atm_model.airmass eq airmass_val) then begin
;;            print,'CACHE MOD!!!!!'
                return,cached_atm_model
        endif
    endif

    grid_pwv=atm_model_tab.pwv
    grid_airmass=atm_model_tab.airmass

    n_wv=n_elements(grid_pwv)
    n_a=n_elements(grid_airmass)
    n_wl_m=n_elements(atm_model_tab.wave)

    mod_wv_cache=(n_elements(atm_model_pwv) eq n_wv*n_wl_m and n_elements(clean_am_cache) eq 1 and n_elements(airmass_wv_val) eq 1)
    if(mod_wv_cache) then $
        mod_wv_cache=((clean_am_cache eq 0) and (airmass_wv_val eq airmass_val)) 

    if(~mod_wv_cache) then begin
        clean_am_cache=0
        atm_model_pwv=atm_model_tab.transmission[*,*,0]
        for j=0l,n_wv-1l do begin
            for i=0l,n_wl_m-1l do begin
                atm_model_pwv[i,j]=$
                    (interpol((atm_model_tab.transmission[i,j,*]),$
                        atm_model_tab.airmass,[airmass_val]))
;                    exp(interpol(alog(atm_model_tab[j+n_wv*lindgen(n_a)].transmission[i]),$
;                        atm_model_tab[j+n_wv*lindgen(n_a)].airmass,[airmass_val],/quadratic))
            endfor
        endfor
    endif ;; else print,'CACHE!!!!!!!!!'

    airmass_wv_val=airmass_val

    for i=0l,n_wl_m-1l do begin
        atm_model_int.transmission[i]=$
            (interpol((atm_model_pwv[i,*]),$
            atm_model_tab.pwv,[pwv_val]))
;            exp(interpol(alog(atm_model_pwv.transmission[i]),$
;            atm_model_pwv.pwv,[pwv_val],/spl))
    endfor
    cached_atm_model=atm_model_int
    return,atm_model_int
end

function fire_read_atmosphere_models,wl,atm_model_grid_file=atm_model_grid_file,atmwl=atmwl
    common atm_model_grid_common, atm_model_tab, clean_am_cache

    clean_am_cache=1
    if(n_elements(atm_model_grid_file) ne 1) then $
        atm_model_grid_file=getenv('FIRE_PIPELINE_PATH')+'calib_FIRE/sky_transmission/la_silla_sky_model_all.fits.gz'
    print,'Reading the atmosphere model from '+atm_model_grid_file
    atm_model_raw=mrdfits(atm_model_grid_file,1,/silent)
    if(n_params() eq 0) then wl=atm_model_raw.wave
    atm_fire = (n_params() eq 0)? atm_model_raw.transmission : bin_hr_spec(atm_model_raw.wave,atm_model_raw.transmission,wl)
    if(keyword_set(atmwl)) then begin
        wlatm=convert_wl_v2a(wl,/nm)
        for x=0,n_elements(atm_model_raw.pwv)-1 do begin
            for y=0,n_elements(atm_model_raw.airmass)-1 do begin
                atm_fire[*,x,y]=interpol(atm_fire[*,x,y],wlatm,wl)
            endfor
        endfor
    endif

    wv = atm_model_raw.pwv
    am = atm_model_raw.airmass

    atm_model_tab={pwv:wv,airmass:am,wave:wl,transmission:atm_fire}
    print,'done'

    return, atm_model_tab
end

function fitfunc_telluric_spectrum,p,$
    lsf_moments=lsf_moments,pixmask=pixmask,$
    wl=wl_inp,spectrum=spectrum,error=error,$
    star=star,gausslosvd=gausslosvd,$
;;;    atm_model_grid=atm_model_grid,$
    mdegree=mdegree,mcont=mcont,$
    swlnpoints=swlnpoints,swlmax=swlmax,swlvec=swlvec,$
    tellcorr=tellcorr,model=model,s_wl_struct=s_wl_struct,weights=weights,w_norm=w_norm,$
    pixconv=pixconv,clean=clean,outpixmask=outpixmask

    common mpoly_arr_cache, mpoly_arr
    common wlshift_par_cache, wlshift_par_ini, wlshift_swlnpoints, wlshift_mpfit_status

    ;; p[0]: star vr, km/s, p[1]: star vsini, km/s
    ;; p[2]: airmass, p[3]: pwv, mm
    ;; p[4]: lsf_sigma, p[5-6]: h3, h4 (if lsf_moments=4)

    wl=wl_inp
    n_wl=n_elements(wl)
    if(n_elements(lsf_moments) ne 1) then lsf_moments=2 
    if(n_elements(pixmask) ne n_wl) then pixmask=intarr(n_wl)+1
    if(n_elements(swlnpoints) ne 1) then swlnpoints=0
    if(n_elements(swlmax) ne 1) then swlmax=1d
    npar=n_elements(p)-mdegree

    velScale=(alog(wl[n_wl-1])-alog(wl[0]))*299792.458d/double(n_wl)
    wllog=wl[0]*exp(velScale*dindgen(n_wl)/299792.458d)
;;    atm_model_int=interp_atm_model(atm_model_grid,p[2],p[3])
    atm_model_int=interp_atm_model(p[2],p[3])
    goodPixels=where(pixmask gt 0, cgoodPixels, compl=badPixels, ncompl=cbadPixels)

    atm_model_int_log=interpol(atm_model_int.transmission, wl, wllog) ;, /spl)

    if(~keyword_set(pixconv)) then begin
        n_tf=2l^(ceil(alog(n_wl)/alog(2d)))
    endif

    if(lsf_moments ge 2) then begin
        lsf_pars=[0d,p[4]]/velScale
        if(lsf_moments gt 2) then lsf_pars=[lsf_pars,p[5],p[6]]
        if(lsf_moments gt 4) then lsf_pars=[lsf_pars,p[7],p[8]]
        if(keyword_set(pixconv)) then begin
            dxmax=ceil(5d*p[4]/velScale)
            lsf_krnl=gauss_hermite(lsf_pars,dxmax)
            atm_model_int_log=convol(atm_model_int_log,lsf_krnl,/edge_trunc)
        endif else begin
            krnl_lsf = gauss_hermite(lsf_pars,n_tf/2l,/fourier)
            atm_model_int_log=convol_1d_fourier(atm_model_int_log,krnl_lsf)
        endelse
    endif else if(~keyword_set(pixconv)) then krnl_lsf=dcomplex((dblarr(n_tf)+1d),dblarr(n_tf))

    v0=-p[0]
    s_star = size(star)
    n_star = (s_star[0] eq 2)? s_star[2] : (s_star[0] eq 1)? 1 : 0

    star_log = dblarr(n_wl, n_star)
    star_conv = star_log

    if(keyword_set(pixconv)) then begin
        star_krnl=(keyword_set(gausslosvd))? $
            psf_gaussian(ndim=1,npix=fix((abs(v0)+p[1]*4.0)/velScale)+5,centroid=(fix((v0+p[1]*4.0)/velScale)+5)/2.0+v0/velScale,fwhm=2.355*p[1]/velScale,/double,/norm) : $
            lsf_rotate_v0(velScale, p[1], v0=v0)
    endif else begin
        krnl_rot=(keyword_set(gausslosvd))? $
            gauss_hermite([p[0]/velScale,p[1]/velScale],n_tf/2l,/fourier) : $
            lsf_rotate_v0_fourier(velScale, p[1], n_tf, v0=p[0])
    endelse
    
    for i=0,n_star-1 do begin
        star_log[*,i] = interpol(star[*,i], wl, wllog)
        if(keyword_set(pixconv)) then begin
            star_conv[*,i] = convol(star_log[*,i], star_krnl, /edge_truncate)
            if(lsf_moments ge 2) then $
                star_conv[*,i] = convol(star_conv[*,i], lsf_krnl, /edge_truncate)
        endif else begin
            star_conv[*,i]=convol_1d_fourier(star_log[*,i],krnl_rot*krnl_lsf)
        endelse
    endfor

    xpoly=(-1d + 2d*dindgen(n_wl)/(n_wl-1))
    mpoly=dblarr(n_wl)+1d

    if(mdegree ge 1) then begin
        s_mp=size(mpoly_arr)
        if(s_mp[0] ne 2 or s_mp[1] ne n_wl or s_mp[2] ne mdegree+1) then begin
            mpoly_arr = dblarr(n_wl, mdegree+1)
            for i=0, mdegree do begin
;                mpoly_arr[*,i] = legendre(xpoly, i, /double)
                mpoly_arr[*,i] = legendre(sin(xpoly*!dpi/2d), i, /double)
            endfor
        endif
    endif

    for j=1,mdegree do mpoly+=mpoly_arr[*,j]*p[npar+j-1]
;    for j=1,mdegree do mpoly+=mpoly_arr[*,j]*p[(lsf_moments>1)+2+swlnpoints+j-1]
    npoly = 0 ;;; no additive terms

    c = dblarr(n_wl,n_star)
    a = c

    if(n_elements(swlmax) ne 1) then swlmax=(wllog[1]-wllog[0])*10.0 ;;; max 10 pixels
    m = 0
    
    repeat begin
        swlvec=wl*0d
        if(swlnpoints gt 0 and swlmax gt 0) then begin
            if(n_elements(wlshift_par_ini) ne swlnpoints) then wlshift_par_ini=dblarr(swlnpoints)

            spline=(swlnpoints gt 3)? 1 : 0
            quadratic=(swlnpoints gt 2 and swlnpoints le 3)? 1 : 0

            p_wl=interpol(wl[goodPixels[0]:goodPixels[cgoodPixels-1]],$
                dindgen(goodPixels[cgoodPixels-1]-goodPixels[0]+1),(dindgen(swlnpoints)+0.5d)*double(cgoodPixels)/double(swlnpoints),spline=spline,quadratic=quadratic)
            d_wl=(swlnpoints eq 1)? wl*0.0+p[npar-swlnpoints] : interpol(p[npar-swlnpoints:npar-1],p_wl,wl,spline=spline,quadratic=quadratic)

            wlshift_par=p[npar-swlnpoints:npar-1]
            e_wlshift_par=p[npar-swlnpoints:npar-1]*0d

            if (total(wlshift_par^2d0) ne 0) then wlshift_par_ini = wlshift_par else wlshift_par = wlshift_par_ini

            s_wl_struct = {wl_coord:p_wl, wl_step:wl[1]-wl[0], wl_shift:wlshift_par, ewl_shift:e_wlshift_par, $
                             chi2dof:1d}
            ;; print, 'wlshift_par:   ', wlshift_par
            swlvec = swlvec + d_wl
        endif

        for i=0, n_star-1 do begin
            c[*,i] = interpol(star_conv[*,i] * atm_model_int_log, wllog, wl+swlvec) * mpoly ;, /spl)
            a[*,i] = c[*,i] / error
        endfor
        weights = bvls_solve_pxf_qp(a[goodPixels,*], spectrum[goodPixels]/error[goodPixels], npoly) ;, method=1)
        model = c # weights

        model = model ;;*mpoly

        err = (spectrum[goodPixels]-model[goodPixels])/error[goodPixels]

        if(keyword_set(clean)) then begin
            tmp = where(abs(err) gt 3, m, complem=w, ncompl=cw)
            if(m ne 0 and m lt n_elements(goodPixels)*0.1) then begin
                print, 'Outliers:',m
                goodPixels=goodPixels[w]
                cgoodPixels=cw
            endif else m=0
        endif
    endrep until (m eq 0)

    outpixmask=pixmask*0
    outpixmask[goodPixels]=1

    mcont = mpoly 
    tellcorr = interpol(atm_model_int_log, wllog, wl+swlvec, spl=0) * mcont
    
    badwl = where(wl+swlvec gt wllog[n_wl-1] or wl+swlvec lt wllog[0], cbadwl)
    if(cbadwl gt 0) then tellcorr[badwl]=!values.d_nan
    w_norm = total(weights)
    weights /= w_norm

    return, err
end



function fire_fit_telluric_spectrum,wl,spectrum,error=error,atmwl=atmwl,$
    star_list=star_list,inp_star_weights=inp_star_weights,fixpar=fixpar,$
    atm_model_grid_file=atm_model_grid_file,exclreg=exclreg,$
    start=start,pixmask=pixmask_inp,clean=clean,maskoh=maskoh,maskh2o=maskh2o,oh_mask_sigma=oh_mask_sigma,mdegree=mdegree,lsf_moments=lsf_moments,$
    inp_lsf=inp_lsf,ilsf_kms=ilsf_kms,conv_nsegments=conv_nsegments,gausslosvd=gausslosvd,pixconv=pixconv,diff_airmass=diff_airmass,$
    swlnpoints=swlnpoints,swlmax=swlmax,swlstart=swlstart,$
    maxwl_global=maxwl_global,minwl_global=minwl_global,trans_thr=trans_thr,$
    wl_s_vec=wl_s_vec,wl_s_struct=wl_s_struct,$
    err_par=err_par,chi2dof=chi2dof,plot=plot,$
    tellcorr=tellcorr,model=model,mcont=mcont,weights=weights,w_norm=w_norm,$
    calib_path=calib_path, str_out=str_out, objname=objname, quiet=quiet

    common wlshift_par_cache, wlshift_par_ini, wlshift_swlnpoints, wlshift_mpfit_status

    n_wl=n_elements(wl)
    dwl=wl[1]-wl[0]

    if(n_elements(calib_path) ne 1) then calib_path=getenv('FIRE_PIPELINE_PATH')+'calib_FIRE/'
    if(n_elements(star_list) eq 0) then star_list=file_search(calib_path+'stellar_templates/phoenix/lte09600-4.50*.fits.gz')
    if(n_elements(mdegree) ne 1) then mdegree=5
    if(n_elements(swlnpoints) ne 1) then swlnpoints=0
    if(n_elements(lsf_moments) ne 1) then lsf_moments=2
    if(n_elements(fixpar) lt 4) then fixpar=intarr(5+swlnpoints)
    if(n_elements(start) lt 4) then start=[0d,20d,1.1d,2.5d,40d]
    if(n_elements(swlstart) ne swlnpoints and swlnpoints gt 0) then swlstart=dblarr(swlnpoints)
    if(n_elements(swlmax) ne 1) then swlmax=(swlnpoints gt 0) ? ((10d*dwl) > (1.01*max(abs(swlstart)))) : (10d*dwl)
    if(n_elements(maxwl_global) ne 1) then maxwl_global=2518.
    if(n_elements(minwl_global) ne 1) then minwl_global=307.
    if(n_elements(trans_thr) ne 1) then trans_thr=0.05
    if(n_elements(diff_airmass) ne 1) then diff_airmass=0d

    wlshift_swlnpoints=swlnpoints
    wlshift_mpfit_status=-1
    wlshift_par_ini=(swlnpoints eq 0)? [0d] : dblarr(swlnpoints)

    if(n_elements(error) ne n_wl) then error=wl*0d +1d
    if(n_elements(pixmask_inp) ne n_wl) then begin
        pixmask_inp=intarr(n_wl)+1
        pixmask_inp[0:40]=0
        pixmask_inp[n_wl-41:*]=0
        neg_pix=where(spectrum lt 0 and spectrum/error gt 1,cneg_pix)
        if(cneg_pix gt 0) then pixmask_inp[neg_pix]=0
    endif
    pixmask=pixmask_inp
    s_excl=size(exclreg)
    nexcl=(s_excl[0] eq 2)? s_excl[2] : ((s_excl[0] eq 1 and s_excl[1] eq 2)? 1 : 0)
    for i=0,nexcl-1 do begin
        minwl=exclreg[0,i]-exclreg[1,i]
        maxwl=exclreg[0,i]+exclreg[1,i]
        badreg=where((wl ge minwl) and (wl le maxwl),cbad)
        if(cbad gt 0) then pixmask[badreg]=0
    endfor

    if (keyword_set(maskoh)) then begin
        if(n_elements(oh_mask_sigma) ne 1) then oh_mask_sigma=50.
        lines_oh=read_asc(calib_path+'linelists/linesOH_R2k_HITRAN_H2O_nm.tab')
        lines_oh=lines_oh[*,where(lines_oh[2,*] gt 1e-1)]
        if(~keyword_set(maskh2o)) then lines_oh=lines_oh[*,where(lines_oh[0,*] lt 2350.0)]
        wl_oh=transpose(lines_oh[0,*])
        if(keyword_set(atmwl)) then wl_oh=convert_wl_v2a(wl_oh,/nm)
        flag=mask_emission_lines(wl,wl_oh,oh_mask_sigma,/kms)
        mask_bad=where(flag gt 0, cmask_bad)
        if(cmask_bad gt 0) then pixmask[mask_bad]=0
    endif

    atm_model_grid=fire_read_atmosphere_models(wl,atm_model_grid_file=atm_model_grid_file,atmwl=atmwl)
    s_atm_grid=size(atm_model_grid[0].transmission)
    if(lsf_moments eq 0 and n_elements(inp_lsf) gt 0) then begin
        atm_model_grid=convol_atm_transmission_lsf(atm_model_grid,inp_lsf,nsegments=conv_nsegments,kms=ilsf_kms)
    endif

    bspec=where((finite(spectrum+error) ne 1) or (spectrum le 0) or $
        (error le 0) or (wl le minwl_global) or (wl ge maxwl_global) or $
        (atm_model_grid[0].transmission[*,s_atm_grid[2]/2,s_atm_grid[3]/2] lt trans_thr), cbspec)
    if(cbspec gt 0) then pixmask[bspec]=0    

    n_star=n_elements(star_list)
    star=dblarr(n_wl,n_star)

    for i=0,n_star-1 do begin
        if(n_star eq n_elements(inp_star_weights)) then if(inp_star_weights[i] eq 0.) then continue
        star_cur=readfits(star_list[i],hs_cur,/silent)
        parse_spechdr,hs_cur,wl=lwls,nlam=nlamStar,velscale=velScStar,unitwl=unitwl
        if(unitwl eq 'nm') then lwls*=10d
        shift_pix = fix(start[0]/velScStar)
        v0_shift = shift_pix*velScStar
        star[*,i]=interpol(star_cur,lwls/1d1*(1d0+v0_shift/299792.458d),wl,/spl) ;;;; vacuum wavelength in nm
        minwls=lwls[0]/1d1*(1d0+v0_shift/299792.458d)
        wl_short_idx=where(wl lt minwls, c_wl_short_idx, compl=wl_short_compl_idx)
        if(c_wl_short_idx gt 0) then star[wl_short_idx,i]=star[wl_short_compl_idx[0],i]
        maxwls=lwls[nlamStar-1]/1d1*(1d0+v0_shift/299792.458d)
        wl_long_idx=where(wl gt maxwls, c_wl_long_idx, compl=wl_long_compl_idx, ncompl=c_wl_long_compl_idx)
        if(c_wl_long_idx gt 0) then star[wl_long_idx,i]=star[wl_long_compl_idx[c_wl_long_compl_idx-1],i]
    endfor
    if (n_star gt 0l) then begin
        idx_pixmask = where((n_star gt 1 ? (product(finite(star),2) ne 1) : (finite(star) ne 1)) or (wl lt lwls[0]/1d1*(1d0+v0_shift/299792.458d)) or (wl gt lwls[nlamStar-1l]/1d1*(1d0+v0_shift/299792.458d)), n_idx_pixmask) ;;; lwls general for all STAR
        if (n_idx_pixmask gt 0l) then pixmask[idx_pixmask] = 0
    endif
    if(n_star eq n_elements(inp_star_weights)) then begin
        ;; using input weights for stellar templates
        star=star # inp_star_weights
        n_star=1
    endif

    if ((lsf_moments eq 0) and (n_elements(inp_lsf) gt 0)) then begin
        size_inp_lsf = size(inp_lsf)

        s_inp_lsf= (n_elements(inp_lsf) eq 1)? size([inp_lsf]) : size(inp_lsf)
        inp_lsf_vel=dblarr(n_wl)
        inp_lsf_sig=dblarr(n_wl)+0.001
        inp_lsf_h3=dblarr(n_wl)
        inp_lsf_h4=dblarr(n_wl)
        inp_lsf_h5=dblarr(n_wl)
        inp_lsf_h6=dblarr(n_wl)
        if(s_inp_lsf[0] eq 1) then inp_lsf_sig=(s_inp_lsf[1] eq 1)? dblarr(n_wl)+inp_lsf[0] : interpol(inp_lsf,dindgen(s_inp_lsf[1])/(s_inp_lsf[1]-1d),dindgen(n_wl)/(n_wl-1d))
        if(s_inp_lsf[0] eq 2) then begin
            inp_lsf_vel=(s_inp_lsf[1] eq 1)? dblarr(n_wl)+inp_lsf[0,0] : interpol(inp_lsf[*,0],dindgen(s_inp_lsf[1])/(s_inp_lsf[1]-1d),dindgen(n_wl)/(n_wl-1d))
            inp_lsf_sig=(s_inp_lsf[1] eq 1)? dblarr(n_wl)+inp_lsf[0,1] : interpol(inp_lsf[*,1],dindgen(s_inp_lsf[1])/(s_inp_lsf[1]-1d),dindgen(n_wl)/(n_wl-1d))
            if(s_inp_lsf[2] gt 2) then begin
                inp_lsf_h3=(s_inp_lsf[1] eq 1)? dblarr(n_wl)+inp_lsf[0,2] : interpol(inp_lsf[*,2],dindgen(s_inp_lsf[1])/(s_inp_lsf[1]-1d),dindgen(n_wl)/(n_wl-1d))
                inp_lsf_h4=(s_inp_lsf[1] eq 1)? dblarr(n_wl)+inp_lsf[0,3] : interpol(inp_lsf[*,3],dindgen(s_inp_lsf[1])/(s_inp_lsf[1]-1d),dindgen(n_wl)/(n_wl-1d))
            endif
            if(s_inp_lsf[2] gt 4) then begin
                inp_lsf_h5=(s_inp_lsf[1] eq 1)? dblarr(n_wl)+inp_lsf[0,4] : interpol(inp_lsf[*,4],dindgen(s_inp_lsf[1])/(s_inp_lsf[1]-1d),dindgen(n_wl)/(n_wl-1d))
                inp_lsf_h6=(s_inp_lsf[1] eq 1)? dblarr(n_wl)+inp_lsf[0,5] : interpol(inp_lsf[*,5],dindgen(s_inp_lsf[1])/(s_inp_lsf[1]-1d),dindgen(n_wl)/(n_wl-1d))
            endif
        endif

        star = conv_multi(star, velScale=dwl, $
            velvec=inp_lsf_vel*(keyword_set(ilsf_kms)? wl/299792.458d : 1d), $
            disvec=inp_lsf_sig*(keyword_set(ilsf_kms)? wl/299792.458d : 1d), $
            h3vec=inp_lsf_h3, h4vec=inp_lsf_h4, $
            h5vec=inp_lsf_h5, h6vec=inp_lsf_h6, nsegments=conv_nsegments)
    endif

    functargs={lsf_moments:lsf_moments,pixmask:pixmask,$
        wl:wl,spectrum:spectrum,error:error,$
        star:star,clean:keyword_set(clean),$
        mdegree:mdegree,swlnpoints:swlnpoints,swlmax:swlmax,$
        gausslosvd:keyword_set(gausslosvd),pixconv:keyword_set(pixconv)}

    npar = 4 + (lsf_moments>1)-1 + swlnpoints + mdegree
    start_vec=(lsf_moments gt 2)? [start,dblarr(lsf_moments-2)] : start
    if(swlnpoints gt 0) then start_vec=[start_vec,swlstart]
    if(mdegree gt 0) then start_vec=[start_vec,dblarr(mdegree)]
    start_vec[0]-=v0_shift
    start_vec[1]=(((dwl/wl[0])*299792.5d/10d) > start[1] < (500d))
    parinfo=replicate({step:1d-3,limits:[-1d,1d],limited:[1,1],fixed:0},npar)
    parinfo[0].step=3d-3*dwl/wl[0]*299792.5d
    parinfo[0].limits=[-1000d,1000d]+start_vec[0] ;;; vr_star, km/s
    parinfo[1].step=3e-2*dwl/wl[0]*299792.5d
    parinfo[1].limits=[(dwl/wl[0])*299792.5d/10d,500d] ;; vsini_star, km/s
    parinfo[2].limits=[1d,2.3d] ;; airmass
    parinfo[3].limits=[min(atm_model_grid.pwv),max(atm_model_grid.pwv)] ;; water vapor
    parinfo[0:3].fixed=fixpar[0:3]
    if(lsf_moments ge 2) then begin
        parinfo[4].step=3d-3
        parinfo[4].limits=[dwl*0.1d,dwl*15d]*299792.458d/wl[0] ;; lsf_sigma
        parinfo[4].fixed=fixpar[4]
        if(lsf_moments gt 2) then parinfo[5:5+lsf_moments-3].limits=[-0.3d,0.3d]
    endif
    if(swlnpoints gt 0) then begin
        parinfo[npar-swlnpoints-mdegree:npar-mdegree-1].limits=rebin([-1d,1d]*swlmax,2,swlnpoints)
        parinfo[npar-swlnpoints-mdegree:npar-mdegree-1].step=(wl[1]-wl[0])*1d-2
    endif
    if(mdegree gt 0) then begin
        parinfo[npar-mdegree:npar-1].limits=rebin([-1d,1d],2,mdegree)
        parinfo[npar-mdegree:npar-1].step=1d-3
    endif
print,'start_vec=',start_vec
    if((total(fixpar) eq n_elements(fixpar)) and swlnpoints eq 0 and mdegree le 0) then begin
        p=start_vec
        err_par=start_vec*0.0
        mpfit_status=10
    endif else begin
        p = mpfit('fitfunc_telluric_spectrum', start_vec, parinfo=parinfo, functargs=functargs, $
                perror=err_par, bestnorm=chisqr, dof=dof, nfev=nfev, nfree=nfree, npegged=npegged, niter=niter, $
                status=mpfit_status, errmsg=mpfit_errmsg, ftol=1d-4*1d-2, quiet=quiet)
        if(n_elements(err_par) ne n_elements(start_vec)) then err_par=start_vec*0.0
    endelse

    if(finite(p[0]) eq 1 and mpfit_status gt 0) then begin
        p_cur=p
        p_cur[2]=1.0>(p[2]+diff_airmass)<2.5
        if(~keyword_set(quiet) and diff_airmass ne 0d) then message,/inf,'Computing the correction for an adjusted airmass value of: '+string(p_cur[2]+diff_airmass)
        e = fitfunc_telluric_spectrum(p_cur,lsf_moments=lsf_moments,pixmask=pixmask,$
                                  wl=wl,spectrum=spectrum,error=error,star=star,$
;                                  atm_model_grid=atm_model_grid,$
                                  mdegree=mdegree,$
                                  mcont=mcont,swlnpoints=swlnpoints,swlmax=swlmax,$
                                  swlvec=wl_s_vec,tellcorr=tellcorr,model=model,$
                                  s_wl_struct=wl_s_struct,weights=weights,w_norm=w_norm,gausslosvd=keyword_set(gausslosvd), $
                                  outpixmask=outpixmask,pixconv=keyword_set(pixconv))
        if(swlnpoints gt 0) then wl_s_struct.ewl_shift=err_par[npar-swlnpoints-mdegree:npar-mdegree-1]
    endif else begin
        message,'Telluric correction FAILED!!!',/info
        if(n_elements(mpfit_errmsg) gt 0) then message,'MPFIT '+mpfit_errmsg,/info
        tellcorr=dblarr(n_elements(spectrum))+1d
        mcont=tellcorr
        outpixmask=pixmask
        p=start_vec*!values.d_nan
        e=dblarr(n_elements(spectrum))
        model=spectrum*!values.d_nan
        weights=dblarr(n_star)+1d/n_star
        w_norm=1d
    endelse

    badtell=where(tellcorr le 0, cbadtell)
    if(cbadtell gt 0) then tellcorr[badtell]=!values.d_nan
    goodPixels=where(pixmask eq 1, cgoodPixels,compl=badPixels,ncompl=cbadPixels)
    chi2dof = total(e^2)/(cgoodPixels-1-n_elements(p)+total(fixpar))
    quality = ~outpixmask

    if(keyword_set(plot) and finite(p[0]) eq 1) then begin
        maxspec=max(spectrum[goodPixels],/nan)
        plot,wl,spectrum,xs=1,ys=1,yr=[-0.1,1.1]*maxspec, $
            xtitle='Wavelength, nm',ytitle='Flux, counts',title=(n_elements(objname) eq 1)? objname : '',/nodata
        wl_good=wl
        if(cbadPixels gt 0) then wl_good[badPixels]=!values.f_nan
        oplot,wl,spectrum,col=254
        oplot,wl,model,col=254
        oplot,wl_good,spectrum
        oplot,wl[goodPixels],model[goodPixels],psym=4,col=192,syms=0.3
        oplot,wl,spectrum-model,col=40,psym=-7,syms=0.3
        oplot,wl_good,spectrum-model,col=128,psym=-7,syms=0.3
        oplot,wl,spectrum/(tellcorr/mcont/total(weights)),col=40
        oplot,wl_good,spectrum/(tellcorr/mcont/total(weights)),col=80
        oplot,wl,mcont*maxspec*0.75,col=128,thick=3
    endif

    p=p[0:n_elements(p)-1-mdegree]
    err_par=err_par[0:n_elements(err_par)-1-mdegree]
    p[0] += v0_shift
    print, 'p = ', p
    print,' e = ', err_par

    str_out = { $
            objname:            (n_elements(objname) eq 1)?         strtrim(objname,2) : '', $
                
            wave:               double(wl), $
            flux:               double(spectrum), $
            error:              double(error), $
            quality:            (n_elements(quality) eq n_wl)?      double(quality)  : dblarr(n_wl) + !values.d_nan, $
            bestfit:            (n_elements(model) eq n_wl)?        double(model)    : dblarr(n_wl) + !values.d_nan, $
            model:              (n_elements(outmodel) eq n_wl)?     double(outmodel) : dblarr(n_wl) + !values.d_nan, $
            mcont:              (n_elements(mcont) eq n_wl)?        double(mcont)    : dblarr(n_wl) + !values.d_nan, $
            tellcorr:           (n_elements(tellcorr) eq n_wl)?     double(tellcorr) : dblarr(n_wl) + !values.d_nan, $
            swlvec:             (n_elements(wl_s_vec) eq n_wl)?     double(wl_s_vec) : dblarr(n_wl), $
            lsfvec:             (n_elements(inp_lsf) eq n_wl)?      double(inp_lsf)  : dblarr(n_wl), $

            mdegree:            (n_elements(mdegree) eq 1)?         fix(mdegree)         : 0, $
            swlnpoints:         (n_elements(swlnpoints) eq 1)?      fix(swlnpoints)      : 0, $
            swlmax:             (n_elements(swlmax) eq 1)?          double(swlmax)       : 0d, $
            lsf_moments:        (n_elements(lsf_moments) eq 1)?     fix(lsf_moments)     : 0, $
            star_list:          (n_elements(star_list) eq n_star)?  strtrim(star_list,2) : '', $
            weights:            (n_elements(weights) eq n_star)?    double(weights)      : 0d, $
            w_norm:             double(w_norm), $

            fixpar:             intarr(9), $

            initial_v:          (n_elements(start_vec) ge 1)?       double(start_vec[0]) : !values.d_nan, $
            initial_vsini:      (n_elements(start_vec) ge 2)?       double(start_vec[1]) : !values.d_nan, $
            initial_airmass:    (n_elements(start_vec) ge 3)?       double(start_vec[2]) : !values.d_nan, $
            initial_pwv:        (n_elements(start_vec) ge 4)?       double(start_vec[3]) : !values.d_nan, $
            initial_lsf:        (n_elements(start_vec) ge 5)?       double(start_vec[4]) : !values.d_nan, $
            initial_h3:         (n_elements(start_vec) ge 6)?       double(start_vec[5]) : 0d, $
            initial_h4:         (n_elements(start_vec) ge 7)?       double(start_vec[6]) : 0d, $
            initial_h5:         (n_elements(start_vec) ge 8)?       double(start_vec[7]) : 0d, $
            initial_h6:         (n_elements(start_vec) ge 9)?       double(start_vec[8]) : 0d, $

            v:                  (n_elements(p)-swlnpoints ge 1)?               double(p[0]) : !values.d_nan, $
            vsini:              (n_elements(p)-swlnpoints ge 2)?               double(p[1]) : !values.d_nan, $
            airmass:            (n_elements(p)-swlnpoints ge 3)?               double(p[2]) : !values.d_nan, $
            pwv:                (n_elements(p)-swlnpoints ge 4)?               double(p[3]) : !values.d_nan, $
            lsf:                (n_elements(p)-swlnpoints ge 5)?               double(p[4]) : !values.d_nan, $
            h3:                 (n_elements(p)-swlnpoints ge 6)?               double(p[5]) : 0d, $ 
            h4:                 (n_elements(p)-swlnpoints ge 7)?               double(p[6]) : 0d, $
            h5:                 (n_elements(p)-swlnpoints ge 8)?               double(p[7]) : 0d, $
            h6:                 (n_elements(p)-swlnpoints ge 9)?               double(p[8]) : 0d, $

            e_v:                (n_elements(err_par)-swlnpoints ge 1)?         double(err_par[0]) : !values.d_nan, $
            e_vsini:            (n_elements(err_par)-swlnpoints ge 2)?         double(err_par[1]) : !values.d_nan, $
            e_airmass:          (n_elements(err_par)-swlnpoints ge 3)?         double(err_par[2]) : !values.d_nan, $
            e_pwv:              (n_elements(err_par)-swlnpoints ge 4)?         double(err_par[3]) : !values.d_nan, $
            e_lsf:              (n_elements(err_par)-swlnpoints ge 5)?         double(err_par[4]) : !values.d_nan, $
            e_h3:               (n_elements(err_par)-swlnpoints ge 6)?         double(err_par[5]) : 0d, $
            e_h4:               (n_elements(err_par)-swlnpoints ge 7)?         double(err_par[6]) : 0d, $
            e_h5:               (n_elements(err_par)-swlnpoints ge 8)?         double(err_par[7]) : 0d, $
            e_h6:               (n_elements(err_par)-swlnpoints ge 9)?         double(err_par[8]) : 0d, $

            chi2dof:            (n_elements(chi2dof) eq 1)?         double(chi2dof) : !values.d_nan, $
            dof:                (n_elements(cgoodPixels) ne 0)?     double(cgoodPixels - 1 - n_elements(p) + total(fixpar)) : !values.d_nan, $

            mpfit_bestnorm:     (n_elements(chisqr) eq 1)?          double(chisqr)       : !values.d_nan, $
            mpfit_dof:          (n_elements(dof) eq 1)?             double(dof)          : !values.d_nan, $
            mpfit_nfev:         (n_elements(nfev) eq 1)?            long(nfev)           : -1l, $
            mpfit_nfree:        (n_elements(nfree) eq 1)?           long(nfree)          : -1l, $
            mpfit_npegged:      (n_elements(npegged) eq 1)?         long(npegged)        : -1l, $
            mpfit_niter:        (n_elements(niter) eq 1)?           fix(niter)           : 0, $
            mpfit_status:       (n_elements(mpfit_status) eq 1)?    double(mpfit_status) : !values.d_nan, $
            mpfit_errmsg:       (n_elements(mpfit_errmsg) eq 1)?    string(mpfit_errmsg) : "ERROR (mpfit): didn't start" $
        }

    str_out.fixpar[0:n_elements(fixpar)-1] = fixpar

    err_par=err_par[0:npar-1-swlnpoints-mdegree]
    return, p[0:npar-1-swlnpoints-mdegree]
end
