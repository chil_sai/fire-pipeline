function fire_extract_order,im,trace_coeff,n_ord,x0=x0,y0=y0,ny=ny,ymap=ymap,nocorrect=nocorrect

    s_im=size(im)
    if(n_elements(x0) ne 1) then x0=s_im[1]/2
    if(n_elements(y0) ne 1) then y0=s_im[2]*0.6

    if(n_elements(ny) ne 1) then ny=51
    order_img=fltarr(s_im[1],ny)+!values.f_nan
    ymap=fltarr(s_im[1],ny)+!values.f_nan

    x_vec=findgen(s_im[1])-x0
    order_low = poly(x_vec,trace_coeff[*,n_ord,0])
    order_hi = poly(x_vec,trace_coeff[*,n_ord,1])
    np=n_elements(trace_coeff[*,n_ord,1])
    if(~keyword_set(nocorrect)) then begin
        order_mean=(order_hi+order_low)/2d
        dorder_mean=(poly(x_vec,trace_coeff[1:*,n_ord,0]*(1d +dindgen(np)))+poly(x_vec,trace_coeff[1:*,n_ord,1]*(1d +dindgen(np))))/2d
;;        ord_ycorr=abs(dorder_mean)*10d*(0.1d +abs(order_mean[x0]-y0)/(s_im[2]))
        ord_ycorr=abs((order_mean-order_mean[x0])/600d)*11d*(0.05d +abs(order_mean[x0]-y0)/(s_im[2]))
        order_low+=ord_ycorr
        order_hi+=ord_ycorr
    endif

    for x=0,s_im[1]-1 do begin
        if(order_low[x] lt 0 or order_hi[x] gt s_im[2]-1) then continue
        xx=order_low[x]+dindgen(ny)*(order_hi[x]-order_low[x])/(ny-1.)
        order_img[x,*]=interpolate(transpose(im[x,*]),xx)
        ymap[x,*]=xx
    endfor

    return, order_img
end
