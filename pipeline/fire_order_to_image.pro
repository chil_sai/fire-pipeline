function fire_order_to_image,order_img,trace_coeff,n_ord,x0=x0,y0=y0,nx=nx,ny=ny,nocorrect=nocorrect

    s_ord=size(order_img)
    ny_order=s_ord[2]
    if(n_elements(nx) ne 1) then nx=2048
    if(n_elements(ny) ne 1) then ny=2048
    if(n_elements(x0) ne 1) then x0=nx/2
    if(n_elements(y0) ne 1) then y0=ny*0.6

    im=fltarr(nx,ny)

    x_vec=findgen(nx)-x0
    order_low = poly(x_vec,trace_coeff[*,n_ord,0])
    order_hi = poly(x_vec,trace_coeff[*,n_ord,1])
    np=n_elements(trace_coeff[*,n_ord,1])
    if(~keyword_set(nocorrect)) then begin
        order_mean=(order_hi+order_low)/2d
        dorder_mean=(poly(x_vec,trace_coeff[1:*,n_ord,0]*(1d +dindgen(np)))+poly(x_vec,trace_coeff[1:*,n_ord,1]*(1d +dindgen(np))))/2d
;;;        ord_ycorr=(order_mean-order_mean[x0])/(abs(order_hi[x0]-order_low[x0])*2.5)
;;        ord_ycorr=abs(dorder_mean)*8d*(0.1d +abs(order_mean[x0]-y0)/(ny))
        ord_ycorr=abs((order_mean-order_mean[x0])/600d)*11d*(0.05d +abs(order_mean[x0]-y0)/(ny))
        order_low+=ord_ycorr
        order_hi+=ord_ycorr
    endif

    for x=0,nx-1 do begin
        if(fix(order_low[x]) lt 0 or fix(order_hi[x]) gt ny-1) then continue
        xx=order_low[x]+dindgen(ny_order)*(order_hi[x]-order_low[x])/(ny_order-1.)
        im_col=interpol(order_img[x,*],$
                        order_low[x]+dindgen(ny_order)*(order_hi[x]-order_low[x])/(ny_order-1.),$
                        fix(order_low[x])+dindgen(fix(order_hi[x])-fix(order_low[x])+1))
        im[x,fix(order_low[x]):fix(order_hi[x])]=transpose(im_col)
    endfor

    return, im
end
