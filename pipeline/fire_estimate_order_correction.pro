function fire_estimate_order_correction,extracted_flat_inp,blaze_offset_struct,wl_arr,$
    deg_bo_blue=deg_bo_blue,deg_bo_red=deg_bo_red,deg_corr=deg_corr,thres=thres,$
    blue_ord_idx=blue_ord_idx,blue_max=blue_max,red_min=red_min,$
    force_illum_corr=force_illum_corr,lin_orders=lin_orders,verbose=verbose

    if(n_elements(blue_ord_idx) ne 1) then blue_ord_idx=31

    if(n_elements(deg_bo_blue) ne 1) then deg_bo_blue=-1 ;; degree of the fitting polynomial to fit the blaze_offset trend in the red
    if(n_elements(deg_bo_red) ne 1) then deg_bo_red=2 ;; degree of the fitting polynomial to fit the blaze_offset trend in the red
    if(n_elements(deg_corr) ne 1) then deg_corr=2 ;; degree of the fitting polynomial for the correction vector
    if(n_elements(blue_max) ne 1) then blue_max=31
    if(n_elements(red_min) ne 1) then red_min=12-1
    if(n_elements(trans_start) ne 1) then trans_start=800
    if(n_elements(trans_end) ne 1) then trans_end=1200

    s_ord=size(extracted_flat_inp)
    if(n_elements(thres) ne 1) then thres=0.02

    nx=s_ord[1]
    n_ord=s_ord[2]
    ord_num = blue_ord_idx-lindgen(n_ord)

    ic_wl_arr=(~keyword_set(force_illum_corr))? dblarr(nx,n_ord)+1d : fire_model_illumination_correction(/twod)
    if(~keyword_set(lin_orders)) then ic_wl_arr=reverse(ic_wl_arr)
    extracted_flat=extracted_flat_inp/ic_wl_arr

    ord_over_start=lonarr(n_ord)
    ord_over_end=lonarr(n_ord)
    for i=1,n_ord-1 do begin
        wl_over_red_idx=where(wl_arr[*,i] lt max(wl_arr[*,i-1],/nan),cwl_over_red_idx) ;;; i
        if(cwl_over_red_idx gt 0) then begin
            wl_over_red=wl_arr[wl_over_red_idx,i]
            ord_over_end[i]=wl_over_red_idx[cwl_over_red_idx-1]
        endif
        wl_over_blue_idx=where(wl_arr[*,i-1] gt min(wl_arr[*,i],/nan),cwl_over_blue_idx) ;;; i-1
        if(cwl_over_blue_idx gt 0) then begin
            wl_over_blue=wl_arr[wl_over_blue_idx,i-1]
            ord_over_start[i-1]=wl_over_blue_idx[0]
        endif
    endfor

    flat_corrvec_array_br=dblarr(nx,n_ord,2)
    flat_corrvec_fit_array=dblarr(nx,n_ord)
    flat_corrvec_fit_array_br=dblarr(nx,n_ord,2)

    ord_flst_min=min(blaze_offset_struct.ord_flat_stitch,idx_boff_min,max=ord_flst_max,sub=idx_boff_max)

    blaze_offset_fit_red=dblarr(n_elements(blaze_offset_struct[0].blaze_offset))
    blaze_offset_fit_blue=dblarr(n_elements(blaze_offset_struct[0].blaze_offset))
    ord_mask=intarr(n_ord)
    ord_mask[[0,1,11,16,17,18,19,20]]=1    ;;; ord_mask[[0,1,11,16,17]]=1
    blue_bo_idx=where(ord_mask eq 0 and $
                      ord_num le blue_max and ord_num gt ord_flst_min and $
                      finite(blaze_offset_struct[idx_boff_min].blaze_offset) eq 1, cblue_bo_idx)
    if(cblue_bo_idx gt 0) then begin
        if(cblue_bo_idx gt deg_bo_blue+1) then begin
            c_blue_bo_fit=robust_poly_fit(double(blue_bo_idx),blaze_offset_struct[idx_boff_min].blaze_offset[blue_bo_idx],deg_bo_blue,/double)
            blaze_offset_fit_blue[0:blue_ord_idx-ord_flst_min-1]=poly(dindgen(blue_ord_idx-ord_flst_min),c_blue_bo_fit)
            blaze_offset_fit_red[0:blue_ord_idx-ord_flst_max-1]=poly(dindgen(blue_ord_idx-ord_flst_max),c_blue_bo_fit)
        endif else begin
            blaze_offset_fit_blue[0:blue_ord_idx-ord_flst_min-1]=(blue_bo_idx eq 1)? blaze_offset_struct[idx_boff_min].blaze_offset[blue_bo_idx] : median(blaze_offset_struct[idx_boff_min].blaze_offset[blue_bo_idx],/even)
            blaze_offset_fit_red[0:blue_ord_idx-ord_flst_max-1]=(blue_bo_idx eq 1)? blaze_offset_struct[idx_boff_min].blaze_offset[blue_bo_idx] : median(blaze_offset_struct[idx_boff_min].blaze_offset[blue_bo_idx],/even)
        endelse
    endif

    red_bo_idx=where(ord_mask eq 0 and $
                     ord_num ge red_min and ord_num le ord_flst_min and $
                     finite(blaze_offset_struct[idx_boff_max].blaze_offset) eq 1, cred_bo_idx)
    if(cred_bo_idx gt 0) then begin
        if(cred_bo_idx gt deg_bo_red+1) then begin
            c_red_bo_fit=robust_poly_fit(double(red_bo_idx),blaze_offset_struct[idx_boff_max].blaze_offset[red_bo_idx],deg_bo_red,/double)
            blaze_offset_fit_blue[blue_ord_idx-ord_flst_min:*]=poly(blue_ord_idx-ord_flst_min+dindgen(n_ord-(blue_ord_idx-ord_flst_min)),c_red_bo_fit)
            blaze_offset_fit_red[blue_ord_idx-ord_flst_max:*]=poly(blue_ord_idx-ord_flst_max+dindgen(n_ord-(blue_ord_idx-ord_flst_max)),c_red_bo_fit)
        endif else begin
            blaze_offset_fit_blue[blue_ord_idx-ord_flst_min:*]=(cred_bo_idx eq 1)? blaze_offset_struct[idx_boff_max].blaze_offset[red_bo_idx] : median(blaze_offset_struct[idx_boff_max].blaze_offset[red_bo_idx],/even)
            blaze_offset_fit_red[blue_ord_idx-ord_flst_max:*]=(cred_bo_idx eq 1)? blaze_offset_struct[idx_boff_max].blaze_offset[red_bo_idx] : median(blaze_offset_struct[idx_boff_max].blaze_offset[red_bo_idx],/even)
        endelse
    endif

    xvec = dindgen(nx)
    for i=0,n_ord-1 do begin
        dflat_blue = median(1d/(extracted_flat[*,i]/shift(extracted_flat[*,i],blaze_offset_fit_blue[i])),15)
        if(i eq 0) then dflat_blue[1340:*]=!values.d_nan
        if(i eq 1) then dflat_blue[1700:*]=!values.d_nan
        if(i eq 2) then dflat_blue[1980:*]=!values.d_nan
        dflat_blue[0:15]=!values.d_nan
        dflat_blue[2034:*]=!values.d_nan
        max_flat_ord = max((median(extracted_flat[*,i],15))[0.3*nx:0.7*nx],/nan)
        good_flat_blue = where(xvec gt abs(blaze_offset_fit_blue[i]) and $
                               xvec lt nx-1-abs(blaze_offset_fit_blue[i]) and $
                               finite(dflat_blue) eq 1 and $
                               (extracted_flat[*,i]<shift(extracted_flat[*,i],blaze_offset_fit_blue[i])) gt ((thres*max_flat_ord)>3d-5), cgood_flat_blue)
        flat_corrvec_array_br[*,i,0]=dflat_blue
        cfit_blue = robust_poly_fit(xvec[good_flat_blue],dflat_blue[good_flat_blue],deg_corr,/double)
        flat_corrvec_fit_array_br[*,i,0]=poly(xvec,cfit_blue)

        dflat_red = median(1d/(extracted_flat[*,i]/shift(extracted_flat[*,i],blaze_offset_fit_red[i])),15)
        good_flat_red = where(xvec gt abs(blaze_offset_fit_red[i]) and $
                               xvec lt nx-1-abs(blaze_offset_fit_red[i]) and $
                               finite(dflat_red) eq 1 and $
                               (extracted_flat[*,i]<shift(extracted_flat[*,i],blaze_offset_fit_red[i])) gt ((thres*max_flat_ord)>3d-5), cgood_flat_red)
        flat_corrvec_array_br[*,i,1]=dflat_red
        cfit_red = robust_poly_fit(xvec[good_flat_red],dflat_red[good_flat_red],deg_corr,/double)
        flat_corrvec_fit_array_br[*,i,1]=poly(xvec,cfit_red)
    endfor

    w_arr=dblarr(nx)
    w_arr[trans_start:trans_end,*]=dindgen(trans_end-trans_start+1)/(trans_end-trans_start)
    w_arr[trans_end+1:*,*]=1d
    flat_corrvec_fit_array=flat_corrvec_fit_array_br[*,*,1]
    if(ord_flst_min lt ord_flst_max) then $
        flat_corrvec_fit_array[*,blue_ord_idx-ord_flst_max]=flat_corrvec_fit_array_br[*,blue_ord_idx-ord_flst_max,0]*(1-w_arr)+flat_corrvec_fit_array_br[*,blue_ord_idx-ord_flst_max,1]*w_arr

    res_array=replicate({order:-1,wl:wl_arr[*,0],overlap_end:-1l,overlap_start:-1l,correction_vector:dblarr(nx)+1d,correction_vector_lin:dblarr(nx)+1d,lin_order:keyword_set(lin_orders)},n_ord)
    res_array.order=blue_ord_idx-indgen(n_ord)
    res_array.overlap_end=ord_over_end
    res_array.overlap_start=ord_over_start
    for i=0,n_ord-1 do begin
        res_array[i].wl=wl_arr[*,i]
        if(keyword_set(lin_orders)) then $
            res_array[i].correction_vector_lin=flat_corrvec_fit_array[*,i] $
        else $
            res_array[i].correction_vector=flat_corrvec_fit_array[*,i]
    endfor

    return,res_array    
end
