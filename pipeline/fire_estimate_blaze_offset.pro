function fire_estimate_blaze_offset,raw_orders,raw_flatn,wl_arr,lin_orders=lin_orders,$
    force_illum_corr=force_illum_corr,$
    xmin=xmin,xmax=xmax,ymargin=ymargin,thres=thres,$
    ord_overlap_flux_ff=ord_overlap_flux_ff,verbose=verbose

    s_ord=size(raw_orders)
    if(n_elements(thres) ne 1) then thres=0.02 ; 0.05
    if(n_elements(xmin) ne 1) then xmin=-40
    if(n_elements(xmax) ne 1) then xmax=40


    ns=fix(xmax)-fix(xmin)+1
    nx=s_ord[1]
    ny=s_ord[2]
    n_ord=s_ord[3]

    ic_wl_arr=(~keyword_set(force_illum_corr))? dblarr(nx,n_ord)+1d : fire_model_illumination_correction(/twod)
    if(~keyword_set(lin_orders)) then ic_wl_arr=reverse(ic_wl_arr)
    ic_wl_arr=rebin(reform(ic_wl_arr,nx,1,n_ord),nx,ny,n_ord)

    if(n_elements(ymargin) ne 1) then ymargin=1
    slit_reg=ymargin+lindgen(ny-1-ymargin)

    ord_extr_overlap=dblarr(nx,n_ord,ns)+!values.d_nan
    ord_over_start=lonarr(n_ord)
    ord_over_end=lonarr(n_ord)
    extracted_orders_arr=dblarr(nx,n_ord,ns)+!values.d_nan
    extracted_flatn=reform(total(raw_flatn[*,slit_reg,*],2),nx,n_ord)
    extracted_mask=dblarr(nx,n_ord)

    blaze_shift=dblarr(n_ord)
    ord_overlap_flux_ff=dblarr(2,n_ord)+!values.d_nan
    ord_overlap_flux_ff_arr=dblarr(2,n_ord,ns)+!values.d_nan

    for i=0,n_ord-1 do begin
        med_ext=median(extracted_flatn[*,i],15)
        b_ord=where(med_ext/max(med_ext[0.25*nx:0.75*nx],/nan) lt thres, cb_ord)
        if(keyword_set(verbose)) then print,'n_ord=',i+1,' cb_ord=',cb_ord
        if(cb_ord gt 0) then extracted_mask[b_ord,i]=!values.d_nan
    endfor
    extracted_mask[1340:*,0]=!values.d_nan
    extracted_mask[1700:*,1]=!values.d_nan
    extracted_mask[1980:*,2]=!values.d_nan
    extracted_mask[0:15,*]=!values.d_nan
    extracted_mask[2034:*,*]=!values.d_nan
    for x=0,ns-1 do begin
        extracted_orders=reform(total(((raw_orders/ic_wl_arr)*(raw_flatn/ic_wl_arr)/shift(raw_flatn/ic_wl_arr,xmin+x,0,0))[*,slit_reg,*],2,/nan)/double(ny-1-ymargin),nx,n_ord)+extracted_mask
        if(~keyword_set(lin_orders)) then extracted_orders=rotate(extracted_orders,5)
        extracted_orders_arr[*,*,x]=extracted_orders
        if(x ne 0) then begin
            extracted_orders[0:abs(x),*]=!values.d_nan
            extracted_orders[nx-1-abs(x):*,*]=!values.d_nan
        endif
        for i=1,n_ord-1 do begin
            wl_over_red_idx=where(wl_arr[*,i] lt max(wl_arr[*,i-1],/nan),cwl_over_red_idx) ;;; i
            if(cwl_over_red_idx gt 0) then begin
                wl_over_red=wl_arr[wl_over_red_idx,i]
                ord_over_end[i]=wl_over_red_idx[cwl_over_red_idx-1]
            endif
            wl_over_blue_idx=where(wl_arr[*,i-1] gt min(wl_arr[*,i],/nan),cwl_over_blue_idx) ;;; i-1
            if(cwl_over_blue_idx gt 0) then begin
                wl_over_blue=wl_arr[wl_over_blue_idx,i-1]
                ord_over_start[i-1]=wl_over_blue_idx[0]
            endif
        
            if(cwl_over_blue_idx gt 60) then begin
                ord_i_seg=interpol(extracted_orders[wl_over_red_idx,i],wl_over_red,wl_over_blue)
                ord_im1_seg=extracted_orders[wl_over_blue_idx,i-1]
                ord_overlap_flux_ff_arr[1,i-1,x]=median(ord_im1_seg)
                ord_overlap_flux_ff_arr[0,i,x]=median(ord_i_seg)
                ord_extr_overlap[wl_over_blue_idx,i-1,x]=ord_i_seg/ord_im1_seg
;;                ord_extr_overlap[0:wl_over_blue_idx[60],i-1,x]=!values.d_nan
;;                ord_extr_overlap[2005:*,i-1,x]=!values.f_nan
            endif
            if(cwl_over_red_idx gt 60) then begin
                ord_im1_seg=interpol(extracted_orders[wl_over_blue_idx,i-1],wl_over_blue,wl_over_red)
                ord_i_seg=extracted_orders[wl_over_red_idx,i]
                ord_extr_overlap[wl_over_red_idx,i,x]=ord_im1_seg/ord_i_seg
            endif
            ord_extr_overlap[*,i-1,x]=median(ord_extr_overlap[*,i-1,x],50)
        endfor
    endfor

    overlap_med=transpose(median(ord_extr_overlap[1024:*,*,*],dim=1))
    for i=0,n_ord-1 do begin
        blaze_shift[i]=interpol(xmin+dindgen(ns),overlap_med[*,i],1.0)
        if(blaze_shift[i] gt xmin and blaze_shift[i] lt xmax and finite(blaze_shift[i]) eq 1) then begin
            if(i gt 0) then ord_overlap_flux_ff[1,i-1]=ord_overlap_flux_ff_arr[1,i-1,fix(blaze_shift[i-1])]
            ord_overlap_flux_ff[0,i]=ord_overlap_flux_ff_arr[0,i,fix(blaze_shift[i])]
        endif else blaze_shift[i]=!values.f_nan
    endfor

    return,blaze_shift

end
