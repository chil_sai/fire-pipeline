function fire_get_wl_ini_from_data,arcline_arr,line_idx,kwl3d,deg1=deg1,deg2=deg2,mean=mean,$
    legendre=legendre,crd_map=crd_map,m0=m0,o_dim=o_dim

    if(n_elements(deg1) ne 1) then deg1=9
    if(n_elements(deg2) ne 1) then deg2=6

    ini_data2=dblarr(3,n_elements(line_idx))
    ini_data2[0,*]=arcline_arr[line_idx].order
    ini_data2[1,*]=(keyword_set(mean))?arcline_arr[line_idx].xmean : arcline_arr[line_idx].xpos[25]
    ini_data2[2,*]=poly3d_echelle(transpose(ini_data2[0,*]),transpose(ini_data2[1,*]),transpose(0d*line_idx)+0.5d,kwl3d,/irreg,legendre=legendre,crd_map=crd_map,m0=m0,o_dim=o_dim)
    t_ini=sfit_2deg(ini_data2,/irreg,deg1,deg2,kx=kxini)
    stdev=stdev(ini_data2[2,*]-t_ini)
    robsig=robust_sigma(ini_data2[2,*]-t_ini)

    wl_ini={deg1:deg1,deg2:deg2,kx:kxini,stdev:stdev,robsig:robsig,n_lines:n_elements(line_idx)}

    return,wl_ini
end
