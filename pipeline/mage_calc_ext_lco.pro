function mage_calc_ext_lco,lambda,airmass
    ;; lambda in Angstrom; airmass is unitless
    ;; La Silla extinction curve fitted with a 9th deg polynomial; good to 1% between 3200A and 25000A
    coef = [0.50211026, 0.00026183965, -3.3507152e-07, 8.1266154e-11, -1.0764788e-14, 8.7815143e-19, -4.5203179e-23, 1.4314619e-27, -2.5470532e-32, 1.9491256e-37]
    
    if(airmass lt 1) then message, /inf, 'Airmass must be >1'
    ext_vec = (1d -10d^(poly(lambda, coef)))^airmass

    return, ext_vec
end
