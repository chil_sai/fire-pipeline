pro run_fire_pipeline,conf_str,plot=plot
    pipeline_version = get_fire_pipeline_version()
    t0=systime(1)
    debug=1


    slit_length=51 ;; FIRE slit length in pixels (linearized/rectified)

    flat_file=conf_str.flat_file
    arc_file=conf_str.arc_file
    dark_file=conf_str.dark_file
    dayarc_file=(tag_exist(conf_str,'dayarc_file'))? conf_str.dayarc_file : ''

    obj_file_a=conf_str.obj_file_a
    obj_file_b=conf_str.obj_file_b
    n_obj_a=n_elements(obj_file_a)
    n_obj_b=n_elements(obj_file_b)
    outfile_merged=conf_str.outfile_merged
    outfile_orders=(tag_exist(conf_str,'outfile_orders'))? conf_str.outfile_orders : ''
    outfile_orders_err=(tag_exist(conf_str,'outfile_orders_err'))? conf_str.outfile_orders_err : ''
    ord_psf_ref=(tag_exist(conf_str,'extr_ord_psf_ref'))? 31-conf_str.extr_ord_psf_ref : 19

    outfile_merged_1d=conf_str.outfile_merged_1d
    outfile_orders_1d=(tag_exist(conf_str,'outfile_orders_1d'))? conf_str.outfile_orders_1d : ''
    outfile_orders_1d_err=(tag_exist(conf_str,'outfile_orders_1d_err'))? conf_str.outfile_orders_1d_err : ''

    raw2doutput=(tag_exist(conf_str,'raw_2d_output'))? conf_str.raw_2d_output : 0
    outfile_orders_raw=(tag_exist(conf_str,'outfile_orders_raw'))? conf_str.outfile_orders_raw : strmid(outfile_orders,0,strpos(outfile_orders,'.fits',/reverse_search))+'_raw.fits'

    bright=(tag_exist(conf_str,'bright'))? conf_str.bright : 1 ;;; bright mode by default
    clean=1 ;; clean=bright
    if(tag_exist(conf_str,'spec_tmp_type') and ~tag_exist(conf_str,'spec_tmp_list')) then begin
        star_list_all=file_search(getenv('FIRE_PIPELINE_PATH')+'calib_FIRE/stellar_templates/uni_vac/*.fits*')
        case strlowcase(conf_str.spec_tmp_type) of
            'agb'    : star_list=star_list_all[where(strpos(star_list_all,'mxcom') ge 0 or $
                           (strpos(star_list_all,'phx20_a+0.0_03') ge 0 and (strpos(star_list_all,'-1.5.fi') ge 0 or strpos(star_list_all,'-2.0.fi') ge 0)))]
            'cool'   : star_list=star_list_all[where(strpos(star_list_all,'phx20_a+0.0_03') ge 0 or strpos(star_list_all,'phx20_a+0.0_04') ge 0)]
            'hot'    : star_list=star_list_all[where(strpos(star_list_all,'bts20') ge 0 or strpos(star_list_all,'phx20_a+0.0_15') ge 0)]
            'normal' : star_list=star_list_all[where(strpos(star_list_all,'phx20') ge 0 and strpos(star_list_all,'phx20_a+0.0_03') eq -1)]
            else     : star_list=star_list_all
        endcase
    endif else $
        star_list=(tag_exist(conf_str,'spec_tmp_list'))? conf_str.spec_tmp_list : file_search(getenv('FIRE_PIPELINE_PATH')+'calib_FIRE/stellar_templates/uni_vac/*.fits*')

    swlnp=(tag_exist(conf_str,'tel_wlcorr_np'))? conf_str.tel_wlcorr_np : 7
    vr0=(tag_exist(conf_str,'star_vr'))? conf_str.star_vr : 0d
    vsini0=(tag_exist(conf_str,'star_vsini'))? conf_str.star_vsini : 0d
    novsini=(tag_exist(conf_str,'star_fixvsini'))? conf_str.star_fixvsini : 0
    star_magv=(tag_exist(conf_str,'star_magv'))? conf_str.star_magv : 6.0d
    gausslosvd=(tag_exist(conf_str,'star_gausslosvd'))? conf_str.star_gausslosvd : 0

    sub_sky=(tag_exist(conf_str,'sub_sky'))? conf_str.sub_sky : 0
    sky_pos_b=(tag_exist(conf_str,'sky_pos_b'))? conf_str.sky_pos_b : 0
    sky_exp_weights=(tag_exist(conf_str,'sky_exp_weights'))? conf_str.sky_exp_weights : 1.0
    sky_normexp=(tag_exist(conf_str,'sky_normexp'))? conf_str.sky_normexp : -1.0
    sky_mask=(tag_exist(conf_str,'sky_mask'))? conf_str.sky_mask : [0,0,intarr(slit_length-4)+1,0,0]
    sky_algorithm=(tag_exist(conf_str,'sky_algorithm'))? conf_str.sky_algorithm : 1 ;; 0=simple subtraction; 1=Kelson, dim=2, npoly=2

    star_tel_std=(tag_exist(conf_str,'star_tel_std'))? conf_str.star_tel_std : 0
    sens_curve_file=(tag_exist(conf_str,'sens_curve_file'))? conf_str.sens_curve_file : ''
    flux_corr=(tag_exist(conf_str,'flux_corr'))? conf_str.flux_corr : 0
    if(sens_curve_file eq '' and star_tel_std eq 0 and flux_corr eq 1) then begin
        message,/inf,'Sensitivity curve file is not specified. Disabling flux calibration'
        flux_corr=0
    endif
    ord_conv_sigma=(tag_exist(conf_str,'tel_ord_conv_sigma'))? conf_str.tel_ord_conv_sigma : 14d ;;; 9d ;;; 0=> no degradation of spectral resolution; 17.7d
    ord_lsf_par=(tag_exist(conf_str,'tel_ord_lsf_par'))? 31-conf_str.tel_ord_lsf_par : 8
    ord_vel_par=(tag_exist(conf_str,'tel_ord_vel_par'))? 31-conf_str.tel_ord_vel_par : 2
    corr_tel=(tag_exist(conf_str,'corr_tel'))? conf_str.corr_tel : 0
    cr_reject=(tag_exist(conf_str,'cr_reject'))? conf_str.cr_reject : 0

    wlini_filename=(tag_exist(conf_str,'wlini_custom'))? conf_str.wlini_custom : '' 
    fullwlsol=(tag_exist(conf_str,'full_wl_sol'))? conf_str.full_wl_sol : 0
    save_wlsol_filename=(tag_exist(conf_str,'save_wl_sol'))? conf_str.save_wl_sol : ''
    no_illum_corr=(tag_exist(conf_str,'field_illum_corr'))? ~conf_str.field_illum_corr : 0

    eff_exptime=(tag_exist(conf_str,'eff_exptime'))? conf_str.eff_exptime : -1d
    scan_semi_amp_default = (1.35>(20.0*0.45/eff_exptime/2.0)<20.0)
    scan_semi_amp=(tag_exist(conf_str,'scan_semi_amp'))? conf_str.scan_semi_amp : scan_semi_amp_default
    if(scan_semi_amp lt 0) then scan_semi_amp=scan_semi_amp_default
    dith_pos_a=(tag_exist(conf_str,'dith_pos_a'))? conf_str.dith_pos_a : 1.5
    dith_pos_b=(tag_exist(conf_str,'dith_pos_b'))? conf_str.dith_pos_b : -dith_pos_a

    dark=fltarr(2048,2048,n_elements(dark_file))
    for i=0,n_elements(dark_file)-1 do dark[*,*,i]=float(fire_read_fits(dark_file[i],h_dark,/silent,/correct_header))
    dark_norm=median(dark,dim=3)
    ;resistant_mean,dark,3.0,dark_norm,dim=3
    dark_norm/=sxpar(h_dark,'EXPTIME')
    dark=0

    pixflat=readfits(getenv('FIRE_PIPELINE_PATH')+'calib_FIRE/detector/H2RG/pixflat.fits',/silent)
    ;illum_corr_ord=readfits(getenv('FIRE_PIPELINE_PATH')+'calib_FIRE/slits/illumination_orders_e0_45.fits',/silent)
    ;if(keyword_set(no_illum_corr)) then illum_corr_ord=illum_corr_ord*0d +1d

    n_flat=n_elements(flat_file)
    flat_cube=fltarr(2048,2048,n_flat)
    for i=0,n_flat-1 do flat_cube[*,*,i]=(float(fire_read_fits(flat_file[i],h_flat,/silent,/correct_header))-dark_norm*sxpar(h_flat,'EXPTIME'))/pixflat
    flat_nc=fltarr(n_flat)
    for i=0,n_flat-1 do flat_nc[i]=max(median(flat_cube[1100:1500,1300:1700,i],9))
    for i=0,n_flat-1 do flat_cube[*,*,i]=flat_cube[*,*,i]/flat_nc[i]
    gain_flat=sxpar(h_flat,'EGAIN')
    if(gain_flat le 1d-5) then gain_flat=3.84
    flat=fire_clean_frame_med2d(clean_cosmic_stack(flat_cube,bias=0.0,nsig=3.,/filter_neg,gain=gain_flat,readn=16.))

    n_arc=n_elements(arc_file)
    arc_cube=fltarr(2048,2048,n_arc)
    for i=0,n_arc-1 do arc_cube[*,*,i]=float(fire_read_fits(arc_file[i],h_arc,/silent,/correct_header))
    gain_arc=sxpar(h_arc,'EGAIN')
    if(gain_arc le 1d-5) then gain_arc=3.84
    arc_thar=fire_clean_frame_med2d(((n_arc eq 1 ? arc_cube : (clean_cosmic_stack(arc_cube,bias=0.0,nsig=3.,/filter_neg,gain=gain_arc,readn=16.)))-dark_norm*sxpar(h_arc,'EXPTIME'))/pixflat)

    star1=(float(fire_read_fits(obj_file_a,h_star1,/silent,/correct_header))-dark_norm*sxpar(h_star1,'EXPTIME'))/pixflat
    
    gain_star1=sxpar(h_star1,'EGAIN')
    if(gain_star1 le 1d-5) then gain_star1=3.84
    rdnoise_star1=sxpar(h_star1,'ENOISE')
    if(rdnoise_star1 le 1d-5) then rdnoise_star1=15.0

    if(sub_sky eq 1 and sky_pos_b eq 1) then begin
        star2_cube=fltarr(2048,2048,n_obj_b)
        if(n_elements(sky_exp_weights) ne n_obj_b) then sky_exp_weights=dblarr(n_obj_b)+1d/n_obj_b
        exp_sky=0.0
        for i=0,n_obj_b-1 do begin
            star2_cur=(float(fire_read_fits(obj_file_b[i],h_star2,/silent,/correct_header))-dark_norm*sxpar(h_star2,'EXPTIME'))/pixflat
            star2_cube[*,*,i]=star2_cur*sky_exp_weights[i]
            exp_sky+=sxpar(h_star2,'EXPTIME')
        endfor
        if(sky_normexp lt 0) then begin
            sky_normexp=sxpar(h_star1,'EXPTIME')/exp_sky*n_obj_b
            print,'Sky exposure normalization factor:',sky_normexp
        endif
        star2=(n_obj_b gt 1)? total(star2_cube,3)*sky_normexp : star2_cube[*,*,0]*sky_normexp
    endif else $
        star2=(float(fire_read_fits(obj_file_b,h_star2,/silent,/correct_header))-dark_norm*sxpar(h_star2,'EXPTIME'))/pixflat

    gain_star2=sxpar(h_star2,'EGAIN')
    if(gain_star2 le 1d-5) then gain_star2=3.84
    rdnoise_star2=sxpar(h_star2,'ENOISE')
    if(rdnoise_star2 le 1d-5) then rdnoise_star2=15.0

    ra_cur=sxpar(h_star1,'RA')
    dec_cur=sxpar(h_star1,'DEC')
    equinox_cur=sxpar(h_star1,'EQUINOX')
    sxaddpar,h_star1,'O_RA',ra_cur,' Right ascension on the equinox of observation'
    sxaddpar,h_star1,'O_DEC',dec_cur,' Declination on the equinox of observation'
    sxaddpar,h_star1,'OEQUINOX',equinox_cur,' Equinox at the time of observation'
    ra2000=ra_cur
    dec2000=dec_cur
    ra2000+=dith_pos_a/3600d
    dec2000+=scan_semi_amp/3600d
    precess,ra2000,dec2000,equinox_cur,2000.0

    sxaddpar,h_star1,'RA',ra2000,' [deg] Right ascension of the target'
    sxaddpar,h_star1,'DEC',dec2000,' [deg] Declination of the target'
    sxaddpar,h_star1,'EQUINOX',2000.0,' Equinox of coordinates (RA,DEC)'
    sxaddpar,h_star1,'RADECSYS','FK5',' Coordinate system'
    
    sxaddpar,h_star1,'MJD_A',sxpar(h_star1,'ACQTIME')-2400000.5d,' UTC Modified Julian Date (A position)'
    sxaddpar,h_star1,'MJD_B',sxpar(h_star2,'ACQTIME')-2400000.5d,' UTC Modified Julian Date (B position)'
    mjd=(sxpar(h_star1,'MJD_A')+sxpar(h_star1,'MJD_B'))/2d
    sxaddpar,h_star1,'MJD',mjd,' UTC Modified Julian Date (mid-observation)
    baryvel,mjd+2400000.5d,2000.0,vh,vb
    vel=vb[0]*cos(dec2000/!radeg)*cos(ra2000/!radeg)+vb[1]*cos(dec2000/!radeg)*sin(ra2000/!radeg)+vb[2]*sin(dec2000/!radeg)
    sxaddpar,h_star1,'BARYVEL',vel,' [km/s] barycentric correction (NOT APPLIED)'
    sxaddpar,h_star1,'SOFTWARE',pipeline_version
    if(eff_exptime le 0) then eff_exptime=sxpar(h_star1,'EXPTIME')

    if(sub_sky eq 1 and sky_pos_b eq 1) then begin
        oh_frame=fire_clean_frame_med2d(star2)
    endif else begin
        star_cube=fltarr(sxpar(h_star1,'NAXIS1'),sxpar(h_star1,'NAXIS2'),2)
        star_cube[*,*,0]=star1
        star_cube[*,*,1]=star2
        ;arc=min(star_cube,dim=3)*0+arc_thar
        oh_frame=(star1<star2)
        bad_oh=where(finite(oh_frame) ne 1, cbad_oh)
        if(cbad_oh gt 0) then oh_frame[bad_oh]=(total(star_cube,3,/nan))[bad_oh]
        oh_frame=fire_clean_frame_med2d(oh_frame)
    endelse

    kwl3d_ini_s=-1
    if(wlini_filename ne '') then if(file_test(wlini_filename)) then kwl3d_ini_s=mrdfits(wlini_filename,1,/silent)

    crd_map=[[22d,1024d,0.5d],[12d,1024d,2d]]

    kwl3d=(keyword_set(fullwlsol))? $
        fire_fit_wlsol(arc_thar/10d, flat, oh=oh_frame/10d,use_oh=1,display=plot,dy_vis=160,trace_coeff=tr_c,output_flat=output_flat,crd_map=crd_map,max=0,input_wlsol_struct=kwl3d_ini_s,output_wlsol_struct=output_wlsol_struct,wlsol_quality=wlsol_quality,large_offset=1,dwl_thr=2.0/2.,min_rms_thr=0.2/2.,deg_fit=[8,6,1]) : $
        fire_fit_wlsol(arc_thar/10d, flat, oh=oh_frame/10d,use_oh=1,display=plot,dy_vis=160,trace_coeff=tr_c,output_flat=output_flat,low_ord=3,input_wlsol_struct=kwl3d_ini_s,output_wlsol_struct=output_wlsol_struct,wlsol_quality=wlsol_quality)

    if(save_wlsol_filename ne '') then begin
        writefits,save_wlsol_filename,0,/silent
        mwrfits,output_wlsol_struct,save_wlsol_filename,/silent
        mwrfits,wlsol_quality,save_wlsol_filename,/silent
    endif

    gain_flat=sxpar(h_flat,'EGAIN')
    if(gain_flat le 1e-5) then gain_flat=3.84
    gain_flat*=flat_nc[0]*n_flat
    flat_sc=fire_model_sc_light(output_flat,tr_c,gain=gain_flat,rdn=15d,/max_deg)
    flat_norm=(output_flat-flat_sc) ; /max(median(flat[1100:1500,1300:1700],9))
    n_ord=n_elements(tr_c[0,*])

    star1sc=fire_model_sc_light(star1,tr_c,gain=gain_star1,rdnoise=rdnoise_star1,/max_deg)
    star1=star1-star1sc
    star2sc=fire_model_sc_light(star2,tr_c,gain=gain_star1,rdnoise=rdnoise_star1,/max_deg)
    star2=star2-star2sc

    xcrd=dindgen(2048) # (dblarr(2048)+1d)
    ycrd=(dblarr(2048)+1d) # dindgen(2048)
    ncrd=fire_xy2nl(xcrd,ycrd,tr_c,l=lcrd,dl=dlcrd,/truen)
    lcrdn=lcrd ;;*0d +0.5d

    good_map=where(ncrd gt 0,cgood_map)
    wl_gm=poly3d_echelle(double(ncrd[good_map]),xcrd[good_map],lcrd[good_map],kwl3d,/irreg,/legendre,m0=11,crd_map=crd_map)
    wlmap=dblarr(2048,2048)+!values.d_nan
    wlmap[good_map]=wl_gm


    flat_norm_corr=flat_norm

    if(sub_sky eq 1 and sky_algorithm eq 1 and sky_pos_b eq 1) then begin
        isky_img=gain_star2^2/(gain_star2*abs((star2+star2sc)/(total(sky_exp_weights)*sky_normexp))+rdnoise_star2^2)
        sky_model=total(sky_exp_weights)*sky_normexp*$
            create_sky_echelle_order((star2)/(total(sky_exp_weights)*sky_normexp),$
            isky_img=isky_img,wlmap,lcrd,ncrd,sky_mask=sky_mask,$
            everyn=50,12+indgen(21),dim=2,npoly=2,rdnoise=rdnoise_star2,gain=gain_star2)
        star=(star1-sky_model)/flat_norm_corr
        star_p=(star1+star1sc+sky_model/sqrt(50))
    endif else begin
        star=(star1-star2)/flat_norm_corr
        star_p=(star1+star2+star1sc+star2sc)/flat_norm_corr
    endelse

    slit_function=mrdfits(getenv('FIRE_PIPELINE_PATH')+'calib_FIRE/slits/slit_function_e0_45.fits',1)
    slit_function_2d=congrid(transpose(slit_function.slit_fit),2048,slit_length)

    ;x_ord=reform(dindgen(2048) # (dblarr(slit_length)+1d), 2048l*slit_length)
    ;l_ord=reform((dblarr(2048)+1d) # dindgen(slit_length)/double(slit_length-1), 2048l*slit_length)
    wl_new=dblarr(2048,n_ord)
    ord_extr=dblarr(2048,n_ord)
    ord_extr_fit=dblarr(2048,n_ord)
    err_ord_extr_fit=dblarr(2048,n_ord)
    flat_extr=dblarr(2048,n_ord)
    ord_extr_sky=dblarr(2048,n_ord)
    extr_prof_all=dblarr(slit_length,n_ord)
    ord_lin=dblarr(2048,slit_length,n_ord)

    star_extr_2d=dblarr(2048,slit_length,n_ord)
    star_p_extr_2d=dblarr(2048,slit_length,n_ord)
    flatn_extr_2d=dblarr(2048,slit_length,n_ord)

    for i=0,n_ord-1 do begin
        star_extr_2d[*,*,i]=fire_extract_order(star,tr_c,i)
        star_p_extr_2d[*,*,i]=fire_extract_order(star_p,tr_c,i)
        flatn_extr_2d[*,*,i]=fire_extract_order(flat_norm_corr,tr_c,i)
    endfor
    ;;flatn_extr_2d*=illum_corr_ord


    extr_fit_all=fire_estimate_extraction_profile(star_extr_2d,flatn_extr_2d=flatn_extr_2d,kwl3d,/maskoh,$
        orig=extr,ref_ord=ord_psf_ref,nseg=5,fulltrace=bright,/verb,slits_lin_2d=slits_lin_2d,wl_lin=wl_new,wlmap_orders=wlmap_orders,$
        flat_thr=0.005d,/legendre,m0=11,crd_map=crd_map)
    star_p_lin_2d=slits_lin_2d*0d
    flatn_lin_2d=slits_lin_2d*0d

    for i=0,n_ord-1 do begin
        print,'Extracting from order: ',31-i
        star_p_lin_2d[*,*,i]=fire_order_linearisation(star_p_extr_2d[*,*,i]/slit_function_2d,i,kwl3d,wl_new[*,i],/spl,/legendre,m0=11,crd_map=crd_map)
        flatn_lin_2d[*,*,i]=fire_order_linearisation(flatn_extr_2d[*,*,i]/slit_function_2d,i,kwl3d,wl_new[*,i],/spl,/legendre,m0=11,crd_map=crd_map)
        ord_x=fire_extract_lin_order(slits_lin_2d[*,*,i],extr_fit_all[*,*,i],$
            var2d=(((star_p_lin_2d[*,*,i]*flatn_lin_2d[*,*,i])>0)*gain_star1+rdnoise_star1^2)/flatn_lin_2d[*,*,i]^2/gain_star1^2,$
            sky=sky,clean=clean,sum=ord_s,error=err_ord_x,thres=0.005)
        ord_extr_fit[*,i]=ord_x
        err_ord_extr_fit[*,i]=err_ord_x
        ord_extr_sky[*,i]=sky
        ord_extr[*,i]=ord_s
    endfor

    if(keyword_set(plot)) then wdelete,0

;; wavelength solution adjustment trough telluric absorptions
    airmass=(sub_sky eq 1 and sky_pos_b eq 1)? sxpar(h_star1,'AIRMASS') : double(sxpar(h_star1,'AIRMASS')+sxpar(h_star2,'AIRMASS'))/2d
    ; airmass*=(8300.0-2380.0)/(8300.0-2635.0)
    fix_lsf_sig=(keyword_set(bright))? [0,1,1,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0] : 1
    errboost=(keyword_set(bright))? 15d : 1d
    tel_res0=fire_get_wloffset_telluric(wl_new,ord_extr_fit,err_ord_extr_fit=err_ord_extr_fit*errboost,airmass=airmass,maxwl_global=2499.,trans_thr=0.02,$
        star_list=star_list,mdeg=15-4,ord_conv_sigma=ord_conv_sigma,plot=plot,fix_lsf_sig=fix_lsf_sig,vr0=vr0,vsini0=vsini0,/maskoh,lsf_mom=2,$
        novsini=novsini,swlnp=swlnp,ord_lsf_par=ord_lsf_par,ord_vel_par=ord_vel_par,fire_red_lsf=~keyword_set(novsini),$
        gausslosvd=gausslosvd,fit_airmass=fit_airmass,/clean)
    tel_res=(swlnp eq 0)? tel_res0 : fire_adjust_wloffset_telluric(tel_res0,deg1=4,deg2=4,thr_e=0.12,/badonly)  ;;; 0.15
    if(keyword_set(debug) and keyword_set(plot) and swlnp gt 0) then begin
        plot,tel_res[5].wl_shift.wl_shift/(wl_new[1,5]-wl_new[0,5]),psym=-4,xs=3,ys=1,yr=[-2,2]
        for i=0,n_ord-1 do oploterror,dindgen(7)+i/30.,tel_res[i].wl_shift.wl_shift/(wl_new[1,i]-wl_new[0,i]),$
                                      dindgen(7)*0,tel_res[i].wl_shift.ewl_shift/(wl_new[1,i]-wl_new[0,i]),$
                                      col=(i+1)*12,psym=-4,errcol=(i+1)*12
    endif

    ic_wl_arr=(keyword_set(no_illum_corr))? dblarr(2048,n_ord)+1d : fire_model_illumination_correction(/twod)

    ord_extr_fit_new=ord_extr_fit*0d
    ord_extr_fit_err_new=ord_extr_fit*0d
    ord_extr_sky_new=ord_extr_sky*0d
    wl_corr=wl_new

    for i=0,n_ord-1 do begin
        case swlnp of
            0 : wl_corr_cur=wl_new[*,i]
            1 : wl_corr_cur=wl_new[*,i]-tel_res[i].wl_shift.wl_shift[0]
            2 : wl_corr_cur=wl_new[*,i]-interpol(tel_res[i].wl_shift.wl_shift,tel_res[i].wl_shift.wl_coord,wl_new[*,i])
            3 : wl_corr_cur=wl_new[*,i]-interpol(tel_res[i].wl_shift.wl_shift,tel_res[i].wl_shift.wl_coord,wl_new[*,i],/quad)
            else : wl_corr_cur=wl_new[*,i]-interpol(tel_res[i].wl_shift.wl_shift,tel_res[i].wl_shift.wl_coord,wl_new[*,i],/spl)
        endcase
        wl_corr[*,i]=wl_corr_cur
        slits_lin_2d[*,*,i]=fire_order_linearisation(star_extr_2d[*,*,i]/slit_function_2d,i,kwl3d,wl_corr_cur,/spl,/legendre,m0=11,crd_map=crd_map)/rebin(ic_wl_arr[*,i],2048,slit_length)
        star_p_lin_2d[*,*,i]=fire_order_linearisation(star_p_extr_2d[*,*,i]/slit_function_2d,i,kwl3d,wl_corr_cur,/spl,/legendre,m0=11,crd_map=crd_map)/rebin(ic_wl_arr[*,i],2048,slit_length)
        flatn_lin_2d[*,*,i]=fire_order_linearisation(flatn_extr_2d[*,*,i]/slit_function_2d,i,kwl3d,wl_corr_cur,/spl,/legendre,m0=11,crd_map=crd_map)/rebin(ic_wl_arr[*,i],2048,slit_length)

        ord_x=fire_extract_lin_order(slits_lin_2d[*,*,i],extr_fit_all[*,*,i],$
            var2d=(((star_p_lin_2d[*,*,i]*flatn_lin_2d[*,*,i])>0)*gain_star1+rdnoise_star1^2)/flatn_lin_2d[*,*,i]^2/gain_star1^2,$
            sky=sky,clean=clean,sum=ord_s,error=err_ord_x,thres=0.005)
        ord_extr_fit_new[*,i]=ord_x
        ord_extr_fit_err_new[*,i]=err_ord_x
        ord_extr_sky_new[*,i]=sky
    endfor

    ; no blaze offset if the illumination correction has not been applied
    blaze_off=(keyword_set(no_illum_corr))? dblarr(n_ord) : fire_estimate_blaze_offset(abs(slits_lin_2d),flatn_lin_2d,/lin,wl_new) ;; ,wl_corr)

    tel_res_upd=fire_get_wloffset_telluric(wl_new,ord_extr_fit_new,airm=tel_res0[ord_lsf_par].p_tel[2],maxwl_global=2499.,trans_thr=0.02,$
        star_list=star_list,mdeg=11-2,ord_conv_sigma=ord_conv_sigma,plot=plot,fix_lsf_sig=fix_lsf_sig,/maskoh,lsf_mom=2,$
        vr0=tel_res0[0].p_tel[0],vsini0=tel_res0[0].p_tel[1],novsini=novsini,swlnp=0,err_ord_extr_fit=ord_extr_fit_err_new*errboost,$
        ord_lsf_par=ord_lsf_par,ord_vel_par=ord_vel_par,$
        fire_red_lsf=~keyword_set(novsini),star_weights=tel_tmp_weights,gausslosvd=gausslosvd,/clean,fit_airmass=fit_airmass)
    tel_struct=replicate({wl:wl_new[*,0],transmission:dblarr(2048)+1d},n_ord)
    tel_struct.wl=wl_new
    tel_struct.transmission=tel_res_upd.tellcorr/tel_res_upd.mcont

    sxaddpar,h_star1,'VR',float(tel_res_upd[0].p_tel[0]),' [km/s] radial velocity from telluric correction'
    sxaddpar,h_star1,'VSINI',float(tel_res_upd[0].p_tel[1]),' [km/s] rotational broadening from telluric correction'
    pos_wgt=where(tel_tmp_weights gt 0, cpos_wgt)
    for k=0,cpos_wgt-1 do begin
        sxaddpar,h_star1,'TELTMP'+string(k,format='(i2.2)'),strmid(star_list[pos_wgt[k]],strpos(star_list[pos_wgt[k]],'/',/reverse_search)+1),' telluric template spectrum'
        sxaddpar,h_star1,'TELWGT'+string(k,format='(i2.2)'),tel_tmp_weights[pos_wgt[k]],' telluric template weight coefficient'
    endfor

    ord_extr_fit_new_corr=ord_extr_fit_new/tel_struct.transmission

    flatn_lin_extr=reform(flatn_lin_2d[*,25,*],2048,n_ord)
    ord_corr=fire_estimate_order_correction(flatn_lin_extr,{blaze_offset:blaze_off,ord_flat_stitch:31},wl_new,/lin,deg_corr=5,deg_bo_red=2)

    ord_corr.correction_vector_lin[0:25]=!values.d_nan
    ord_corr.correction_vector_lin[2023:*]=!values.d_nan
    ;; for i=0,n_ord-1 do ord_corr[i].correction_vector_lin=1d/ord_corr[i].correction_vector_lin
    spec_merged=fire_merge_orders(star_extr_2d,flatn_extr_2d,extr_fit_all,kwl3d,ord_corr,$
        /autowl,wl=wl_m,sky_merged=sky_merged,ord_wl_shift=(swlnp gt 0 ? tel_res.wl_shift : 0),clean=clean,ord_conv_sigma=ord_conv_sigma,$
        ord_all=ord_all,sky_all=sky_all,err_ord_all=err_ord_all,gain=gain_star1,rdnoise=rdnoise_star1,$
        spec_2d_merged=spec_2d_merged,specerr_merged=specerr_merged,/spl,/legendre,m0=11,crd_map=crd_map)
    spec_merged_corr=fire_merge_orders(star_extr_2d,flatn_extr_2d,extr_fit_all,kwl3d,ord_corr,$
        /autowl,wl=wl_m,sky_merged=sky_merged,ord_wl_shift=(swlnp gt 0 ? tel_res.wl_shift : 0),clean=clean,ord_conv_sigma=ord_conv_sigma,$
        ord_all=ord_all_corr,sky_all=sky_all_corr,err_ord_all=err_ord_all_corr,$
        /correct_telluric,tell=tel_struct,specerr_merged=specerr_merged_corr,$
        spec_2d_merged=spec_2d_merged_corr,specerr_2d_merged=specerr_2d_merged_corr,/spl,/legendre,m0=11,crd_map=crd_map)

    if(flux_corr eq 1) then begin
        if(star_tel_std eq 1) then begin
            mask_telabs=1./(spec_merged/spec_merged_corr gt 0.7) ;; creating a vector with NaNs in the regions of atmospheric transmission <0.7
            flux_struct=fire_estimate_spectral_response_1d(mask_telabs*spec_merged_corr,wl_m,$
                err=sqrt((spec_merged_corr>0)+15.^2),airmass=airmass,$
                vsin=tel_res_upd[0].p_tel[1],v0=tel_res_upd[0].p_tel[0],/maskoh,$
                magv=star_magv,texp=eff_exptime*2.0,plot=plot)
            if(sens_curve_file ne '') then begin
                writefits,sens_curve_file,0
                mwrfits,flux_struct,sens_curve_file
            endif
        endif else flux_struct=mrdfits(sens_curve_file,1,/silent)
        if((size(flux_struct))[2] ne 8) then begin
            message,/inf,'Problem reading flux calibration data. Flux calibration is disabled'
        endif
    endif

    if(keyword_set(debug) and keyword_set(plot)) then begin
        o=10
        plot,wl_new[*,o],ord_extr_fit_new[*,o],xs=1,ys=1,yr=[0,median(spec_merged)]*5.5,xr=[830,2500],/nodata
        for i=0,n_ord-1 do begin &$
            oplot,wl_new[*,i],ord_extr_fit_new[*,i]/(ord_corr[i].correction_vector_lin),col=100*(i+1) &$
            oplot,wl_new[*,i],tel_res_upd[i].mcont*median(spec_merged),col=100*(i+1),thick=3 &$
        endfor
        oplot,wl_m,spec_merged_corr,thick=1,col=210
        oplot,wl_m,spec_merged,thick=1
    endif

    h_out=h_star1
    h_out[0]='XTENSION= ''IMAGE   ''           / IMAGE extension'
    sxaddpar,h_out,'AIRMASS',airmass
    sxaddpar,h_out,'CTYPE1','WAVE'
    sxaddpar,h_out,'CUNIT1','nm'
    sxaddpar,h_out,'BUNIT',(flux_corr eq 1 ? '1e-17 erg/cm^2/s/Angstrom' : 'counts')
    sxaddpar,h_out,'CRVAL1',wl_m[0]
    sxaddpar,h_out,'CD1_1',wl_m[1]-wl_m[0]
    sxaddpar,h_out,'CDELT1',wl_m[1]-wl_m[0]
    sxaddpar,h_out,'CRPIX1',1d
    sxaddpar,h_out,'EXPTDET',sxpar(h_out,'EXPTIME'),' total exposure time on the detector'
    sxaddpar,h_out,'EXPTIME',eff_exptime*(2.0-(sky_pos_b eq 1 and sub_sky eq 1)),' total exposure time on the slit (2 scans if applicable)'
    sxaddpar,h_out,'DITHPOSA',dith_pos_a,' A dither position (arcsec)'
    sxaddpar,h_out,'DITHPOSB',dith_pos_b,' B dither position (arcsec)'
    sxaddpar,h_out,'SSEMIAMP',scan_semi_amp,' scan semi-amplitude across the slit (arcsec)'
    if(keyword_set(flux_corr)) then sxaddpar,h_out,'SENSCURV',sens_curve_file,' Sensitivity curve data filename'
    fire_fits_output_mef, outfile_merged_1d, spec_merged_corr, wl_m, h_out, error=specerr_merged_corr, atmtrans=spec_merged/spec_merged_corr, flux=flux_corr, fcorr=flux_struct, exptime=eff_exptime*2, /verbose
    fire_fits_output_mef, outfile_merged, spec_2d_merged_corr, wl_m, h_out, error=specerr_2d_merged_corr, atmtrans=spec_2d_merged/spec_2d_merged_corr, flux=flux_corr, fcorr=flux_struct, exptime=eff_exptime*2

    if(outfile_orders_1d ne '') then fire_fits_output_mef, outfile_orders_1d, reform(ord_extr_fit_new_corr,n_elements(wl_new[*,0]),1,n_ord), wl_new, h_out, flux=flux_corr, fcorr=flux_struct, exptime=eff_exptime*2
    if(outfile_orders_1d_err ne '') then fire_fits_output_mef, outfile_orders_1d_err, reform(ord_extr_fit_err_new/tel_struct.transmission,n_elements(wl_new[*,0]),1,n_ord), wl_new, h_out, flux=flux_corr, fcorr=flux_struct, exptime=eff_exptime*2

    t1=systime(1)
    print,'Time elapsed: ',t1-t0,' seconds'

end
