pro fire_display_arc_lines,image,fit_data,kwl3d,line_data,wl_lines,good_id_max,$
    zoom=zoom,z_vis=z_vis,dy_vis=dy_vis,flmin=flmin,flmax=flmax,noresid=noresid,$
    verbose=verbose,d_lam_thr=d_lam_thr,slith=slith,$
    colred=colred,colblue=colblue,noimage=noimage,legendre=legendre,m0=m0,crd_map=crd_map

    if(n_elements(flmin) ne 1) then flmin=-100.
    if(n_elements(flmax) ne 1) then flmax=6e3
    if(n_elements(zoom) ne 1) then zoom=400.
    if(n_elements(dy_vis) ne 1) then dy_vis=0
    if(n_elements(d_lam_thr) ne 1) then d_lam_thr=0.07 ;;; 0.07nm
    if(n_elements(z_vis) ne 1) then z_vis=1d
    if(n_elements(slith) ne 1) then slith=51l
    if(n_elements(colred) ne 1) then colred=254
    if(n_elements(colblue) ne 1) then colblue=60

    loadct,0
;;    if(~keyword_set(noimage)) then tv,bytscl(image[*,dy_vis:*],flmin,flmax)
    if(~keyword_set(noimage)) then $
        if(z_vis ne 1d) then $
            tv,bytscl(congrid(image[*,dy_vis:*],s_img[1]*z_vis,(s_img[2]-dy_vis)*z_vis),flmin,flmax) $
        else $
            tv,bytscl(image[*,dy_vis:*],flmin,flmax)
    loadct,39
    cgood_id_max=n_elements(good_id_max)
    lam_fit=reform(poly3d_echelle(transpose(fit_data[0,*]),transpose(fit_data[1,*]),transpose(fit_data[2,*]),kwl3d,/irreg,legendre=legendre,m0=m0,crd_map=crd_map),51l,cgood_id_max)
    for i=0,n_elements(line_data)-1 do plots,(line_data[i].xpos)*z_vis,(line_data[i].ypos-dy_vis)*z_vis,col=128,/dev; psym=7,syms=0.2

    for k=0,cgood_id_max-1 do if(finite(wl_lines[good_id_max[k]]) eq 1) then begin
        if(~keyword_set(noresid)) then $
            for j=0,slith-1 do plots,([0,(wl_lines[good_id_max[k]]-lam_fit[j,k])*zoom]+line_data[good_id_max[k]].xpos[j])*z_vis,([0d,0d]+line_data[good_id_max[k]].ypos[j]-dy_vis)*z_vis,$
                                col=((wl_lines[good_id_max[k]] gt lam_fit[j,k])? colred : colblue),psym=-3,/dev
        arrow,median(line_data[good_id_max[k]].xpos)*z_vis,(median(line_data[good_id_max[k]].ypos)-dy_vis)*z_vis,$
              (median(line_data[good_id_max[k]].xpos)+median(wl_lines[good_id_max[k]]-lam_fit[*,k])*zoom)*z_vis,(median(line_data[good_id_max[k]].ypos)-dy_vis)*z_vis,col=192,hsize=5
        if(keyword_set(verbose)) then begin
            d_lam_cur=median(wl_lines[good_id_max[k]]-lam_fit[*,k])
            if(abs(d_lam_cur) gt d_lam_thr) then $
                print,'High offset line (>',d_lam_thr,'nm), Ord #',line_data[good_id_max[k]].order,' wl=',wl_lines[good_id_max[k]],' dwl=',d_lam_cur,'nm',$
                    format='(a,f4.2,a,i2,a,f8.2,a,f5.2,a)'
        endif
    endif

end
