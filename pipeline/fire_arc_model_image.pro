function fire_arc_model_image,nx,ny,kwl3d,trace_coeff,fwhm,$
        x0=x0,ord_ny=ord_ny,linetab=linetab,weight_tab=weight_tab,$
        el_lab_tab=el_lab_tab,labellist=labellist,pixel_fwhm=pixel_fwhm,$
        sigma=sigma,flat_orders=flat_orders,$
        legendre=legendre,crd_map=crd_map,m0=m0,o_dim=o_dim

    if(n_elements(ord_ny) ne 1) then ord_ny=51
    if(n_elements(m0) ne 1) then m0=11

    image=dblarr(nx,ny)
    s_tr=size(trace_coeff)
    n_ord=s_tr[2]

    if(n_elements(flat_orders) ne nx*n_ord) then flat_orders=1d
    wlmap=dblarr(nx,ord_ny,n_ord)
    flat_map=dblarr(nx,ord_ny,n_ord)+1d

    x_ord=reform(dindgen(nx) # (dblarr(ord_ny)+1d), long(nx)*long(ord_ny))
    l_ord=reform((dblarr(nx)+1d) # dindgen(ord_ny)/(double(ord_ny)-1d), long(nx)*long(ord_ny))

    for i=0,n_ord-1 do begin
        wlmap[*,*,i]=reform(poly3d_echelle(x_ord*0d +double(m0+n_ord-1-i),x_ord,l_ord,kwl3d,/irreg,legendre=legendre,crd_map=crd_map,m0=m0,o_dim=o_dim),nx,ord_ny)
        if(n_elements(flat_orders) eq nx*n_ord) then $
            flat_map[*,*,i]=(flat_orders[*,i] # (dblarr(ord_ny) + 1d))
    endfor

    model_orders=reform(fire_arc_model_2d(reform(wlmap,nx,ord_ny*n_ord),fwhm,pixel_fwhm=pixel_fwhm,linetab=linetab,weight_tab=weight_tab,el_lab_tab=el_lab_tab,labellist=labellist),nx,ord_ny,n_ord)*flat_map
    model_orders[*,0,*]*=0.5d
    model_orders[*,ord_ny-1,*]*=0.5d
    for i=0,n_ord-1 do image=image+fire_order_to_image(model_orders[*,*,i],trace_coeff,i,x0=x0,nx=nx,ny=ny)

    if(n_elements(sigma) eq 1) then begin
        if(sigma gt 0) then begin
            n=((sigma*2.355*3) > 9)
            krnl=psf_gaussian(ndim=2,npix=[n,n],fwhm=[sigma,sigma]*2.355d,/norm,/double)
            image=convol(image,krnl)
        endif
    endif

    return,image
end
