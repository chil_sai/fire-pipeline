function read_arc_list,linetab_list,weight_tab=weight_tab,el_lab_tab=el_lab_tab,$
        labellist=labellist
    n_tab=n_elements(linetab_list)
    if(n_elements(weight_tab) ne n_tab) then weight_tab=dblarr(n_tab)+1d
    if(n_elements(el_lab_tab) ne n_tab) then el_lab_tab=strarr(n_tab)

    labellist=[' ']
    for i=0,n_tab-1 do begin
        linelist_cur=read_asc(linetab_list[i])
        linelist_cur[2,*]*=weight_tab[i]
        if(i eq 0) then begin
            linelist=linelist_cur
        endif else begin
            linelist=transpose([transpose(linelist),transpose(linelist_cur)])
        endelse
        labellist=[labellist,replicate(el_lab_tab[i],n_elements(linelist_cur[0,*]))]
    endfor
    labellist=labellist[1:*]
    return,linelist
end
