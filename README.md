# README #

This repository contains a data reduction pipeline for the Folded-port
Infrared Echelette, an near-IR Echelle spectrograph operated at the 6.5-m
Magellan Baade telescope.
Details can be found in the project [wiki pages](https://bitbucket.org/chil_sai/fire-pipeline/wiki/Home)

* Version 0.5.0 beta 1

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact